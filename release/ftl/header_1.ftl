<#include "include.ftl">
<header>
	<div class="container page_title">
		<div class="row">
			<div class="col-md-8"><h1 class="text-left" style="color:#fff;margin-top: 10px;">公告发布管理后台</h1></div>
			<div class="col-md-4">
				<ul class="inline">
					<li class="userinfo"><p class="text-right"><span style="color:#A1ABAF;">欢迎</span> &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#0066aa;">${username!}</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="${webPath}/cmndd/logout" style="color:#A1ABAF;">退出</a></p></li>
				</ul>
			</div>
		</div>
	</div>
</header>
