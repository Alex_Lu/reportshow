<#include "include.ftl">
<html  lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>公告列表</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0"/>
		<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="${basePath}/resources/lib/bootstrapValidator/css/bootstrapValidator.css"/>
		<link rel="stylesheet" type="text/css" href="${basePath}/resources/css/index.css"/>
		<script>
		var typemap = new Map();
			<#if typelist?exists>
                <#list typelist as type>
                	typemap.put("${type.code}","${type.name}"); 
                </#list>
		  </#if>	
		  var thememap = new Map();
			<#if themelist?exists>
                <#list themelist as theme>
                	thememap.put("${theme.code}","${theme.name}"); 
                </#list>
		  </#if>	
		  var company = '';
		  <#if company?exists>
               company = '${company}';
		  </#if>
		</script>
		
	</head>

	<body>
		<#include "header.ftl">
		<div class="container list">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					    <label for="inputselect">公告类型</label>
					    <div class="controls">
					      <select id="inputselect" class="form-control">
						      <#if typelist?exists>
					                <#list typelist as type>
					      				<option value="${type.code}">${type.name}</option>
					                </#list>
							  </#if>	
							</select>
					    </div>
					  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					    <label for="inputname">公告名称</label>
					    <div class="controls">
					      <input type="text" class="form-control" id="inputname" placeholder="公告名称">
					    </div>
					  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					    <label for="inputdate">创建时间</label>
					    <div class="controls">
					      <input type="date" class="form-control" id="inputdate" placeholder="公告时间">
					    </div>
					  </div>
				</div>
				<div class="col-md-3">
					   <button type="button" class="btn btn-primary " onclick="querylist()" style="margin-top: 23px;height: 35px;width: 70px;">查询</button>
				</div>
			</div>
			
			<div class="btn-toolbar" style="margin-bottom:5px;">
				<div class="btn-group">
				  <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height: 35px;width: 70px;">新建</button>
				</div>
				<div class="btn-group">
				  <button class="btn btn-primary" style="height: 35px;width: 70px;" onclick="valiDeleteSelectReportAjax()">删除</button>
				</div>
				<div class="btn-group">
				  <button class="btn btn-primary" style="height: 35px;width: 70px;" onclick="publishReport()">发布</button>
				</div>
			</div>
				<table class="table table-bordered table-striped">
					<tr><th><input type="checkbox" id="check_all"/></th><th>公告类型</th><th>公告名称</th><th>二维码</th><th>创建时间</th><th>操作</th></tr>
					<tr class="zwsjtr"><td colspan="7">暂无数据···</td></tr>
				</table>
			<ul id="pagination-report" class="pagination-lg" style="float:right;margin-top: 5px;"></ul>
		</div>
		<div class="noshow erweimadiv" style="position:absolute;">
			<img src="${basePath}/resources/image/test.png" id="erweima"/>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">创建公告</h4>
		      </div>
		      <div class="modal-body" style="width:80%;margin-left:auto;margin-right:auto;">
		        <form class="form-horizontal" id="addform">
				  <div class="form-group">
				    <label for="inputGgname">公告名称</label>
				    <div class="controls">
				      <input type="text" id="inputGgname" class="form-control" name="ggname" placeholder="公告名称">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputGgtype">公告类型</label>
				    <div class="controlsggtype">
				      <select id="inputGgtype" name="ggtype" class="form-control">
				      		<#if typelist?exists>
					                <#list typelist as type>
					      				<option value="${type.code}" <#if type.code=='001-003'>selected</#if>>${type.name}</option>
					                </#list>
							  </#if>
						</select>
				    </div>
				  </div>
				  <div class="form-group themefield">
				    <label for="inputGgtheme">公告主题</label>
				    <div class="controls">
				      <select id="inputGgtheme" class="form-control">
				      	      <#if themelist?exists>
					                <#list themelist as theme>
					      				<option value="${theme.code}">${theme.name}</option>
					                </#list>
							  </#if>
						</select>
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputGgdate">公告年度</label>
				    <div class="controls">
				    	<select id="inputGgdate" class="form-control">
				    		<option value="2015">2015</option>
				    	</select>
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputGgunit">公告单位</label>
				    <div class="controls">
				      <select id="inputGgunit" class="form-control">
				      	      <#if unitlist?exists>
					                <#list unitlist as unit>
					      				<option value="${unit.code}">${unit.name}</option>
					                </#list>
							  </#if>
						</select>
				    </div>
				  </div>
					<div class="form-group " style="">
				       <div class="col-lg-10 controls has-feedback has-error" style="width: 100%;text-indent: 25px;">
				        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style="">除封面、封底外，所有页面均为可选页，公司可根据自身情况选择是否填写并展示，但为报告生成美观考虑，建议展示页面不少于10页</small>
				       </div>
		          	</div>
				</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default canceladd" data-dismiss="modal" onclick="resetadd()">取消</button>
		        <button type="button" class="btn btn-primary" onclick="addReport()">保存</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- delete -->
		<div class="modal fade" id="myDelete" tabindex="-1" role="dialog" aria-labelledby="myDeleteLabel">
		  <div class="modal-dialog" role="document" style="margin-top: 15%;width: 300px;">
		    <div class="modal-content" style="height: 120px;">
		    	<div class="modal-content" style="box-shadow: 0 0px 100px rgba(0,0,0,.5);">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">确认删除吗</h4>
			      </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default canceladd" data-dismiss="modal" onclick="resetadd()">取消</button>
		        <button type="button" class="btn btn-primary" onclick="deleteReport()" data-dismiss="modal"  style="border:none;">删除</button>
		      </div>
		    </div>
		  </div>
		</div>
		
		<script src="${basePath}/resources/lib/jquery-1.11.1.min.js"></script>
    	<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js"></script>
    	<script src="${basePath}/resources/lib/bootstrap/js/jquery.twbsPagination.js"></script>
     	<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
    	<script src="${basePath}/resources/js/list.js"></script>
	</body>
</html>