<#assign cmndd=JspTaglibs["/conf/cmndd.tld"]>
<#assign basePath=rc.contextPath/>
<#assign webPath=basePath+"/web"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>公告页面管理</title>
	<script src="${basePath}/resources/lib/jquery-1.11.1.min.js"></script>
	<script src="${basePath}/resources/lib/jquery-ui-1.11.1/jquery-ui.min.js"></script>
	<script src="${basePath}/resources/js/report_page.js"></script>
	<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="${basePath}/resources/lib/toastr/toastr.js"></script>
	<link rel="stylesheet" href="${basePath}/resources/lib/toastr/toastr.css" type="text/css" />
	<script src="${basePath}/resources/js/util.js"></script>
<style type="text/css">
.page_title{
	width:90%;
	
}
header{
	height: 60px;
    border-bottom: 1px solid #f0f0f0;
    line-height: 60px;
    margin-bottom: 30px;
    background-color:#243141;
}
.navigation {
	height: 40px;
	line-height:40px;
	width: 100%;
	background-color: #2175bc;
}
a {
	text-decoration: none;
}
a:hover {text-decoration: none;}
.navigator_zy {
	float: left;
	font-size: 15px;
	text-decoration: none;
	color: #FFF;
	padding-left: 30px;
	padding-left: 30px;
}

.navigator_zy>a>span {
	color: #fff;
}
#dropzone {
	padding: 20px;
    background: #eaeaea;
  	min-height: 100px;
	overflow: hidden;
}

.item {
	float:left;
	width:145px;
	height:220px;
  	cursor: pointer;
  	margin: 5px;
  	padding: 5px 10px;
  	border-radius: 3px;
  	position: relative;
}

.item .remove {
  	position: absolute;
  	bottom: 0px;
  	right: 0px;
}
</style>
<script type="text/javascript">
var basePath = '${basePath}';
var webPath = '${webPath}';

var reportId = '${reportId}';
</script>
</head>
<body>
	<header>
		<div class="container page_title">
			<div class="row">
				<div class="col-md-8"><h1 class="text-left" style="color:#fff;margin-top: 10px;">公告发布管理后台</h1></div>
				<div class="col-md-4">
					<ul class="inline">
						<li class="userinfo"><p class="text-right"><span style="color:#A1ABAF;">欢迎</span> &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#0066aa;">${username}</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="${webPath}/loginctrl/logout" style="color:#A1ABAF;">退出</a></p></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<div style="bottom: 0;margin-top: -32px;" class="navigation">
		<div class="navigator_zy">
			<a href="${webPath}/report/listview"><span>返回主页</span></a>
		</div>
	</div>
	<div class="container">
		<div id="dropzone">
			<#if pageList?exists>
				<#list pageList as page>
				<#if '1'==page.fbPage.dynamic_mode>
				<div class="item drop-item" idStr="${page.fbPage.idStr}" style="background:url(${basePath}/resources/image/001-003/${page.fbPage.page_number}.jpg);background-size:100% 100%;">
					<button type="button" class="btn btn-default btn-xs remove">
						<span class="glyphicon glyphicon-trash"></span>
					</button>
				</div>
				<#else>
				<div class="item" idStr="${page.fbPage.idStr}" style="background:url(${basePath}/resources/image/001-003/${page.fbPage.page_number}.jpg);background-size:100% 100%;">
				</div>
				</#if>
				</#list>
			</#if>
		</div>
		<div class="pull-right">
			<button class="btn btn-primary" id="report_page_def_btn">重设</button>
			<button class="btn btn-primary" id="report_page_save_btn">保存</button>
		</div>
	</div>
</body>
</html>