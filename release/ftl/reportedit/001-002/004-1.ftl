<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>业绩预盈主要原因-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	  	<link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form  id = "form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="4">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<table>
					<tr >
						<td class="td_title"><label for="inputGgname">页面标题</label><span style="color:red;padding:2px;">*</span></td>
						<td >
					  		<div class="form-group">
							    <div class="controls">
							      <input type="text" maxlength="10" placeholder="最多只允许填入10个字(必填)"  class="form-control input_text" id="key_page_title4" name="key_page_title4" value="${key_page_title4!'业绩预增主要原因'}"/>
							    </div>
							 </div>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div class="div_input_submit">
			<input name="submit" type="button" onclick="submit_form()" value="下一步">
		</div>
	</body>
</html>

<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	var fields = {};
	fields.key_page_title4={};
	fields.key_page_title4.group='.controls';
	fields.key_page_title4.validators={};
	fields.key_page_title4.validators.notEmpty={};
	fields.key_page_title4.validators.notEmpty.message='公告名称不可为空';
	validator('form',fields);
});
function submit_form(){
	var key_page_title4 = $("#key_page_title4").val();
	if( key_page_title4 == undefined || key_page_title4 ==null || key_page_title4.length==0){
		j_tip("页面标题不能为空","error");
		return ;
	}
	var key_yysm1 = $("#key_yysm1").val();
	if( key_yysm1 == undefined || key_yysm1 ==null || key_yysm1.length==0){
		j_tip("原因说明1不能为空","error");
		return ;
	}
	var key_yysm2 = $("#key_yysm2").val();
	if( key_yysm2 == undefined || key_yysm2 ==null || key_yysm2.length==0){
		j_tip("原因说明2不能为空","error");
		return ;
	}
	var key_yysm3 = $("#key_yysm3").val();
	if( key_yysm3 == undefined || key_yysm3 ==null || key_yysm3.length==0){
		j_tip("原因说明3不能为空","error");
		return ;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>
