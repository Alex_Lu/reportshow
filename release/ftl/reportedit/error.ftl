<#include "../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>登陆</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="${basePath}/resources/css/index.css"/>
 	<link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
		<header>
			<div class="container page_title">
				<div class="row">
					<div class="col-md-8" style="float: left;"><h1 class="text-left" style="color:#fff;margin-top: 10px;">公告发布管理后台</h1></div>
					<div class="col-md-4" style="float: right;">
						<ul class="inline">
							<li class="userinfo"><p class="text-right"><span style="color:#A1ABAF;">欢迎</span> &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#0066aa;">${username}</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="${webPath}/loginctrl/logout" style="color:#A1ABAF;">退出</a></p></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<div style="margin-top: -31px;;bottom: 0;" class="header">
			<div class="navigator" style="height: 40px;">
			</div>
			<div class="navigator">
				${error}<a href="${webPath}/report/listview" style="padding-left: 10px;">返回主页</a>
			</div>
		</div>
	</body>
</html>
