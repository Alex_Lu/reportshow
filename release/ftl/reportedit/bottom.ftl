<#include "../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>登陆</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
  <link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.
     This must be loaded before fileinput.min.js -->
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<!-- bootstrap.js below is only needed if you wish to the feature of viewing details
     of text file preview via modal dialog -->
<!-- optionally if you need translation for your language then include 
    locale file as mentioned below -->
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput_locale_zh.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>


  <link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
<div class="div_form">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageIdstr!''}">
				<table>
					<tr >
						<td class="td_title">Logo上传</td>
						<td> 
							  <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
							  	  上传文件
							  </button>
	    				</td>
					</tr>
					<tr>
						<td class="td_title">背景</td>
						<td>
							<select name='bj1' >
								<#list bg_emun as emun>
									<option value='${emun.code}' <#if emun.code=bj1!'002-001'>selected</#if>>${emun.name}</option>
								</#list>
							</select>
						</td>
					</tr>
					<tr>
						<td class="td_title"></td>
						<td>
						</td>
					</tr>
				<table>
		</div>
		<div class="div_input_submit">
			<input name="submit" type="button" onclick="submitForm('form','${webPath}/reportsave/savedgnt')" value="提交">
		</div>

  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Modal Heading</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

		
	</body>
</html>

<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/js/transition.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/js/modal.js"></script>

