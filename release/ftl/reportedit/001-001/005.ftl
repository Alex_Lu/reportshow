<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>其他事项说明-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	  	<link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
		<form  id = "form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="5">
			<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
			<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
			<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
			<table>
				<tr >
					<td class="td_title">页面标题<span style="color:red;padding:2px;">*</span></td>
					<td> 
						<input type="text" maxlength="10" placeholder="最多只允许填入10个字(必填)"  class="input_text" id="key_page_title5" name="key_page_title5" value="${key_page_title5!'其他事项说明'}"/>
    				</td>
				</tr>
				<tr>
					<td class="td_title">其他说明<span style="color:red;padding:2px;">*</span></td>
					<td>
						<textarea rows="8" maxlength="60" placeholder="最多只允许填入60个字(必填)"   id="key_qtsm" name="key_qtsm">${key_qtsm!'以上预告数据仅为初步核算数据，具体准确的财务数据以公司正式披露的${(fbReport.date?number-1)?c}年年度报告为准，敬请广大投资者注意投资风险。'}</textarea>
					</td>
				</tr>
					<tr >
						<td class="td_title"></td>
						<td colspan="1">
							<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="下一步">
							</div>
	    				</td>
					</tr>
			</table>
		</form>
		</div>
	</body>
</html>

<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
function submit_form(){
	var key_page_title5 = $("#key_page_title5").val();
	if( key_page_title5 == undefined || key_page_title5 ==null || key_page_title5.length==0){
		j_tip("页面标题不能为空","error");
		return ;
	}
	var key_qtsm = $("#key_qtsm").val();
	if( key_qtsm == undefined || key_qtsm ==null || key_qtsm.length==0){
		j_tip("其他说明不能为空","error");
		return ;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>
