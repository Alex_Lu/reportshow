<#assign _reportedit_ggpagelist = [{"page":"001", "name":"公告封面"},{"page":"002", "name":"业绩预告情况"},{"page":"003", "name":"两年业绩对比"},{"page":"004", "name":"业绩预增主要原因"},{"page":"005", "name":"其他说明事项"},{"page":"006", "name":"相关链接"}]>
<ul class="navList" id="navList">
	<#list _reportedit_ggpagelist as item>
		<a id="page_${item_index+1}"  <#if pageName=='${item.page}'>class="navList_a_select" style="background-color: #bebec5;"</#if>
			href='${webPath}/reportedit/view/${reportType}/${reportIdstr}/${item.page}' title="点击进入${item.name}编辑页面" >
		<li><span>${item_index+1}、${item.name}</span></li>
	</a>
	</#list>
</ul>

