<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>相关链接-编辑</title>
		<meta name="description" content="">
  <link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
  <link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
		<form  id = "form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="6">
			<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
			<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
			<input id="img_key_names" name="img_key_names" type="hidden" value="img_key_tp1,img_key_tp2,img_key_tp3,img_key_tp4">
			<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
			<table>
				<tr>
					<td class="td_title">图片1<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input id="img_key_tp1" name="img_key_tp1"   type="file"  value="${img_key_tp1!''}">
					</td>
				</tr>
				<tr>
					<td class="td_title">图片1的连接地址<span style="color:red;padding:2px;">*</span></td>
					<td><input type="text" class="input_text" placeholder="(必填)"  name="key_ljdz1"  id="key_ljdz1" value="${key_ljdz1!''}"  onblur="isURL(this)" /></td>
				</tr>
					<tr >
						<td class="td_title"></td>
						<td colspan="1">
							<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="下一步">
							</div>
	    				</td>
					</tr>
			</table>
		</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput_locale_zh.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/js/transition.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/js/modal.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>

<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	initFileInput("img_key_tp1",'${img_key_tp1!''}');
	initFileInput("img_key_tp2",'${img_key_tp2!''}');
	initFileInput("img_key_tp3",'${img_key_tp3!''}');
	initFileInput("img_key_tp4",'${img_key_tp4!''}');
});
</script>
<script type="text/javascript" language="javascript">
function submit_form(){
	
	var key_ljdz1 = $("#key_ljdz1").val();
	if( key_ljdz1 == undefined || key_ljdz1 ==null || key_ljdz1.length==0){
		j_tip("图片1的连接地址不能为空","error");
		return ;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
 	setTimeout("load_href('${webPath}/report/listview')", 1000);
}
</script>