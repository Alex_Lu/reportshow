<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>两年业绩对比-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	  	<link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
		<form  id = "form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="3">
			<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
			<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
			<table>
				<tr >
					<td class="td_title">页面标题<span style="color:red;padding:2px;">*</span></td>
					<td colspan="5" > 
						<input type="text" maxlength="10" placeholder="最多只允许填入10个字(必填)"  class="input_text" id="key_page_title3" name="key_page_title3" value="${key_page_title3!'两年业绩对比'}"/>
    				</td>
				</tr>
				<tr >
					<td class="td_title">对比年份<span style="color:red;padding:2px;">*</span></td>
					<td colspan="5" >
						<#if key_dbnf?exists>${key_dbnf!''}<#else>${(fbReport.date?number-1)?c}</#if>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_dbnf" name="key_dbnf" value="<#if key_dbnf?exists>${key_dbnf!''}<#else>${(fbReport.date?number-1)?c}</#if>"  />
					</td>
				</tr>
				<tr>
					<td class="td_title">${(fbReport.date?number-1)?c}年净利润<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input type="text" class="input_text input_number" placeholder="(必填)"   id="gsssgsgddjlr_dbnfjlr" name="gsssgsgddjlr_dbnfjlr" value="${key_gsssgsgddjlr_dbnfjlr!''}"  onblur="this.value=fouces_qfh(this.value,'key_gsssgsgddjlr_dbnfjlr')"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"   />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">(万元)</div>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_gsssgsgddjlr_dbnfjlr" name="key_gsssgsgddjlr_dbnfjlr" value="${key_gsssgsgddjlr_dbnfjlr!''}"  />
					</td>
					<td class="td_title">本期净利润最低<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input type="text" class="input_text input_number" placeholder="(必填)" id="gsssgsgddjlr_ggnfzd" name="gsssgsgddjlr_ggnfzd" value="${key_gsssgsgddjlr_ggnfzd!''}"  onblur="this.value=fouces_qfh(this.value,'key_gsssgsgddjlr_ggnfzd')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">(万元)</div>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_gsssgsgddjlr_ggnfzd" name="key_gsssgsgddjlr_ggnfzd" value="${key_gsssgsgddjlr_ggnfzd!''}"   />
					</td>
					<td class="td_title">本期净利润最高<span style="color:red;padding:2px;">*</span></td>
					<td>	
						<input type="text" class="input_text input_number" placeholder="(必填)" id="gsssgsgddjlr_ggnfzg" name="gsssgsgddjlr_ggnfzg" value="${key_gsssgsgddjlr_ggnfzg!''}"  onblur="this.value=fouces_qfh(this.value,'key_gsssgsgddjlr_ggnfzg')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">(万元)</div>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_gsssgsgddjlr_ggnfzg" name="key_gsssgsgddjlr_ggnfzg" value="${key_gsssgsgddjlr_ggnfzg!''}"  />
					</td>
				</tr>
				<tr>
					<td class="td_title">${(fbReport.date?number-1)?c}年营业收入<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input type="text" class="input_text input_number"  placeholder="(必填)" id="mgsy_dbnfjlr" name="mgsy_dbnfjlr" value="${key_mgsy_dbnfjlr!''}"  onblur="this.value=fouces_qfh(this.value,'key_mgsy_dbnfjlr')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">(元)</div>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_mgsy_dbnfjlr" name="key_mgsy_dbnfjlr" value="${key_mgsy_dbnfjlr!''}"  />
					</td>
					<td class="td_title">本期营业收入最低<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input type="text" class="input_text input_number" placeholder="(必填)" id="mgsy_ggnfzd" name="mgsy_ggnfzd" value="${key_mgsy_ggnfzd!''}"  onblur="this.value=fouces_qfh(this.value,'key_mgsy_ggnfzd')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">(元)</div>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_mgsy_ggnfzd" name="key_mgsy_ggnfzd" value="${key_mgsy_ggnfzd!''}"  />
					</td>
					<td class="td_title">本期营业收入最高<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input type="text" class="input_text input_number" placeholder="(必填)" id="mgsy_ggnfzg" name="mgsy_ggnfzg" value="${key_mgsy_ggnfzg!''}"  onblur="this.value=fouces_qfh(this.value,'key_mgsy_ggnfzg')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">(元)</div>
						<input type="hidden" class="input_text input_number" readonly="true" id="key_mgsy_ggnfzg" name="key_mgsy_ggnfzg" value="${key_mgsy_ggnfzg!''}"  />
					</td>
				</tr>
				<tr>
					<td class="td_title">说明 <span style="color:red;padding:2px;">*</span></td>
					<td colspan="5" >
						<#if key_sm?exists>
							<input type="text" maxlength="40" placeholder="最多只允许填入40个字(必填)"   class="input_text"  id="key_sm" name="key_sm" value="${key_sm!''}"  />
						<#else>
							<input type="text" maxlength="40" placeholder="最多只允许填入40个字(必填)"  class="input_text"  id="key_sm" name="key_sm" value="${(fbReport.date?number-1)?c}年的每股收益已按规定重新计算"  />
						</#if>
					</td>
				</tr>
					<tr >
						<td class="td_title"></td>
						<td colspan="5">
							<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="下一步">
							</div>
	    				</td>
					</tr>
			</table>
		</form>
		</div>
	</body>
</html>


<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>

<script src="${basePath}/resources/lib/My97DatePicker/WdatePicker.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">

$(document).on('ready', function() {
	$("#gsssgsgddjlr_dbnfjlr").val(fouces_qfh('${key_gsssgsgddjlr_dbnfjlr!''}','key_gsssgsgddjlr_dbnfjlr'));
	$("#gsssgsgddjlr_ggnfzd").val(fouces_qfh('${key_gsssgsgddjlr_ggnfzd!''}','key_gsssgsgddjlr_ggnfzd'));
	$("#gsssgsgddjlr_ggnfzg").val(fouces_qfh('${key_gsssgsgddjlr_ggnfzg!''}','key_gsssgsgddjlr_ggnfzg'));
	$("#mgsy_dbnfjlr").val(fouces_qfh('${key_mgsy_dbnfjlr!''}','key_mgsy_dbnfjlr'));
	$("#mgsy_ggnfzd").val(fouces_qfh('${key_mgsy_ggnfzd!''}','key_mgsy_ggnfzd'));
	$("#mgsy_ggnfzg").val(fouces_qfh('${key_mgsy_ggnfzg!''}','key_mgsy_ggnfzg'));
});

function submit_form(){
	//var key_dbnf = $("#key_dbnf").val();
	//if( key_dbnf == undefined || key_dbnf ==null || key_dbnf.length==0){
	//	alert("未填对比年份");
	//	return ;
	//}
	var key_page_title3 = $("#key_page_title3").val();
	if( key_page_title3 == undefined || key_page_title3 ==null || key_page_title3.length==0){
		j_tip("页面标题不能为空","error");
		return ;
	}
	var gsssgsgddjlr_dbnfjlr = $("#gsssgsgddjlr_dbnfjlr").val();
	if( gsssgsgddjlr_dbnfjlr == undefined || gsssgsgddjlr_dbnfjlr ==null || gsssgsgddjlr_dbnfjlr.length==0){
		j_tip("净利润不能为空.","error");
		return ;
	}
	var gsssgsgddjlr_ggnfzd = $("#gsssgsgddjlr_ggnfzd").val();
	if( gsssgsgddjlr_ggnfzd == undefined || gsssgsgddjlr_ggnfzd ==null || gsssgsgddjlr_ggnfzd.length==0){
		j_tip("本期净利润不能为空.","error");
		return ;
	}
	var gsssgsgddjlr_ggnfzg = $("#gsssgsgddjlr_ggnfzg").val();
	if( gsssgsgddjlr_ggnfzg == undefined || gsssgsgddjlr_ggnfzg ==null || gsssgsgddjlr_ggnfzg.length==0){
		j_tip("本期净利润不能为空.","error");
		return ;
	}
	
	var key_gsssgsgddjlr_ggnfzd = $("#key_gsssgsgddjlr_ggnfzd").val();
	var key_gsssgsgddjlr_ggnfzg = $("#key_gsssgsgddjlr_ggnfzg").val();
	if(Number(key_gsssgsgddjlr_ggnfzd) > Number(key_gsssgsgddjlr_ggnfzg)){
		j_tip("本期净利润最低不能高于最高值.","error");
		return ;
	}
	
	
	
	var mgsy_dbnfjlr = $("#mgsy_dbnfjlr").val();
	if( mgsy_dbnfjlr == undefined || mgsy_dbnfjlr ==null || mgsy_dbnfjlr.length==0){
		j_tip("营业收入不能为空.","error");
		return ;
	}
	var mgsy_ggnfzd = $("#mgsy_ggnfzd").val();
	if( mgsy_ggnfzd == undefined || mgsy_ggnfzd ==null || mgsy_ggnfzd.length==0){
		j_tip("本期营业收入不能为空.","error");
		return ;
	}
	var mgsy_ggnfzg = $("#mgsy_ggnfzg").val();
	if( mgsy_ggnfzg == undefined || mgsy_ggnfzg ==null || mgsy_ggnfzg.length==0){
		j_tip("本期营业收入不能为空.","error");
		return ;
	}
	
	if(Number(mgsy_ggnfzd) > Number(mgsy_ggnfzg)){
		j_tip("本期营业收入最低不能高于最高值.","error");
		return ;
	}
	
	
	var key_sm = $("#key_sm").val();
	if( key_sm == undefined || key_sm ==null || key_sm.length==0){
		j_tip("说明不能为空.","error");
		return ;
	}
	submitForm('form','${webPath}/reportsave/savedgnt')
}

function fouces_qfh(obj,vname) {
	if( obj == undefined || obj == null || obj.length==0){
		$("#"+vname).val("");
		return "";
	}
	var o = obj.replace(/[^0-9.]/g, '') + "";
	if( o == undefined || o == null || o.length==0){
		$("#"+vname).val("");
		return "";
	}
	
	
	
	o = format1(o);
	
	
	
	if (o.indexOf(".") <= -1) {
		o += ".00";
	}
	if (o.substr(o.length - 1, 1) == ".") {
		o += ".00";
	}
	if (o.substr(o.length - 2, 1) == ".") {
		o += "0";
	}
	
	
	
	$("#"+vname).val(o.replace(/\,/g, ''));
	return o;
}

/**
 * 千分符格式化
 * 
 * @param num
 * @returns
 */
function format1(num) {
	if (num == null || num == undefined || num == "") {
		return "";
	}
	if (isNaN(num))
		return num;
	// alert(num);
	return (parseFloat(num).toFixed(2) + '').replace(
			/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
	// return (toFixed(num,2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g,
	// '$&,');
}

</script>