<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>公告信息编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog modal-lg" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">请选择Excel文件</h4>
	                </div>
	                <div class="modal-body">
	                    <a href="~/Data/ExcelTemplate/Order.xlsx" class="form-control" style="border:none;">下载导入模板</a>
	                    <input type="file" name="txt_file" id="txt_file" multiple class="file-loading" />
	                </div>
	             </div>
	        </div>
	   </div>
	
	   <div class="btn-toolbar" style="margin-bottom:5px;">
			<div class="btn-group">
			  <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height: 35px;width: 70px;">新增</button>
			</div>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput_locale_zh.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_fileinput.js"></script>
<script type="text/javascript" language="javascript">
$(function () {
    //0.初始化fileinput
    var oFileInput = new FileInput();
    oFileInput.Init("txt_file", '${basePath}',"/api/OrderApi/ImportOrder");
});

</script>