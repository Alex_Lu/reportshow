<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>业绩预告情况-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	  	<link href="${basePath}/resources/css/reportedit/edit.css" rel="stylesheet">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
		<form  id = "form" method="post" enctype="multipart/form-data">
			<input id="page_label" name="page_label" type="hidden" value="2">
			<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
			<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
			<table>
					<tr >
						<td class="td_title">页面标题<span style="color:red;padding:2px;">*</span></td>
						<td colspan="3"> 
							<input type="text" maxlength="10" placeholder="最多只允许填入10个字(必填)"   class="input_text" id="key_page_title2" name="key_page_title2" value="${key_page_title2!'业绩预告情况'}"/>
	    				</td>
					</tr>
				<!-- 
				<tr>
					<td class="td_title">净利润说明<span style="color:red;padding:2px;">*</span></td>
					<td colspan="3">
						<input type="text"  maxlength="20" placeholder="最多只允许填入20个字(必填)" class="input_text" id="key_jlrsm" name="key_jlrsm" value="${key_jlrsm!''}"/>
					</td>
				</tr>
				-->
				<tr>
					<td class="td_title">同期增长范围(最低)<span style="color:red;padding:2px;">*</span></td><!-- onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  -->
					<td>
						<input type="text" class="input_text" placeholder="(必填)" style="width: 80%;text-align:right;float: left;" id="tqzzfwzd" name="tqzzfwzd" value="${key_tqzzfwzd!''}"  onblur="this.value=fouces_bfh(this.value,'key_tqzzfwzd')" />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">%</div>
						<input type="hidden" class="input_text" readonly="true" id="key_tqzzfwzd" name="key_tqzzfwzd" value="${key_tqzzfwzd!''}"  />
					</td>
					<td class="td_title">同期增长范围(最高)<span style="color:red;padding:2px;">*</span></td>
					<td>
						<input type="text" class="input_text" placeholder="(必填)" style="width: 80%;text-align:right;float: left;"  id="tqzzfwzg" name="tqzzfwzg" value="${key_tqzzfwzg!''}"  onblur="this.value=fouces_bfh(this.value,'key_tqzzfwzg')"  />
						<div style="font-size: 10px;;height: 30;line-height: 40px;">%</div>
						<input type="hidden" class="input_text" readonly="true" id="key_tqzzfwzg" name="key_tqzzfwzg" value="${key_tqzzfwzg!''}" />
					</td>
				</tr>
				<tr>
					<td class="td_title">申明<span style="color:red;padding:2px;">*</span></td>
					<td colspan="3">
						<#if key_smwz1?exists>
							<input type="text" maxlength="20" placeholder="最多只允许填入20个字(必填)"   class="input_text" id="key_smwz1" name="key_smwz1" value="${key_smwz1!''}"/>
						<#else>
							<input type="text" maxlength="20" placeholder="最多只允许填入20个字(必填)"  class="input_text" id="key_smwz1" name="key_smwz1" value="本次所预计的业绩"/>
						</#if>
					</td>
				</tr>
				<tr>
					<td class="td_title"><span style="color:red;padding:2px;">*</span></td>
					<td colspan="3">
						<#if key_smwz2?exists>
							<input type="text" maxlength="20" placeholder="最多只允许填入20个字(必填)"  class="input_text" id="key_smwz2" name="key_smwz2" value="${key_smwz2!''}"/>
						<#else>
							<input type="text" maxlength="20" placeholder="最多只允许填入20个字(必填)"  class="input_text" id="key_smwz2" name="key_smwz2" value="未经注册会计师审计"/>
						</#if>
					</td>
				</tr>
					<tr >
						<td class="td_title"></td>
						<td colspan="3">
							<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="下一步">
							</div>
	    				</td>
					</tr>
			</table>
		</form>
		</div>
	</body>
</html>

<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">

$(document).on('ready', function() {
	$("#tqzzfwzd").val(fouces_bfh("${key_tqzzfwzd!''}",'key_tqzzfwzd'));
	$("#tqzzfwzg").val(fouces_bfh("${key_tqzzfwzg!''}",'key_tqzzfwzg')) ;
});
/**
 * 自动补充百分比符号
 * @param obj
 * @returns {String}
 */
function fouces_bfh(obj,vname) {
	var o = obj.replace(',', '').replace('.', '') + "";
	/*
	if(Number(o)>100 || Number(o)<0){
		return $("#"+vname).val();
	}
	*/
	var v = o;
	if(!isNaN(v)){
		$("#"+vname).val(Number(v));
	}
	return Number(o) ;
}
</script>


<script type="text/javascript" language="javascript">
function submit_form(){
	//var key_jlrsm = $("#key_jlrsm").val();
	//if( key_jlrsm == undefined || key_jlrsm ==null || key_jlrsm.length==0){
	//	alert("未填写净利润说明");
	//	return ;
	//}
	
	var key_page_title2 = $("#key_page_title2").val();
	if( key_page_title2 == undefined || key_page_title2 ==null || key_page_title2.length==0){
		j_tip("页面标题不能为空","error");
		return ;
	}
	var tqzzfwzd = $("#tqzzfwzd").val();
	if( tqzzfwzd == undefined || tqzzfwzd ==null || tqzzfwzd.length==0){
		j_tip("同比增长范围不能为空","error");
		return ;
	}
	var tqzzfwzg = $("#tqzzfwzg").val();
	if( tqzzfwzg == undefined || tqzzfwzg ==null || tqzzfwzg.length==0){
		j_tip("同比增长范围不能为空","error");
		return ;
	}
	
	if(tqzzfwzd!=undefined && tqzzfwzd!=null && tqzzfwzd.length>0 &&tqzzfwzg!=undefined && tqzzfwzg!=null && tqzzfwzg.length>0 ){
		if(Number(tqzzfwzd)> Number(tqzzfwzg)){
			j_tip("同期增长范围最低不能高于最高值.","error");
			return ;
		}
	}
	
	
	var key_smwz1 = $("#key_smwz1").val();
	if( key_smwz1 == undefined || key_smwz1 ==null || key_smwz1.length==0){
		j_tip("申明不能为空","error");
		return ;
	}
	var key_smwz2 = $("#key_smwz2").val();
	if( key_smwz2 == undefined || key_smwz2 ==null || key_smwz2.length==0){
		j_tip("申明不能为空","error");
		return ;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>