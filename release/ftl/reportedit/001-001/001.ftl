<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>封面-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form  id = "form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<input id="img_key_names" name="img_key_names" type="hidden" value="img_key_logo">
				<table>
					<tr >
						<td class="td_title">页面标题<span style="color:red;padding:2px;">*</span></td>
						<td> 
							<input type="text" placeholder="最多只允许填入10个字(必填)"   maxlength="10"  class="input_text" id="key_page_title1" name="key_page_title1" value="${key_page_title1!'公告封面'}"/>
	    				</td>
					</tr>
					<tr >
						<td class="td_title">公司全称<span style="color:red;padding:2px;">*</span></td>
						<td> 
							<#if key_company_fullname?exists>
								<input type="text" maxlength="20" placeholder="最多只允许填入20个字(必填)"   class="input_text" id="key_company_fullname" name="key_company_fullname" value="${key_company_fullname!''}"/>
							<#else>
								<input type="text" maxlength="20" placeholder="最多只允许填入20个字(必填)"  class="input_text" id="key_company_fullname" name="key_company_fullname" value="${session_company!''}"/>
							</#if>
	    				</td>
					</tr>
					<tr >
						<td class="td_title">公司LOGO<span style="color:red;padding:2px;">*</span><br/>(640x156)</td>
						<td>
							<input id="img_key_logo" name="img_key_logo" type="file" >
							<input id="logo_001_img_key_logo" name="logo_001_img_key_logo" value="${img_key_logo!''}" type="hidden">
	    				</td>
					</tr>
					<tr >
						<td class="td_title"></td>
						<td>
							<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="下一步">
							</div>
	    				</td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput_locale_zh.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/js/transition.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/js/modal.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>

<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	initFileInput("img_key_logo","${img_key_logo!''}");
});
function submit_form(){
	var logo_001_img_key_logo = $("#logo_001_img_key_logo").val();
	if( logo_001_img_key_logo == undefined || logo_001_img_key_logo ==null || logo_001_img_key_logo.length==0){
		var key_logo = $("#img_key_logo").val();
		if( key_logo == undefined || key_logo ==null || key_logo.length==0){
			j_tip("公司LOGO不能为空","error");
			return ;
		}
	}
	
	var key_page_title1 = $("#key_page_title1").val();
	if( key_page_title1 == undefined || key_page_title1 ==null || key_page_title1.length==0){
		j_tip("页面标题不能为空","error");
		return ;
	}
	var key_company_fullname = $("#key_company_fullname").val();
	if( key_company_fullname == undefined || key_company_fullname ==null || key_company_fullname.length==0){
		j_tip("公司名称不能为空","error");
		return ;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>