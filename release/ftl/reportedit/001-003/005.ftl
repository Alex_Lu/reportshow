<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>财务数据-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<div class="form-group " style="margin: 10px;margin-top: -10px;">
				<div class="container-fluid" style="padding:0px;width: 1060px;margin-top:20px;">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
							<table class="table-condensed"  style="width:1060px;margin-left:0px;border: 1px solid #cccccc;margin-left: auto;margin-right: auto;">
								<thead>
									<tr  class="">
										<th style="text-align: left;padding-left: 20px;width: 400px;">
										项目（单位:${fbReport.unitEnumeration.name}）
										</th>
										<th style="text-align: left;padding-left: 20px;">
											${(fbReport.date!'')}年
										</th>
										<th style="text-align: left;padding-left: 20px;">
											${(fbReport.date?number-1)?c}年
										</th>
										<!--
										<th style="text-align: left;padding-left: 20px;">
											本期比上年同期增减(%)
										</th>
										 -->
										<th style="text-align: left;padding-left: 20px;">
											${(fbReport.date?number-2)?c}年
										</th>
									</tr>
								</thead>
								<tbody>
									<tr   class="success">
										<td style="width: 400px;">
											<label class="col-sm-2 control-label" for="key_001_003_yysr">营业收入</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_yysr_bq" value="${(key_001_003_yysr_bq)!''}" name="hidden_key_001_003_yysr_bq"  onblur="zhszfd(this.value,'key_001_003_yysr_bq','hidden_key_001_003_yysr_bq',2,2)"    type="text" />
						                     	<input class="form-control" id="key_001_003_yysr_bq" value="${(key_001_003_yysr_bq)!''}" name="key_001_003_yysr_bq"    type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
						                   	<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_yysr_sn1" value="${(key_001_003_yysr_sn1)!''}" name="hidden_key_001_003_yysr_sn1"    onblur="zhszfd(this.value,'key_001_003_yysr_sn1','hidden_key_001_003_yysr_sn1',2,2)"   type="text" />
						                     	<input class="form-control" id="key_001_003_yysr_sn1" value="${(key_001_003_yysr_sn1)!''}" name="key_001_003_yysr_sn1"    type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<!--
										<td>
						                   	<div class="col-sm-2 controls" style="width:inherit;">
						                   		<input class="form-control" id="hidden_key_001_003_yysr_zs" value="${(key_001_003_yysr_zs)!''}" name="hidden_key_001_003_yysr_zs"    onblur="zhszfd(this.value,'key_001_003_yysr_zs','hidden_key_001_003_yysr_zs',2,2)"   type="text" />
						                   		<input class="form-control" id="key_001_003_yysr_zs" value="${(key_001_003_yysr_zs)!''}" name="key_001_003_yysr_zs"    type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										 -->
										<td>
						                   	<div class="col-sm-2 controls" style="width:inherit;">
						                   		<input class="form-control" id="hidden_key_001_003_yysr_sn2" value="${(key_001_003_yysr_sn2)!''}" name="hidden_key_001_003_yysr_sn2"    onblur="zhszfd(this.value,'key_001_003_yysr_sn2','hidden_key_001_003_yysr_sn2',2,2)"   type="text" />
						                   		<input class="form-control" id="key_001_003_yysr_sn2" value="${(key_001_003_yysr_sn2)!''}" name="key_001_003_yysr_sn2"    type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<tr class="success">
										<td style="width: 400px;">
											<label class="col-sm-2 control-label" for="key_001_003_gslmgsdjlr">归属于母公司的净利润</label>
										</td>
										<td>
						                  	<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_gslmgsdjlr_bq" value="${(key_001_003_gslmgsdjlr_bq)!''}" name="hidden_key_001_003_gslmgsdjlr_bq"   onblur="zhszfd(this.value,'key_001_003_gslmgsdjlr_bq','hidden_key_001_003_gslmgsdjlr_bq',2,2)"   type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_gslmgsdjlr_bq" value="${(key_001_003_gslmgsdjlr_bq)!''}" name="key_001_003_gslmgsdjlr_bq"  type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_gslmgsdjlr_sn1" value="${(key_001_003_gslmgsdjlr_sn1)!''}" name="hidden_key_001_003_gslmgsdjlr_sn1"   onblur="zhszfd(this.value,'key_001_003_gslmgsdjlr_sn1','hidden_key_001_003_gslmgsdjlr_sn1',2,2)"   type="text" placeholder=""/>
						                     	<input class="form-control" id="key_001_003_gslmgsdjlr_sn1" value="${(key_001_003_gslmgsdjlr_sn1)!''}" name="key_001_003_gslmgsdjlr_sn1"  type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<!--
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
											<input class="form-control" id="hidden_key_001_003_gslmgsdjlr_zs" value="${(key_001_003_gslmgsdjlr_zs)!''}" name="hidden_key_001_003_gslmgsdjlr_zs"   onblur="zhszfd(this.value,'key_001_003_gslmgsdjlr_zs','hidden_key_001_003_gslmgsdjlr_zs',2,2)"   type="text" placeholder=""/>
											<input class="form-control" id="key_001_003_gslmgsdjlr_zs" value="${(key_001_003_gslmgsdjlr_zs)!''}" name="key_001_003_gslmgsdjlr_zs"  type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										 -->
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
											<input class="form-control" id="hidden_key_001_003_gslmgsdjlr_sn2" value="${(key_001_003_gslmgsdjlr_sn2)!''}" name="hidden_key_001_003_gslmgsdjlr_sn2"   onblur="zhszfd(this.value,'key_001_003_gslmgsdjlr_sn2','hidden_key_001_003_gslmgsdjlr_sn2',2,2)"  type="text" placeholder=""/>
											<input class="form-control" id="key_001_003_gslmgsdjlr_sn2" value="${(key_001_003_gslmgsdjlr_sn2)!''}" name="key_001_003_gslmgsdjlr_sn2"  type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<tr class="error">
										<td style="width: 400px;">
											<div class="col-sm-2 controls has-feedback" style="width:inherit;">
						                     	<input class="form-control" id="key_001_003_005_qt_title" value="${(key_001_003_005_qt_title)!'归属于母公司的净资产'}"  name="key_001_003_005_qt_title" type="text" placeholder="(必填)"/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_005_qt_bq" value="${(key_001_003_005_qt_bq)!''}"  name="hidden_key_001_003_005_qt_bq"   onblur="zhszfd(this.value,'key_001_003_005_qt_bq','hidden_key_001_003_005_qt_bq',2,2)"  type="text" placeholder=""/>
						                     	<input class="form-control" id="key_001_003_005_qt_bq" value="${(key_001_003_005_qt_bq)!''}"  name="key_001_003_005_qt_bq"   type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_005_qt_sn1" value="${(key_001_003_005_qt_sn1)!''}"  name="hidden_key_001_003_005_qt_sn1"   onblur="zhszfd(this.value,'key_001_003_005_qt_sn1','hidden_key_001_003_005_qt_sn1',2,2)"   type="text" placeholder=""/>
						                     	<input class="form-control" id="key_001_003_005_qt_sn1" value="${(key_001_003_005_qt_sn1)!''}"  name="key_001_003_005_qt_sn1"    type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<!--
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_005_qt_zs" value="${(key_001_003_005_qt_zs)!''}"  name="hidden_key_001_003_005_qt_zs"  onblur="zhszfd(this.value,'key_001_003_005_qt_zs','hidden_key_001_003_005_qt_zs',2,2)"   type="text" placeholder=""/>
						                     	<input class="form-control" id="key_001_003_005_qt_zs" value="${(key_001_003_005_qt_zs)!''}"  name="key_001_003_005_qt_zs"  type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										 -->
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control" id="hidden_key_001_003_005_qt_sn2" value="${(key_001_003_005_qt_sn2)!''}"  name="hidden_key_001_003_005_qt_sn2"    onblur="zhszfd(this.value,'key_001_003_005_qt_sn2','hidden_key_001_003_005_qt_sn2',2,2)"   type="text" placeholder=""/>
						                     	<input class="form-control" id="key_001_003_005_qt_sn2" value="${(key_001_003_005_qt_sn2)!''}"  name="key_001_003_005_qt_sn2"   type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
								</tbody>
							</table>
							</fieldset>
						</div>
						<div class="form-group " style="">
						       <div class="col-lg-10 controls has-feedback has-error">
						        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style="">除营业收入和净利润外，还可选填总资产、净资产或其他公司认为需要重点展示的财务指标。<br/>本报告所有金额单位：(${fbReport.unitEnumeration.name})</small>
						       </div>
			          	</div>
					</div>
				</div>
				</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {

	$('#hidden_key_001_003_yysr_bq').val(format2(Number('${key_001_003_yysr_bq!''}'),2,2));
	$('#hidden_key_001_003_yysr_sn1').val(format2(Number('${key_001_003_yysr_sn1!''}'),2,2));
	$('#hidden_key_001_003_yysr_zs').val(format2(Number('${key_001_003_yysr_zs!''}'),2,2));
	$('#hidden_key_001_003_yysr_sn2').val(format2(Number('${key_001_003_yysr_sn2!''}'),2,2));
	$('#hidden_key_001_003_gslmgsdjlr_bq').val(format2(Number('${key_001_003_gslmgsdjlr_bq!''}'),2,2));
	$('#hidden_key_001_003_gslmgsdjlr_sn1').val(format2(Number('${key_001_003_gslmgsdjlr_sn1!''}'),2,2));
	$('#hidden_key_001_003_gslmgsdjlr_zs').val(format2(Number('${key_001_003_gslmgsdjlr_zs!''}'),2,2));
	$('#hidden_key_001_003_gslmgsdjlr_sn2').val(format2(Number('${key_001_003_gslmgsdjlr_sn2!''}'),2,2));
	$('#hidden_key_001_003_005_qt_bq').val(format2(Number('${key_001_003_005_qt_bq!''}'),2,2));
	$('#hidden_key_001_003_005_qt_sn1').val(format2(Number('${key_001_003_005_qt_sn1!''}'),2,2));
	$('#hidden_key_001_003_005_qt_zs').val(format2(Number('${key_001_003_005_qt_zs!''}'),2,2));
	$('#hidden_key_001_003_005_qt_sn2').val(format2(Number('${key_001_003_005_qt_sn2!''}'),2,2));
	
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  hidden_key_001_003_yysr_bq: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	    	  hidden_key_001_003_yysr_sn1: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	          /*
	    	  hidden_key_001_003_yysr_zs: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	          */
	    	  hidden_key_001_003_yysr_sn2: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	    	  hidden_key_001_003_gslmgsdjlr_bq: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	    	  hidden_key_001_003_gslmgsdjlr_sn1: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	          /*
	    	  hidden_key_001_003_gslmgsdjlr_zs: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	          */
	    	  hidden_key_001_003_gslmgsdjlr_sn2: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	    	  hidden_key_001_003_005_qt_bq: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	    	  hidden_key_001_003_005_qt_sn1: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	          /*
	    	  hidden_key_001_003_005_qt_zs: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	          */
	    	  hidden_key_001_003_005_qt_sn2: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
			            	message: '输入的金额不正确',
	                      	callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	 	}
	                  }
	              }
	          },
	    	  key_001_003_005_qt_title: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    }
	              }
	          }
	      }
	  });
});




function submit_form(){

	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		return;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>