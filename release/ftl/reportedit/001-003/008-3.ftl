<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>主营业务发展-行业-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		
		<div class="div_form">
			<form class="form-horizontal" onSubmit="return false;" id="form" method="get" enctype="multipart/form-data">
				<div id="toolbar">
			        <button id="insertRow" class="btn btn-danger">
			            <i class="glyphicon"></i> 新增
			        </button>
			        <button id="remove" class="btn btn-danger" disabled>
			            <i class="glyphicon"></i> 删除
			        </button>
			    </div>
			 	<table id="table" data-click-to-select="true" style="padding: 0px;"></table>
		      	<div class="form-group " style="margin: 10px;">
		          	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
		      	</div>
	      	</form>
      	</div>
	</body>
</html>
<link href="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.css" rel="stylesheet">
<link href="${basePath}/resources/lib/bootstrap-table-examples-master/assets/bootstrap-table/bootstrap-editable.css" rel="stylesheet">
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse_state.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json2.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/cycle.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/mindmup-editabletable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.js"></script>
<script src="${basePath}/resources/lib/x-editable-master/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/dist/extensions/editable/bootstrap-table-editable.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/numeric-input-example.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript">
var $table;
var $insertRow = $('#insertRow');
var $remove = $('#remove');
var $tableRowsNum = 0;
$(function () {
    initTable();
});
//按下Enter搜索
function enterEvent(evt) {
    if (evt.keyCode == 13) {
        $("#btnSearch").click();
    }
}
        
function initTable() {
	var data =${(json_key_001_003_008_table)!'[]'};
        
        
    $table = $('#table').bootstrapTable({
        height: getHeight(),
        columns: [ 
    	{
            field: 'state',
            checkbox: true,
            align: 'center',
            height:20,
            valign: 'middle'
        }, {
            field: 'id',
            title: 'id',
            visible:false
        }, {
            field: 'json_key_001_003_008_fhy_',
            title: '分行业'
        }, {
            field: 'json_key_001_003_008_yysr_',
            title: '营业收入',
            formatter: format1
        }, {
            field: 'json_key_001_003_008_mlr_',
            title: '毛利率(%)',
            formatter: format1,
            editable: {
                type: 'text'
            }
        }],
       data: data,
       onPostBody: function () {
          	$('#table').editableTableWidget({editor: $('<textarea>')});
        }
    });
    $(".no-records-found").css("display","none");
    $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        selections = getIdSelections();
    });
    
    
    $remove.click(function () {
        var ids = getIdSelections();
        $table.bootstrapTable('remove', {
            field: 'id',
            values: ids
        });
    	$(".no-records-found").css("display","none");
        //$remove.prop('disabled', true);
    });
    
    $insertRow.click(function () {
    	$tableRowsNum++;
        $('#table').bootstrapTable('insertRow', {
        	index: $tableRowsNum ,
        	row: {
        		id:$tableRowsNum,
	        	json_key_001_003_008_fhy_:'',
	        	json_key_001_003_008_yysr_: '',
	        	json_key_001_003_008_mlr_: ''
            }
        });
    });
}

/**
 * 千分符格式化
 * 
 * @param num
 * @returns
 */
function format1(num) {
	if (num == null || num == undefined || num == "") {
		return "";
	}
	if (isNaN(num))
		return num;
	return (parseFloat(num).toFixed(2) + '').replace(
			/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}


function getRowsSelections() {
   return ($table.bootstrapTable('getOptions').data.length+1);
}

function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
    });
}

function getHeight() {
    //return $(window).height() - $('h1').outerHeight(true);
    return 300;
}
function operateFormatter(value, row, index) {
    return [
            '<a class="edit btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="编辑">',
                '<i class="icon-pencil">编辑</i>',
            '</a>',
            '<a class="remove btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="移除">',
                '<i class="icon-remove">移除</i>',
            '</a>'
        ].join('');
}


</script>
<script type="text/javascript" language="javascript">
function submit_form(){
	var _rb_tr = $("#table tr");
	if( _rb_tr.length>1){
		var _rb_data = {};
		_rb_data.reportType='${reportType!''}';
		_rb_data.reportIdstr='${reportIdstr!''}';
		_rb_data.pageIdstr='${pageName!''}';
		
		
		var jStr = "[";
		var i = 0 ;
		var s = false;
		$("#table tr").each(function (){
			var _rb_tr_d =  $(this).children("td").length;
			if( _rb_tr_d==4){
				s = true;
				//console.log( $(this).children("td").eq(0));
				var _rb_str='{';
		        _rb_str += '"id":"'+ i +'",';
		        _rb_str += '"json_key_001_003_008_fhy_":"'+$(this).children("td").eq(1).html()+'",';
		        _rb_str += '"json_key_001_003_008_yysr_":"'+$(this).children("td").eq(2).html()+'",';
		        _rb_str += '"json_key_001_003_008_mlr_":"'+$(this).children("td").eq(3).html()+'"';
				_rb_str+="},";
				i++;
				jStr+=(_rb_str);
			}
        });
        if(s){
	        if(jStr.substr(jStr.length-1,1)==","){
	        	jStr = jStr.substr(0,jStr.length-1);
	        }
		    jStr += " ]";
			//console.log(jStr);
		    _rb_data.json_key_001_003_008_table=jStr;
			submitAjax('${webPath}/reportsave/savedgnt',_rb_data);
        }else{
			j_tip('没有需要保存的数据','error');
        }
	}else{
		j_tip('没有需要保存的数据','error');
	}
}
</script>
