<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>研发投入-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data" style="padding-top: 0px;">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<div class="form-group " style="margin: 10px;">
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
							<legend>表单填写<span style="color: #a94442;font-size: 12px;">(与文本选填)</span></legend>
							<table class="table-condensed"  style="margin-left:0px;border: 1px solid #cccccc;width: 1060px;margin-left: auto;margin-right: auto;">
								<thead>
								</thead>
								<tbody>
									<tr  class="">
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrbqfy">${(fbReport.date!'')}年本期费用化研发投入(${fbReport.unitEnumeration.name})</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrbqfy" value="${(key_001_003_yftrbqfy)!''}" name="hidden_key_001_003_yftrbqfy" type="text" onblur="zhszfd(this.value,'key_001_003_yftrbqfy','hidden_key_001_003_yftrbqfy')"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrbqfy" value="${(key_001_003_yftrbqfy)!''}" name="key_001_003_yftrbqfy" type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrbqzbh">${(fbReport.date!'')}年本期资本化研发投入(${fbReport.unitEnumeration.name})</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrbqzbh" value="${(key_001_003_yftrbqzbh)!''}" name="hidden_key_001_003_yftrbqzbh" type="text" onblur="zhszfd(this.value,'key_001_003_yftrbqzbh','hidden_key_001_003_yftrbqzbh')"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrbqzbh" value="${(key_001_003_yftrbqzbh)!''}" name="key_001_003_yftrbqzbh" type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<tr  class="">
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrhj">${(fbReport.date!'')}年研发投入合计(${fbReport.unitEnumeration.name})</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrhj" value="${(key_001_003_yftrhj)!''}" name="hidden_key_001_003_yftrhj" type="text" onblur="zhszfd(this.value,'key_001_003_yftrhj','hidden_key_001_003_yftrhj')"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrhj" value="${(key_001_003_yftrhj)!''}" name="key_001_003_yftrhj" type="hidden"   />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrzezyysrbl">${(fbReport.date!'')}年研发投入总额占营业收入比例(%)</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrzezyysrbl" value="${(key_001_003_yftrzezyysrbl)!''}" name="hidden_key_001_003_yftrzezyysrbl" type="text" onblur="zhszfd(this.value,'key_001_003_yftrzezyysrbl','hidden_key_001_003_yftrzezyysrbl')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"   placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrzezyysrbl" value="${(key_001_003_yftrzezyysrbl)!''}" name="key_001_003_yftrzezyysrbl" type="hidden" />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<tr  class="">
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrrydsl">${(fbReport.date!'')}年公司研发人员的数量(人)</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrrydsl" value="${(key_001_003_yftrrydsl)!''}" name="hidden_key_001_003_yftrrydsl" type="text" onblur="zhszfd(this.value,'key_001_003_yftrrydsl','hidden_key_001_003_yftrrydsl',0)"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrrydsl" value="${(key_001_003_yftrrydsl)!''}" name="key_001_003_yftrrydsl" type="hidden"  />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrryslzgszrsdbl">${(fbReport.date!'')}年研发人员数量占公司总人数的比例(%)</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrryslzgszrsdbl" value="${(key_001_003_yftrryslzgszrsdbl)!''}" name="hidden_key_001_003_yftrryslzgszrsdbl" type="text" onblur="zhszfd(this.value,'key_001_003_yftrryslzgszrsdbl','hidden_key_001_003_yftrryslzgszrsdbl')"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrryslzgszrsdbl" value="${(key_001_003_yftrryslzgszrsdbl)!''}" name="key_001_003_yftrryslzgszrsdbl" type="hidden"  />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<!-- 
									<tr  class="">
										<td>
											<label class="col-sm-2 control-label" for="key_001_003_yftrzbhdbz">${(fbReport.date!'')}年研发投入资本化的比重(%)</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_yftrzbhdbz" value="${(key_001_003_yftrzbhdbz)!''}" name="hidden_key_001_003_yftrzbhdbz" type="text" onblur="zhszfd(this.value,'key_001_003_yftrzbhdbz','hidden_key_001_003_yftrzbhdbz')" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"   placeholder="(表单与文本选填)"/>
												<input class="form-control " id="key_001_003_yftrzbhdbz" value="${(key_001_003_yftrzbhdbz)!''}" name="key_001_003_yftrzbhdbz" type="hidden"  />
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									-->
								</tbody>
							</table>
							</fieldset>
							<div style="height:10px;"></div>
							<fieldset>
								<legend>文本填写<span style="color: #a94442;font-size: 12px;">(与表单选填)</span></legend>
								<div class="form-group " style="margin: 10px;">
				                   	<div class="col-sm-12 controls">
				                   		<textarea class="form-control input_text" rows="5" id="key_001_003_yftr_txt" name="key_001_003_yftr_txt"   placeholder="(选填)">${(key_001_003_yftr_txt)!''}</textarea>
				                   	</div>
				              	</div>
							</fieldset>
						</div>
						<div class="form-group " style="width: 60%;margin-right: auto;margin-left: auto;display:none;">
						       <div class="col-lg-10 controls has-feedback has-error">
						        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style="">本页表单和文本填写可二选一，也可都填.</small>
						       </div>
			          	</div>
					</div>
				</div>
				</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {

	$('#hidden_key_001_003_yftrbqfy').val(format2(Number('${key_001_003_yftrbqfy!''}'),2));
	$('#hidden_key_001_003_yftrbqzbh').val(format2(Number('${key_001_003_yftrbqzbh!''}'),2));
	$('#hidden_key_001_003_yftrhj').val(format2(Number('${key_001_003_yftrhj!''}'),2));
	$('#hidden_key_001_003_yftrzezyysrbl').val(format2(Number('${key_001_003_yftrzezyysrbl!''}'),2));
	$('#hidden_key_001_003_yftrrydsl').val(format2(Number('${key_001_003_yftrrydsl!''}'),0));
	$('#hidden_key_001_003_yftrryslzgszrsdbl').val(format2(Number('${key_001_003_yftrryslzgszrsdbl!''}'),2));
	//$('#hidden_key_001_003_yftrzbhdbz').val(format2(Number('${key_001_003_yftrzbhdbz!''}'),2));
	
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  hidden_key_001_003_yftrbqfy: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '表单数据与文本至少需要填写一项完整的信息',
	                      callback: function(value, validator) {
	                      		if(value == null || value==""){
									var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
									if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
										return false;
									}
									return true;
	                      		}
								var v = $("#hidden_key_001_003_yftrbqzbh").val();
								if(v==null || v==''){
									v = 0;
								}else{
									v = Number(v.replace(/[^0-9.]/g,''));
								}
								v = Number(value.replace(/[^0-9.]/g,''))+Number(v);
								$("#key_001_003_yftrhj").val(v);
								$('#hidden_key_001_003_yftrhj').val(format2(Number(v),2));
								
								
                          		return true ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_yftrbqzbh: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '表单数据与文本至少需要填写一项完整的信息',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="" ){
									var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
									if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
										return false;
									}
									return true;
	                      		}
								
								var v = $("#hidden_key_001_003_yftrbqfy").val();
								if(v==null  || v==''){
									v = 0;
								}else{
									v = Number(v.replace(/[^0-9.]/g,''));
								}
								
								v = Number(value.replace(/[^0-9.]/g,''))+Number(v);
								$("#key_001_003_yftrhj").val(v);
								$('#hidden_key_001_003_yftrhj').val(format2(Number(v),2));
								
                          		return true ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_yftrhj: {
	              group: '.controls',
	              validators: {
	                 	callback: {
			                  message: '表单数据与文本至少需要填写一项完整的信息',
			                  callback: function(value, validator) {
			                  		if(value == null || value=="" ){
										var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
										if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
											return false;
										}
										return true;
			                  		}
			                  		return true ;
			            	  }
		               	}
	              }
	          },
	    	  hidden_key_001_003_yftrzezyysrbl: {
	              group: '.controls',
	              validators: {
	                    between:{
	                     	 message: '比列范围为0~100(包含0和100)',
				             min: '0.00',
				             max: '100.00'
	                    },
	                 	callback: {
			                  message: '表单数据与文本至少需要填写一项完整的信息',
			                  callback: function(value, validator) {
			                  		if(value == null || value=="" ){
										var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
										if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
											return false;
										}
										return true;
			                  		}
			                  		return true ;
			            	  }
		               	}
	              }
	          },
	    	  hidden_key_001_003_yftrrydsl: {
	              group: '.controls',
	              validators: {
	                    numeric:{
	                     	 message: '只能填写数字或者小数点'
	                    },
	                 	callback: {
			                  message: '表单数据与文本至少需要填写一项完整的信息',
			                  callback: function(value, validator) {
			                  		if(value == null || value=="" ){
										var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
										if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
											return false;
										}
										return true;
			                  		}
			                  		return true ;
			            	  }
		               	}
	              }
	          },
	    	  hidden_key_001_003_yftrryslzgszrsdbl: {
	              group: '.controls',
	              validators: {
	                    between:{
	                     	 message: '比列范围为0~100(包含0和100)',
				             min: '0.00',
				             max: '100.00'
	                    },
	                 	callback: {
			                  message: '表单数据与文本至少需要填写一项完整的信息',
			                  callback: function(value, validator) {
			                  		if(value == null || value=="" ){
										var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
										if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
											return false;
										}
										return true;
			                  		}
			                  		return true ;
			            	  }
		               	}
	              }
	          }
	       }
	  });
});




function submit_form(){
	var key_001_003_yftr_txt = $("#key_001_003_yftr_txt").val();
	if( key_001_003_yftr_txt == null || key_001_003_yftr_txt == undefined || key_001_003_yftr_txt.length == 0 ){
		$('#form').bootstrapValidator('validate')
		if(!$('#form').data('bootstrapValidator').isValid()){
			return;
		}
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>