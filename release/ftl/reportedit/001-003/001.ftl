<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>封面-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<style>
		  	.file-preview{
		  		width:290px!important;
		  	}
  		</style>
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<input id="img_key_" name="img_key_" type="hidden" value="img_key_001_003_gslogo">
				<div style="width: 1060px;">
					<div class="form-group " style="margin: 10px;">
	                   	<label class="col-sm-2 control-label" for="key_001_003_ggmc">公司全称<span style="color:red;padding:2px;">*</span></label>
	                   	<div class="col-sm-10 controls">
	                     	<input maxlength="20" class="form-control input_text" id="key_001_003_ggmc" value="${(key_001_003_ggmc)!'${(session_company)}'}" name="key_001_003_ggmc" type="text" placeholder="最多允许输入20个字(必填)"/>
	                   	</div>
	              	</div>
	              	<div class="form-group " style="margin: 10px;">
	                   	<label class="col-sm-2 control-label" for="key_001_003_gpdm">股票代码<span style="color:red;padding:2px;">*</span></label>
	                   	<div class="col-sm-10 controls">
	                     	<input maxlength="6" class="form-control input_text" id="key_001_003_gpdm" value="${(key_001_003_gpdm)!''}" name="key_001_003_gpdm" type="text" placeholder="请填写6位数字(必填)"/>
	                   	</div>
	              	</div>
	              	<div class="form-group " style="margin: 10px;">
	                  	<label class="col-sm-2 control-label" for="img_key_001_003_gslogo">公司LOGO<span style="color:red;padding:2px;">*</span></label>
	                  	<div class="col-sm-10 controls">
	                     	<input class="form-control" id="img_key_001_003_gslogo" name="img_key_001_003_gslogo"  type="file" placeholder="上传公司LOGO(必填)"/>
	                     	<input class="form-control" id="hidden_img_key_001_003_gslogo" name="hidden_img_key_001_003_gslogo" value="${img_key_001_003_gslogo!''}" type="hidden" placeholder="上传公司LOGO(必填)"/>
	                  	</div>
	              	</div>
		          	<div class="form-group " style="margin: 10px;">
		          		<label class="col-sm-2 control-label" for="validate_number_key_001_003_001"></label>
				        <div class="col-lg-10 controls has-feedback has-error">
				        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_001" data-bv-result="INVALID" style="">建议图片像素尺寸：4:3</small>
				       </div>
		          	</div>
	              	<div class="form-group " style="margin: 10px;">
	                  	<div class="div_input_submit">
							<input name="submit" type="button" onclick="submit_form()" value="保存">
						</div>
	              	</div>
				</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput_locale_zh.js"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_fileinput.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(function () {
    //0.初始化fileinput
    var _rb_oFileInput1 = new FileInput();
    _rb_oFileInput1.Init("img_key_001_003_gslogo", '${basePath}', "${img_key_001_003_gslogo!''}");
});
</script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  key_001_003_ggmc: {
	              group: '.controls',
	              validators: {
	                  notEmpty: {
	                      message: '公司全称不可为空'
	                  }
	              }
	          },
	    	  key_001_003_gpdm: {
	              validators: {
	                  callback: {
	                      message: '股票代码必须为6位的数字',
	                      callback: function(value, validator) {
								var re = /[^0-9]/g;
                          		return !re.test(value) && value!=null && value.length==6;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_gsmnndbg: {
	              group: '.controls',
	              validators: {
	                  notEmpty: {
	                      message: '${(fbReport.date)}年年度报告不可为空'
	                  }
	              }
	          }
	      }
	  });

});


function submit_form(){
	//表单验证
	//$('#form').bootstrapValidator('validate')
	//if(!$('#form').data('bootstrapValidator').isValid()){
	//	return;
	//}
	
	var img_key_001_003_gslogo = $("#img_key_001_003_gslogo").val();
	if( img_key_001_003_gslogo == undefined || img_key_001_003_gslogo ==null || img_key_001_003_gslogo.length==0){
		var hidden_img_key_001_003_gslogo = $("#hidden_img_key_001_003_gslogo").val();
		if( hidden_img_key_001_003_gslogo == undefined || hidden_img_key_001_003_gslogo ==null || hidden_img_key_001_003_gslogo.length==0){
			j_tip("公司LOGO不能为空","error");
			return false ;
		}else{
			var v = $(".file-error-message").css("display");
			if( v == "block"){
				j_tip('请上传正确的图片','error');
				return false
			}
		}
	}else{
		var suppotFile = new Array();
			suppotFile[0] = "jpg";
			suppotFile[1] = "gif";
			suppotFile[2] = "bmp";
			suppotFile[3] = "png";
			suppotFile[4] = "jpeg";
		if(!isFile(img_key_001_003_gslogo,suppotFile)){
			j_tip('上传的文件格式不正确！上传格式有(.gif、.jpg、.png、.bmp、.jpeg)','error');
			return false;
		}
	}
	
	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		j_tip('验证不通过','error');
		return false;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}

</script>