<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>公司经营-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<div class="form-group " style="margin: 10px;">
                   	<label class="col-sm-3 controls" for="key_001_003_zlhxjzl">
                     	<input class="form-control" id="key_001_003_zlhxjzl_title" value="${(key_001_003_zlhxjzl_title)!''}"  name="key_001_003_zlhxjzl_title" type="text" placeholder="(可选)"/>
                  	</label>
                   	<div class="col-sm-9 controls">
                   		<textarea class="form-control input_text" rows="2" id="key_001_003_zlhxjzl" name="key_001_003_zlhxjzl"  placeholder="(具体内容)">${(key_001_003_zlhxjzl)!''}</textarea>
                   	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-3 controls" for="key_001_003_zbscbx">
                     	<input class="form-control" id="key_001_003_zbscbx_title" value="${(key_001_003_zbscbx_title)!''}"  name="key_001_003_zbscbx_title" type="text" placeholder="(可选)"/>
                  	</label>
                  	<div class="col-sm-9 controls">
                  		<textarea class="form-control input_text" rows="2" id="key_001_003_zbscbx" name="key_001_003_zbscbx" placeholder="(具体内容)">${(key_001_003_zbscbx)!''}</textarea>
                  	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-3 controls" for="key_001_003_ygcgzrz">
                     	<input class="form-control" id="key_001_003_ygcgzrz_title" value="${(key_001_003_ygcgzrz_title)!''}"  name="key_001_003_ygcgzrz_title" type="text" placeholder="(可选)"/>
                  	</label>
                  	<div class="col-sm-9 controls">
                  		<textarea class="form-control input_text" rows="2" id="key_001_003_ygcgzrz" name="key_001_003_ygcgzrz" placeholder="(具体内容)">${(key_001_003_ygcgzrz)!''}</textarea>
                  	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-3 controls" for="key_001_003_xcpxsc">
                     	<input class="form-control" id="key_001_003_xcpxsc_title" value="${(key_001_003_xcpxsc_title)!''}"  name="key_001_003_xcpxsc_title" type="text" placeholder="(可选)"/>
                  	</label>
                  	<div class="col-sm-9 controls">
                  		<textarea class="form-control input_text" rows="2" id="key_001_003_xcpxsc" name="key_001_003_xcpxsc" placeholder="(具体内容)">${(key_001_003_xcpxsc)!''}</textarea>
                  	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-3 controls" for="key_001_003_rxzs">
                     	<input class="form-control" id="key_001_003_rxzs_title" value="${(key_001_003_rxzs_title)!''}"  name="key_001_003_rxzs_title" type="text" placeholder="(可选)"/>
                  	</label>
                  	<div class="col-sm-9 controls">
                  		<textarea class="form-control input_text" rows="2" id="key_001_003_rxzs" name="key_001_003_rxzs" placeholder="(具体内容)">${(key_001_003_rxzs)!''}</textarea>
                  	</div>
              	</div>
	          	<div class="form-group " style="margin-left: 10px;">
			        <div class="col-lg-11 controls has-feedback has-error">
			        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_003" data-bv-result="INVALID" style="">至少需要填写3项<br/>本页公司经营，公司可选取本年度除财务数据外的其他亮点事项，如：公司战略、核心竞争力、内控、风控、公司治理，获得行业认证（ISO、GMP、FDA等）、入选指数、股价表现、评级情况、再融资、员工持股/股权激励、并购、重组等</small>
			       </div>
	          	</div>
	          	<div class="clear"></div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
		
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  key_001_003_zlhxjzl_title: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写8个字符',
	                      callback: function(value, validator) {
	                      		return value.length<9;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_zbscbx_title: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写8个字符',
	                      callback: function(value, validator) {
	                      		return value.length<9;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_ygcgzrz_title: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写8个字符',
	                      callback: function(value, validator) {
	                      		return value.length<9;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_xcpxsc_title: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写8个字符',
	                      callback: function(value, validator) {
	                      		return value.length<9;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_rxzs_title: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写8个字符',
	                      callback: function(value, validator) {
	                      		return value.length<9;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_zlhxjzl: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写100个字符，并且必须先填写对应标题项',
	                      callback: function(value, validator) {
	                      		var v = $("#key_001_003_zlhxjzl_title").val();
	                      		if( v == undefined || v == null || v.length==0){
	                      			if( value == undefined || value == null || value.length==0){
	                      				return true;
	                      			}
	                      			return false;
	                      		}
	                      		return value.length<101;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_zbscbx: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写100个字符，并且必须先填写对应标题项',
	                      callback: function(value, validator) {
	                      		var v = $("#key_001_003_zbscbx_title").val();
	                      		if( v == undefined || v == null || v.length==0){
	                      			if( value == undefined || value == null || value.length==0){
	                      				return true;
	                      			}
	                      			return false;
	                      		}
	                      		return value.length<101;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_ygcgzrz: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写100个字符，并且必须先填写对应标题项',
	                      callback: function(value, validator) {
	                      		var v = $("#key_001_003_ygcgzrz_title").val();
	                      		if( v == undefined || v == null || v.length==0){
	                      			if( value == undefined || value == null || value.length==0){
	                      				return true;
	                      			}
	                      			return false;
	                      		}
	                      		return value.length<101;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_xcpxsc: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写100个字符，并且必须先填写对应标题项',
	                      callback: function(value, validator) {
	                      		var v = $("#key_001_003_xcpxsc_title").val();
	                      		if( v == undefined || v == null || v.length==0){
	                      			if( value == undefined || value == null || value.length==0){
	                      				return true;
	                      			}
	                      			return false;
	                      		}
	                      		return value.length<101;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_rxzs: {
	              group: '.controls',
	              validators: {
	                    callback: {
	                      message: '最多允许填写100个字符，并且必须先填写对应标题项',
	                      callback: function(value, validator) {
	                      		var v = $("#key_001_003_rxzs_title").val();
	                      		if( v == undefined || v == null || v.length==0){
	                      			if( value == undefined || value == null || value.length==0){
	                      				return true;
	                      			}
	                      			return false;
	                      		}
	                      		return value.length<101;
                    	  }
	                  }
	              }
	          }
	      }
	  });
});

function submit_form(){
	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		j_tip('保存失败,请检查是否有漏填项.','error');
		return;
	}
	
	var key_001_003_zlhxjzl = $('#key_001_003_zlhxjzl').val();
	var key_001_003_zbscbx = $('#key_001_003_zbscbx').val();
	var key_001_003_ygcgzrz = $('#key_001_003_ygcgzrz').val();
	var key_001_003_xcpxsc = $('#key_001_003_xcpxsc').val();
	var key_001_003_rxzs = $('#key_001_003_rxzs').val();
	var i =0;
	if( key_001_003_zlhxjzl!=null && $.trim(key_001_003_zlhxjzl).length>0){
		i++;
	}
	if( key_001_003_zbscbx!=null && $.trim(key_001_003_zbscbx).length>0){
		i++;
	}
	if( key_001_003_ygcgzrz!=null && $.trim(key_001_003_ygcgzrz).length>0){
		i++;
	}
	if( key_001_003_xcpxsc!=null && $.trim(key_001_003_xcpxsc).length>0){
		i++;
	}
	if( key_001_003_rxzs!=null && $.trim(key_001_003_rxzs).length>0){
		i++;
	}
	if(i<3){
		j_tip('至少填写3项','error');
		return false;
	}
	
	
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>