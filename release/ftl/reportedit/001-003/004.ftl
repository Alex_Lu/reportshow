<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>获奖情况-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" onSubmit="return false;" id="form" method="get" enctype="multipart/form-data">
				<div class="row-fluid" style="width: 1060px;margin-left: auto;margin-right: auto;">
					<div class="span12">
						<div id="toolbar">
					        <button id="insertRow" class="btn btn-danger">
					            <i class="glyphicon"></i> 新增
					        </button>
					        <button id="remove" class="btn btn-danger" disabled>
					            <i class="glyphicon"></i> 删除
					        </button>
					    </div>
					 	<table id="table" data-click-to-select="true" style="padding: 0px;"></table>
			          	<div class="form-group " style="">
					        <div class="col-lg-10 controls has-feedback has-error">
					        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_004" data-bv-result="INVALID" style="">可填写1到3条数据</small>
					       </div>
			          	</div>
				      	<div class="form-group " style="margin: 10px;">
				          	<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="保存">
							</div>
				      	</div>
					</div>
		      	</div>
	      	</form>
      	</div>
	</body>
</html>
<link href="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.css" rel="stylesheet">
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse_state.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json2.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/cycle.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/mindmup-editabletable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/numeric-input-example.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript">
var $table;
var $insertRow = $('#insertRow');
var $remove = $('#remove');
var $tableRowsNum = 0;
$(function () {
    initTable();
    $tableRowsNum =  $('#table tr').length;
});
//按下Enter搜索
function enterEvent(evt) {
    if (evt.keyCode == 13) {
        $("#btnSearch").click();
    }
}
        
function initTable() {
	var data =${(json_key_001_003_table_hjqk)!'[]'};
        
    $table = $('#table').bootstrapTable({
        //height: getHeight(),
        columns: [ 
    	{
            field: 'state',
            checkbox: true,
            align: 'center',
            height:20,
            valign: 'middle'
        }, {
            field: 'id',
            title: 'id',
            visible:false
        }, {
            field: 'json_key_001_003_gjqk_',
            title: '获奖情况'
        }],
       data: data,
       onPostBody: function () {
          	$('#table').editableTableWidget({editor: $('<textarea>')});
        }
    });
    $(".no-records-found").css("display","none");
    $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        selections = getIdSelections();
    });
    
    
    $remove.click(function () {
    	update_tabledata();
        var ids = getIdSelections();
        $table.bootstrapTable('remove', {
            field: 'id',
            values: ids
        });
    	$(".no-records-found").css("display","none");
    	tiggerOnEven_updateCell();
		tiggerOnEven_display();
        //$remove.prop('disabled', true);
    });
    
    $insertRow.click(function () {
    	if( $('#table tr').length>3){
			j_tip('最多允许添加3条数据','error');
    		return;
    	}
    	update_tabledata();
    	$tableRowsNum++;
    	tiggerOnEven_updateCell();
        $('#table').bootstrapTable('insertRow', {
        	index: $tableRowsNum ,
        	row: {
        		id:$tableRowsNum,
	        	json_key_001_003_gjqk_: ''
            }
        });
		tiggerOnEven_display();
		tiggerOnEven_validate();
    });
	tiggerOnEven_display();
	tiggerOnEven_validate();
}


function update_tabledata(){
	var _rb_tr = $("#table tr");
	var data = $table.bootstrapTable('getOptions').data;
	var i = 0;
	console.log(data);
	$("table tr").each(function (){
		var d = data[i];
		if( d == undefined ){
			return false;
		}
		var _rb_tr_d =  $(this).children("td").length;
		if( _rb_tr_d==2){
		 	d.id=i;
		 	d.json_key_001_003_gjqk_=$(this).children("td").eq(1).html();
			i ++;
		}
 	});	
	console.log(data);
}


function tiggerOnEven_updateCell(){
    $('#table td').on('change', function(evt, newValue) {
    	if($(this).attr("class")=="bs-checkbox "){//过滤不需要记录的
			return false;
		}
		if( newValue == undefined ){
			return true;
		}
		$('#table').bootstrapTable('updateCell',{
			index:$("#table").find("tr").index(($(this).parent()))-1,
			field:'json_key_001_003_gjqk_',
			value:newValue
		});
		tiggerOnEven_updateCell();
		tiggerOnEven_display();
		tiggerOnEven_validate();
		//console.log($("table").find("tr").index(($(this).parent())));
		//alert($("#table").find("tr").index(($(this).parent())));
		//console.log($table.bootstrapTable('getOptions').data);
		return true;
	});
}



function tiggerOnEven_validate(){
    $('#table td').on('validate', function(evt, newValue) {
		if( newValue != null && newValue.length>30){
			j_tip('获奖内容过长，不能超过30个字符,已超过'+(newValue.length-30)+'字','error');
	    	return false;
		}
	});
}

function tiggerOnEven_display(){
    $('#table td').on('click keypress dblclick', function(evt, newValue) {
		if($(this).attr("class")=="bs-checkbox "){//过滤不需要记录的
			return false;
		}
	});
}

/**
 * 千分符格式化
 * 
 * @param num
 * @returns
 */
function format1(num) {
	if (num == null || num == undefined || num == "") {
		return "";
	}
	if (isNaN(num))
		return num;
	return (parseFloat(num).toFixed(2) + '').replace(
			/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}


function getRowsSelections() {
   return ($table.bootstrapTable('getOptions').data.length+1);
}

function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
    });
}

function getHeight() {
    //return $(window).height() - $('h1').outerHeight(true);
    return 210;
}
function operateFormatter(value, row, index) {
    return [
            '<a class="edit btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="编辑">',
                '<i class="icon-pencil">编辑</i>',
            '</a>',
            '<a class="remove btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="移除">',
                '<i class="icon-remove">移除</i>',
            '</a>'
        ].join('');
}


</script>
<script type="text/javascript" language="javascript">
function submit_form(){
	//console.log($table.bootstrapTable('getOptions').data);
	update_tabledata();
	var data = $table.bootstrapTable('getOptions').data;
	//console.log(data);
	if( data != undefined && data.length>0){
		var _rb_data = {};
		_rb_data.reportType='${reportType!''}';
		_rb_data.reportIdstr='${reportIdstr!''}';
		_rb_data.pageIdstr='${pageName!''}';
		var jStr = "[";
		for(var i = 0;i<data.length;i++){
			var tem = data[i];
			if( tem.json_key_001_003_gjqk_ == null ||  tem.json_key_001_003_gjqk_  ==undefined|| tem.json_key_001_003_gjqk_.length==0){
				continue;
			}
			var _rb_str='{';
	        _rb_str += '"id":"'+ i +'",';
	        _rb_str += '"json_key_001_003_gjqk_":"'+tem.json_key_001_003_gjqk_+'"';
			_rb_str+="},";
			jStr+=(_rb_str);
		}
		if(jStr.substr(jStr.length-1,1)==","){
        	jStr = jStr.substr(0,jStr.length-1)+ " ]";
			//console.log(jStr);
		    _rb_data.json_key_001_003_table_hjqk=jStr;
			submitAjax('${webPath}/reportsave/savedgnt',_rb_data);
			return;
        }
	}
	j_tip('没有需要保存的数据','error');
}
</script>
