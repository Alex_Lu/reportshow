<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>财务指标-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<div class="form-group " style="margin: 10px;">
                   	<label class="col-sm-2 control-label" for="key_001_003_018_mgsy">每股收益<span style="color:red;padding:2px;">*</span></label>
                   	<div class="col-sm-10 controls">
                     	<input class="form-control input_text" id="key_001_003_018_mgsy" value="${(key_001_003_018_mgsy)!''}" name="key_001_003_018_mgsy" type="text" placeholder="(必填)"/>
                   	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-2 control-label" for="key_001_003_018_pjjzcsyl">平均净资产收益率<span style="color:red;padding:2px;">*</span></label>
                  	<div class="col-sm-10 controls">
                     	<input class="form-control" id="key_001_003_018_pjjzcsyl" value="${(key_001_003_018_pjjzcsyl)!''}" name="key_001_003_018_pjjzcsyl"  type="text" placeholder="(必填)"/>
                  	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-2 control-label" for="key_001_003_018_pjzzcsyl">平均总资产收益率<span style="color:red;padding:2px;">*</span></label>
                  	<div class="col-sm-10 controls">
                     	<input class="form-control" id="key_001_003_018_pjzzcsyl" value="${(key_001_003_018_pjzzcsyl)!''}"  name="key_001_003_018_pjzzcsyl" type="text" placeholder="(必填)"/>
                  	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
function submit_form(){
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>