<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>利润分配-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-2 control-label" for="key_001_003_msgpxs">每十股派息数(元)(含税)</label>
                  	<div class="col-sm-10 controls">
                     	<input class="form-control" id="key_001_003_msgpxs" value="${(key_001_003_msgpxs)!''}" name="key_001_003_msgpxs"  type="text" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" placeholder="(选填)"/>
                  	</div>
              	</div>
				<div class="form-group " style="margin: 10px;">
                   	<label class="col-sm-2 control-label" for="key_001_003_msgshgs">每十股送红股数(股)</label>
                   	<div class="col-sm-10 controls">
                     	<input class="form-control input_text" id="key_001_003_msgshgs" value="${(key_001_003_msgshgs)!''}" name="key_001_003_msgshgs" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" type="text" placeholder="(选填)"/>
                   	</div>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-2 control-label" for="key_001_003_msgzzs">每十股转增数(股)</label>
                  	<div class="col-sm-10 controls">
                     	<input class="form-control" id="key_001_003_msgzzs" value="${(key_001_003_msgzzs)!''}"  name="key_001_003_msgzzs" type="text" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  placeholder="(选填)"/>
                  	</div>
              	</div>
	          	<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-2 control-label" for="validate_number_key_001_003_"></label>
			        <div class="col-lg-10 controls has-feedback has-error">
			        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_" data-bv-result="INVALID" style="">选填其中的0项或多项</small>
			       </div>
	          	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
function submit_form(){
	/*
	var key_001_003_msgshgs = $('#key_001_003_msgshgs').val();
	var key_001_003_msgpxs = $('#key_001_003_msgpxs').val();
	var key_001_003_msgzzs = $('#key_001_003_msgzzs').val();
	var i =0;
	if( key_001_003_msgshgs!=null && $.trim(key_001_003_msgshgs).length>0){
		i++;
	}
	if( key_001_003_msgpxs!=null && $.trim(key_001_003_msgpxs).length>0){
		i++;
	}
	if( key_001_003_msgzzs!=null && $.trim(key_001_003_msgzzs).length>0){
		i++;
	}
	if(i<1){
		j_tip('至少填写1项','error');
		return false;
	}
	*/

	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>