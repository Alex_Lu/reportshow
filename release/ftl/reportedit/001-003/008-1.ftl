<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>主营业务发展-行业-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		
		<div class="div_form">
			<form class="form-horizontal" onSubmit="return false;" id="form" method="get" enctype="multipart/form-data">
				<div id="toolbar">
			        <button id="insertRow" class="btn btn-danger">
			            <i class="glyphicon"></i> 新增
			        </button>
			        <button id="remove" class="btn btn-danger" disabled>
			            <i class="glyphicon"></i> 删除
			        </button>
			    </div>
			 	<table id="table" data-click-to-select="true" style="padding: 0px;"></table>
		      	<div class="form-group " style="margin: 10px;">
		          	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
		      	</div>
	      	</form>
      	</div>
	</body>
</html>
<link href="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.css" rel="stylesheet">
<link href="${basePath}/resources/lib/bootstrap-table-examples-master/assets/bootstrap-table/bootstrap-editable.css" rel="stylesheet">
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse_state.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json2.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/cycle.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/mindmup-editabletable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.js"></script>
<script src="${basePath}/resources/lib/x-editable-master/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/dist/extensions/editable/bootstrap-table-editable.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/numeric-input-example.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript">
var $table;
var $insertRow = $('#insertRow');
var $remove = $('#remove');
var $tableRowsNum = 0;
$(function () {
    initTable();
});
//按下Enter搜索
function enterEvent(evt) {
    if (evt.keyCode == 13) {
        $("#btnSearch").click();
    }
}
        
function initTable() {
	var data = [{
            id: 1,
        	json_key_001_003_008_fhy_: 'Item 1',
        	json_key_001_003_008_yysr_: '123',
        	json_key_001_003_008_yycb_: 'Item 1',
        	json_key_001_003_008_mlr_: '123',
        	json_key_001_003_008_yysrbsnzj_: 'Item 1',
        	json_key_001_003_008_yycbbsnzj_: '123',
        	json_key_001_003_008_mlrbsnzj_: '123'
       }]
        
        
    $table = $('#table').bootstrapTable({
        height: getHeight(),
        columns: [ 
    	{
            field: 'state',
            checkbox: true,
            align: 'center',
            height:20,
            valign: 'middle'
        }, {
            field: 'json_key_001_003_008_fhy_',
            title: '分行业'
        }, {
            field: 'json_key_001_003_008_yysr_',
            title: '营业收入'
        }, {
            field: 'json_key_001_003_008_yycb_',
            title: '营业成本'
        }, {
            field: 'json_key_001_003_008_mlr_',
            title: '毛利率(%)'
        }, {
            field: 'json_key_001_003_008_yysrbsnzj_',
            title: '年收入比上年增减(%)'
        }, {
            field: 'json_key_001_003_008_yycbbsnzj_',
            title: '营业成本比上年增减(%)'
        }, {
            field: 'json_key_001_003_008_mlrbsnzj_',
            title: '毛利率比上年增减(%)'
        }],
       data: data,
       onPostBody: function () {
          	$('#table').editableTableWidget({editor: $('<textarea>')});
        }
    });
    $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        selections = getIdSelections();
    });
    
    
    $remove.click(function () {
        var ids = getIdSelections();
        $table.bootstrapTable('remove', {
            field: 'id',
            values: ids
        });
        //$remove.prop('disabled', true);
    });
    
    $insertRow.click(function () {
    	$tableRowsNum++;
        $('#table').bootstrapTable('insertRow', {
        	index: $tableRowsNum ,
        	row: {
        		id:$tableRowsNum,
	        	json_key_001_003_008_fhy_:$tableRowsNum,
	        	json_key_001_003_008_yysr_: '',
	        	json_key_001_003_008_yycb_: '',
	        	json_key_001_003_008_mlr_: '',
	        	json_key_001_003_008_yysrbsnzj_: '',
	        	json_key_001_003_008_yycbbsnzj_: '',
	        	json_key_001_003_008_mlrbsnzj_: ''
            }
        });
    });
}

function getRowsSelections() {
   return ($table.bootstrapTable('getOptions').data.length+1);
}

function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
    });
}

function getHeight() {
    //return $(window).height() - $('h1').outerHeight(true);
    return 300;
}
function operateFormatter(value, row, index) {
    return [
            '<a class="edit btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="编辑">',
                '<i class="icon-pencil">编辑</i>',
            '</a>',
            '<a class="remove btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="移除">',
                '<i class="icon-remove">移除</i>',
            '</a>'
        ].join('');
}


window.operateEvents = {
    'click .like': function (e, value, row, index) {
        alert(row.id);
    },
    'click .edit': function (e, value, row, index) {
    
		alert(4);
    	/*
        var url = '@Url.Content("~/Welcome/UpdateRecord/")' + row.id + '?rnd=' + Math.random();
        art.dialog.open(url, { title: "编辑客户信息", id: 'detail', width: 500, height: 520, lock: true, opacity: 0.3, close: function () { $table.bootstrapTable('refresh'); } });
        */
    },
    'click .remove': function (e, value, row, index) {
		alert(5);
    	/*
        mif.showQueryMessageBox("将删除本条记录，是否确认删除？", function () {
            var url = '@Url.Content("~/Welcome/DeleteRecord/")' + row.id + '?rnd=' + Math.random();
            mif.ajax(url, null, afterDelete);
        });
        */
    }
};
</script>
<script type="text/javascript" language="javascript">
function submit_form(){
	//console.log($table.bootstrapTable('getOptions').data[0].json_key_001_003_008_mlr_);
	var _rb_data = {};
	//_rb_data.json_key_001_003_008_table=$table.bootstrapTable('getOptions').data;
	_rb_data.reportType='${reportType!''}';
	_rb_data.reportIdstr='${reportIdstr!''}';
	_rb_data.pageIdstr='${pageName!''}';
	 var d = $table.bootstrapTable('getOptions').data;
	var jStr = "{ ";
	for(var i =0;i<d.length;i++){
		var item = d[i];
		console.log(item);
		var _rb_str="{";
        _rb_str += "'id':'"+item.id+"',";
        _rb_str += "'json_key_001_003_008_fhy_':'"+item.json_key_001_003_008_fhy_+"',";
        _rb_str += "'json_key_001_003_008_yysr_':'"+item.json_key_001_003_008_yysr_+"',";
        _rb_str += "'json_key_001_003_008_yycb_':'"+item.json_key_001_003_008_yycb_+"',";
        _rb_str += "'json_key_001_003_008_mlr_':'"+item.json_key_001_003_008_mlr_+"',";
        _rb_str += "'json_key_001_003_008_yysrbsnzj_':'"+item.json_key_001_003_008_yysrbsnzj_+"',";
        _rb_str += "'json_key_001_003_008_yycbbsnzj_':'"+item.json_key_001_003_008_yycbbsnzj_+"',";
        _rb_str += "'json_key_001_003_008_mlrbsnzj_':'"+item.json_key_001_003_008_mlrbsnzj_+"'";
		_rb_str+="}";
		if( i<d.length-1){
			_rb_str+=",";
		}
		jStr+=_rb_str;
	}
    jStr += " }";
	console.log(jStr);
    _rb_data.json_key_001_003_008_table=jStr;
	//submitAjax('${webPath}/reportsave/savedgnt',_rb_data);
}
</script>
