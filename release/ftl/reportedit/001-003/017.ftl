<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>投资者关系管理-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<style>
		  	.file-preview{
		  		width:290px!important;
		  	}
  		</style>
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" onSubmit="return false;" id="form" method="get" enctype="multipart/form-data">
				<div class="row-fluid" style="width: 1060px;margin-left: auto;margin-right: auto;">
					<div class="span12" style="">
						<fieldset>
                    	<legend>IR日历</legend>
						<div id="toolbar">
					        <button id="insertRow" class="btn btn-danger">
					            <i class="glyphicon"></i> 新增
					        </button>
					        <button id="remove" class="btn btn-danger" disabled>
					            <i class="glyphicon"></i> 删除
					        </button>
					    </div>
					 	<table id="table" data-click-to-select="true" style="padding: 0px;"></table>
						<div class="form-group ">
						       <div class="col-lg-10 controls has-feedback has-error">
						        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style="">填写1到3条数据.</small>
						       </div>
			          	</div>
					 	</fieldset>
				    </div>
			    </div>
			    <div style="height:20px;"></div>
			    <div class="row-fluid" style="width: 1060px;margin-left: auto;margin-right: auto;">
					<div class="span12">
						<fieldset>
                    	<legend>联系方式</legend>
                    	<div class="form-group " >
		                   	<label class="col-sm-2 control-label" for="key_001_003_lxr">联系人</label>
		                   	<div class="col-sm-2 controls">
		                   		<input class="form-control input_text" maxlength="10"  id="key_001_003_lxr" value="${(key_001_003_lxr)!''}" name="key_001_003_lxr"   type="text" placeholder=""/>
		                   	</div>
                  			<span style="color:red;padding:2px;line-height: 35px;margin-left: -315px;">*</span>
		                  	<label class="col-sm-2 control-label" for="key_001_003_dh">电话</label>
		                  	<div class="col-sm-2 controls">
		                   		<input class="form-control input_text" maxlength="20" id="key_001_003_dh" value="${(key_001_003_dh)!''}" name="key_001_003_dh"   type="text" placeholder=""/>
		                  	</div>
                  			<span style="color:red;padding:2px;line-height: 35px;margin-left: 290px;">*</span>
		              	</div>
		              	<div class="form-group ">
		                  	<label class="col-sm-2 control-label" for="key_001_003_dz">地址</label>
		                  	<div class="col-sm-6 controls">
		                   		<input class="form-control input_text" maxlength="50"  id="key_001_003_dz" value="${(key_001_003_dz)!''}" name="key_001_003_dz"   type="text" placeholder=""/>
		                  	</div>
                  			<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
		              	</div>
					 	</fieldset>
						<div class="form-group "  style="width: 65%;margin-right: auto;margin-left: auto;">
						       <div class="col-lg-5 controls has-feedback has-error">
						        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style=""></small>
						       </div>
			          	</div>
				      	<div class="form-group " style="margin: 10px;">
				          	<div class="div_input_submit">
								<input name="submit" type="button" onclick="submit_form()" value="保存，去发布">
							</div>
				      	</div>
				    </div>
			    </div>
	      	</form>
      	</div>
	</body>
</html>
<link href="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.css" rel="stylesheet">
<link href="${basePath}/resources/lib/bootstrap-table-examples-master/assets/bootstrap-table/bootstrap-editable.css" rel="stylesheet">
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse_state.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json_parse.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/json2.js"></script>
<script src="${basePath}/resources/lib/JSON-js-master/cycle.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/mindmup-editabletable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/src/bootstrap-table.js"></script>
<script src="${basePath}/resources/lib/x-editable-master/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script src="${basePath}/resources/lib/bootstrap-table-master/dist/extensions/editable/bootstrap-table-editable.js"></script>
<script src="${basePath}/resources/lib/editable-table-master/numeric-input-example.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript">
var $table;
var $insertRow = $('#insertRow');
var $remove = $('#remove');
var $tableRowsNum = 0;
$(function () {
    initTable();
    $tableRowsNum =  $('#table tr').length;
});
//按下Enter搜索
function enterEvent(evt) {
    if (evt.keyCode == 13) {
        $("#btnSearch").click();
    }
}
        
function initTable() {
	var data =${(json_key_001_003_table_irrl)!'[]'};
        
        
    $table = $('#table').bootstrapTable({
        columns: [ 
    	{
            field: 'state',
            checkbox: true,
            align: 'center',
            height:20,
            valign: 'middle'
        }, {
            field: 'id',
            title: 'id',
            visible:false
        }, {
            field: 'json_key_001_003_rq_',
            title: '日期'
        }, {
            field: 'json_key_001_003_mc_',
            title: '活动'
        }, {
            field: 'json_key_001_003_nr_',
            title: '活动具体说明<br/><span style="font-size:12px;font-weight: normal;">(可以填写活动地址或其他信息)</span>'
        }],
       data: data,
       onPostBody: function () {
          	$('#table').editableTableWidget({editor: $('<textarea>')});
        }
    });
    $(".no-records-found").css("display","none");
    $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table', function () {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        selections = getIdSelections();
    });
    
    
    $remove.click(function () {
    	update_tabledata();
        var ids = getIdSelections();
        $table.bootstrapTable('remove', {
            field: 'id',
            values: ids
        });
    	$(".no-records-found").css("display","none");
		tiggerOnEven_display();
		tiggerOnEven_validate();
        //$remove.prop('disabled', true);
    });
    
    $insertRow.click(function () {
    	if( $('#table tr').length>3){
			j_tip('最多允许添加3条IR日历数据','error');
    		return;
    	}
    	update_tabledata();
    	$tableRowsNum++;
        $('#table').bootstrapTable('insertRow', {
        	index: $tableRowsNum ,
        	row: {
        		id:$tableRowsNum,
	        	json_key_001_003_rq_:'',
	        	json_key_001_003_mc_: '',
	        	json_key_001_003_nr_: ''
            }
        });
		tiggerOnEven_display();
		tiggerOnEven_validate();
		tiggerOnEven_updateCell();
    });
	tiggerOnEven_display();
	tiggerOnEven_validate();
	tiggerOnEven_updateCell();
}


function update_tabledata(){
	var _rb_tr = $("#table tr");
	var data = $table.bootstrapTable('getOptions').data;
	var i = 0;
	//console.log(data);
	$("table tr").each(function (){
		var d = data[i];
		if( d == undefined ){
			return false;
		}
		var _rb_tr_d =  $(this).children("td").length;
		if( _rb_tr_d==4){
		 	d.id=i;
		 	d.json_key_001_003_rq_=$(this).children("td").eq(1).html();
		 	d.json_key_001_003_mc_=$(this).children("td").eq(2).html();
		 	d.json_key_001_003_nr_=$(this).children("td").eq(3).html();
			i ++;
		}
 	});	
	//console.log(data);
}





function tiggerOnEven_updateCell(){
    $('#table td').on('change', function(evt, newValue) {
    	if( newValue == null || newValue == undefined || newValue.length == 0){
    		return true;
    	}
    	
		var r = $("table").find("tr").index(($(this).parent())) - 1;
		var c = $(this).parent().find("td").index($(this));
		if(c == 1){
			if(!/^(\d{4})\/(\d{1,2})\/(\d{1,2})$/.test(newValue)){
				j_tip('日期格式错误（例：2016/01/01）','error');
				return false;
			}
		}
		return true;
	});
}



function tiggerOnEven_validate(){

    $('#table td').on('validate', function(evt, newValue) {
    	if( newValue == null || newValue == undefined || newValue.length == 0){
    		return true;
    	}
		
		var r = $("table").find("tr").index(($(this).parent())) - 1;
		var c = $(this).parent().find("td").index($(this));
		if( c == 2){
			if( newValue != null && newValue.length>10){
				j_tip('大事名称不能超过10个字符,已超过'+(newValue.length-10)+'字','error');
		    	return false;
			}
		}else if(c == 3){
			if( newValue != null && newValue.length>50){
				j_tip('内容不能超过50个字符,已超过'+(newValue.length-50)+'字','error');
		    	return false;
			}
		}
		return true;
	});

}

function tiggerOnEven_display(){
    $('#table td').on('click keypress dblclick', function(evt, newValue) {
		if($(this).attr("class")=="bs-checkbox "){//过滤不需要记录的
			return false;
		}
	});
}

/**
 * 千分符格式化
 * 
 * @param num
 * @returns
 */
function format1(num) {
	if (num == null || num == undefined || num == "") {
		return "";
	}
	if (isNaN(num))
		return num;
	return (parseFloat(num).toFixed(2) + '').replace(
			/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}


function getRowsSelections() {
   return ($table.bootstrapTable('getOptions').data.length+1);
}

function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
    });
}

function getHeight() {
    //return $(window).height() - $('h1').outerHeight(true);
    return 210;
}
function operateFormatter(value, row, index) {
    return [
            '<a class="edit btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="编辑">',
                '<i class="icon-pencil">编辑</i>',
            '</a>',
            '<a class="remove btn btn-xs btn-default" style="margin-left:10px" href="javascript:void(0)" title="移除">',
                '<i class="icon-remove">移除</i>',
            '</a>'
        ].join('');
}


</script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  key_001_003_lxr: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    }
	              }
	          },
	    	  key_001_003_dh: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    }
	              }
	          },
	    	  key_001_003_dz: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围5~100个字',
				            min: '5',
				            max: '100'
	                    }
	              }
	          }
	      }
	  });
});




function submit_form(){
	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		return;
	}

    	update_tabledata();
	var data = $table.bootstrapTable('getOptions').data;
	if( data != undefined && data.length>0){
		var _rb_data = {};
		_rb_data.reportType='${reportType!''}';
		_rb_data.reportIdstr='${reportIdstr!''}';
		_rb_data.pageIdstr='${pageName!''}';
		_rb_data.key_001_003_dh=$('#key_001_003_dh').val();
		_rb_data.key_001_003_lxr=$('#key_001_003_lxr').val();
		_rb_data.key_001_003_dz=$('#key_001_003_dz').val();
		
		var jStr = "[";
		
		var x = 0;
		for(var i = 0;i<data.length;i++){
			var tem = data[i];
			if( tem.json_key_001_003_rq_ == null ||  tem.json_key_001_003_rq_  == undefined|| tem.json_key_001_003_rq_.length==0
			||  tem.json_key_001_003_mc_ == null ||  tem.json_key_001_003_mc_  == undefined|| tem.json_key_001_003_mc_.length==0
			|| tem.json_key_001_003_nr_ == null ||  tem.json_key_001_003_nr_  == undefined|| tem.json_key_001_003_nr_.length==0
			){
				j_tip('请填写完整需要保存的数据，新增的每一项都需要填写','error');
				return false;
			}
			x++;
			var _rb_str='{';
	        _rb_str += '"id":"'+ i +'",';
	        _rb_str += '"json_key_001_003_rq_":"'+tem.json_key_001_003_rq_+'",';
	        _rb_str += '"json_key_001_003_mc_":"'+tem.json_key_001_003_mc_+'",';
	        _rb_str += '"json_key_001_003_nr_":"'+tem.json_key_001_003_nr_+'"';
			_rb_str+="},";
			jStr+=(_rb_str);
		}
		if(jStr.substr(jStr.length-1,1)==","){
        	jStr = jStr.substr(0,jStr.length-1)+ " ]";
			//console.log(jStr);
		    _rb_data.json_key_001_003_table_irrl=jStr;
			submitAjax('${webPath}/reportsave/savedgnt',_rb_data);
 			setTimeout("load_href('${webPath}/report/listview')", 1000);
			return;
        }
	}
	j_tip('需要保存1~3条数据','error');
}
</script>
