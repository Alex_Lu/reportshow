<#assign _reportedit_ggpagelist = [{"page":"001", "name":"封面"},{"page":"002", "name":"本期亮点"},{"page":"003", "name":"公司经营"},{"page":"004", "name":"获奖情况"},{"page":"005", "name":"财务数据"},{"page":"006", "name":"财务指标"},{"page":"007", "name":"利润分配"},{"page":"008", "name":"主营业务分析-行业"},{"page":"009", "name":"主营业务分析-产品"},{"page":"010", "name":"主营业务分析-地区"},{"page":"011", "name":"研发投入"},{"page":"012", "name":"股东信或董事会报告"},{"page":"013", "name":"企业社会责任"},{"page":"014-1", "name":"董事长"},{"page":"014", "name":"其他高管团队"},{"page":"015", "name":"公司大事记"},{"page":"016", "name":"公司文化"},{"page":"017", "name":"投资者关系管理"}]>
<style type="text/css">  
    .header {
    	height:100px;
    }  
    .navigator_zy{
    	margin-top: 10px;
    }
</style> 
<!--,{"page":"012-1", "name":"管理层讨论与分析"}-->

<ul class="navList" id="navList">
	<#list _reportedit_ggpagelist as item>
		<a id="page_${item_index+1}"  <#if pageName=='${item.page}'>class="navList_a_select" style="background-color: #bebec5;"</#if>
			href='${webPath}/reportedit/view/${reportType}/${reportIdstr}/${item.page}' title="点击进入${item.name}编辑页面" >
		<li><span>${item_index+1}、${item.name}</span></li>
	</a>
	</#list>
</ul>
