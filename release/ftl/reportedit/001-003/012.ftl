<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>股东信-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form"  method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				
				<div class="form-group " style="margin: 10px;">
                  	<label class="col-sm-3 controls" for="key_001_003_zgd_title">
                     	<input class="form-control" id="key_001_003_zgd_title" name="key_001_003_zgd_title"  value="${(key_001_003_zgd_title)!'股东信'}"  name="key_001_003_zgd_title" type="text" placeholder="(可选)"/>
                  	</label>
                   	<div class="col-sm-8 controls" for="key_001_003_zgd">
                   		<textarea class="form-control input_text" rows="10" id="key_001_003_zgd" name="key_001_003_zgd"   placeholder="(选填)">${(key_001_003_zgd)!''}</textarea>
                   	</div>
              	</div>
				<div class="form-group ">
                  	<label class="col-sm-3 controls">
                  	</label>
			      	<div class="col-lg-8 controls has-feedback has-error">
			        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style="">标题字数限制8个字符，内容字数限制500个字符</small>
			       	</div>
	          	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {

	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  key_001_003_zgd: {
	              group: '.controls',
	              validators: {
		              	stringLength: {
				            message: '最长不能超过500个字',
				            max: '500'
	                    }
	              }
	          },
	    	  key_001_003_zgd_title: {
	              group: '.controls',
	              validators: {
	                  	notEmpty: {
	                      	message: '标题必须填写'
	                  	},
		              	stringLength: {
				            message: '最长不能超过8个字',
				            max: '8'
	                    }
	              }
	          }
	      }
	  });
});




function submit_form(){

	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		return;
	}
	
	
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>