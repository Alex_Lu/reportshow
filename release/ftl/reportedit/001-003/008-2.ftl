<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>主营业务发展-行业-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<input id="json_key_" name="json_key_" type="hidden" value="json_key_001_003_008_fhy_,json_key_001_003_008_yysr_,json_key_001_003_008_yycb_,json_key_001_003_008_mlr_,json_key_001_003_008_yysrbsnzj_,json_key_001_003_008_yycbbsnzj_,json_key_001_003_008_mlrbsnzj_">
				<div class="form-group " style="margin: 10px;">
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
							<table class="table-condensed" id="table_ggtd" style="margin-left:0px;border: 1px solid #cccccc;">
								<thead>
									<tr  class="">
										<th style="text-align: left;padding-left: 20px;">
											分行业
										</th>
										<th style="text-align: left;padding-left: 20px;">
											营业收入
										</th>
										<th style="text-align: left;padding-left: 20px;">
											营业成本
										</th>
										<th style="text-align: left;padding-left: 20px;">
											毛利率(%)
										</th>
										<th style="text-align: left;padding-left: 20px;">
											营业收入比上年增减(%)
										</th>
										<th style="text-align: left;padding-left: 20px;">
											营业成本比上年增减(%)
										</th>
										<th style="text-align: left;padding-left: 20px;">
											毛利率比上年增减(%)
										</th>
										<th style="text-align: left;padding-left: 20px;">
										</th>
									</tr>
								</thead>
								<tbody >
									<#if json_key_001_003_008_fhy_?exists>
									<#list json_key_001_003_008_fhy_ as item>
									<tr  class="">
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_fhy_${(item_index+1)}" value="${(item)!''}" name="json_key_001_003_008_fhy_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yysr_${(item_index+1)}" value="${(json_key_001_003_008_yysr_[item_index])!''}" name="json_key_001_003_008_yysr_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yycb_${(item_index+1)}" value="${(json_key_001_003_008_yycb_[item_index])!''}" name="json_key_001_003_008_yycb_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_mlr_${(item_index+1)}" value="${(json_key_001_003_008_mlr_[item_index])!''}" name="json_key_001_003_008_mlr_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yysrbsnzj_${(item_index+1)}" value="${(json_key_001_003_008_yysrbsnzj_[item_index])!''}" name="json_key_001_003_008_yysrbsnzj_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yycbbsnzj_${(item_index+1)}" value="${(json_key_001_003_008_yycbbsnzj_[item_index])!''}" name="json_key_001_003_008_yycbbsnzj_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_mlrbsnzj_${(item_index+1)}" value="${(json_key_001_003_008_mlrbsnzj_[item_index])!''}" name="json_key_001_003_008_mlrbsnzj_${(item_index+1)}"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<th style="text-align: left;padding-left: 20px;">
											删除
										</th>
									</tr>
									</#list>
									<#else>
									<tr  class="">
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_fhy_1" name="json_key_001_003_008_fhy_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yysr_1" name="json_key_001_003_008_yysr_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yycb_1" name="json_key_001_003_008_yycb_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_mlr_1" name="json_key_001_003_008_mlr_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yysrbsnzj_1" name="json_key_001_003_008_yysrbsnzj_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_yycbbsnzj_1" name="json_key_001_003_008_yycbbsnzj_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                     	<input class="form-control input_text" id="json_key_001_003_008_mlrbsnzj_1" name="json_key_001_003_008_mlrbsnzj_1"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"  type="text" />
						                   	</div>
										</td>
									</tr>
									</#if>
								</tbody>
							</table>
							</fieldset>
						</div>
					</div>
				</div>
				</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
                  		<input type="button" value="新增获取情况" onclick="addDiv()">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
function submit_form(){
	submitForm('form','${webPath}/reportsave/savedgnt');
}
function addDiv(){
	//console.log($(".form-group").length);
	var _rp00_table_tr_num = $("#table_ggtd>tbody>tr").length;
	var _rp00_div='<tr  class="">';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_fhy_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_fhy_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_yysr_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_yysr_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_yycb_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_yycb_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_mlr_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_mlr_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_yysrbsnzj_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_yysrbsnzj_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_yycbbsnzj_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_yycbbsnzj_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='<td>';
	_rp00_div+='<div class="col-sm-2 controls" style="width:inherit;">';
	_rp00_div+='<input class="form-control input_text" id="json_key_001_003_008_mlrbsnzj_'+(_rp00_table_tr_num+1)+'" name="json_key_001_003_008_mlrbsnzj_'+(_rp00_table_tr_num+1)+'"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\')"  type="text" />';
	_rp00_div+='</div>';
	_rp00_div+='</td>';
	_rp00_div+='</tr>';
	$("#table_ggtd tbody").append(_rp00_div);
}
</script>