<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>董事长-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<style>
			/*
		  	.file-default-preview img{
		  		height:200px;
		  		width:220px!important;
		  	}
		  	.file-preview{
		  		height:240px;
		  		width:240px!important;
    			margin-right: auto;
		  		display:block!important;
		  		background:url("${basePath}/resources/image/tup.png") no-repeat;
		  		background-size:100% 100%;
		  	}
		  	.file-preview-frame{
		  		margin: 0px!important;
		  		padding: 0px!important;
		  		float:none!important;
		  		display:block!important;
		  		height:200px!important;
		  	}
		  	.file-preview-frame img{
		  		height:200px!important;
		  		width:240px!important;
		  	}
		  	*/
  		</style>
		<style>
		  	.file-preview{
		  		width:290px!important;
		  	}
  		</style>
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<input id="img_key_" name="img_key_" type="hidden" value="img_key_001_003_dsztx">
				<div style="width: 1060px;">
					<div class="form-group " style="margin: 10px;">
	                   	<label class="col-sm-2 control-label" for="key_001_003_dsz">姓名<span style="color:red;padding:2px;">*</span></label>
	                   	<div class="col-sm-10 controls">
	                     	<input maxlength="10" class="form-control input_text" id="key_001_003_dsz" value="${(key_001_003_dsz)!''}" name="key_001_003_dsz" type="text" placeholder="最多允许输入10个字(必填)"/>
	                   	</div>
	              	</div>
	              	<div class="form-group " style="margin: 10px;display:none;">
	                   	<label class="col-sm-2 control-label" for="key_001_003_dszzw">职位<span style="color:red;padding:2px;">*</span></label>
	                   	<div class="col-sm-10 controls">
	                     	<input maxlength="40" class="form-control input_text" id="key_001_003_dszzw" value="${(key_001_003_dszzw)!'董事长'}" name="key_001_003_dszzw" type="text" placeholder="(必填)"/>
	                   	</div>
	              	</div>
					<div class="form-group " style="margin: 10px;">
	                   	<label class="col-sm-2 control-label" for="key_001_003_dszjj">简介<span style="color:red;padding:2px;">*</span></label>
	                   	<div class="col-sm-10 controls">
	                   		<textarea class="form-control input_text" maxlength="150"  rows="5" id="key_001_003_dszjj" name="key_001_003_dszjj"  placeholder="(必填)">${(key_001_003_dszjj)!''}</textarea>
	                   	</div>
	              	</div>
	              	<div class="form-group " style="margin: 10px;">
	                  	<label class="col-sm-2 control-label" for="img_key_001_003_dsztx">头像<span style="color:red;padding:2px;">*</span></label>
	                  	<div class="col-sm-10 controls" style="">
	                     	<input class="form-control" id="img_key_001_003_dsztx" name="img_key_001_003_dsztx"  type="file" placeholder="上传董事长头像(必填)"/>
	                     	<input class="form-control" id="hidden_img_key_001_003_dsztx" name="hidden_img_key_001_003_dsztx" value="${img_key_001_003_dsztx!''}" type="hidden"/>
	                  	</div>
	              	</div>
		          	<div class="form-group " style="margin: 10px;">
		          		<label class="col-sm-2 control-label" for="validate_number_key_001_003_001"></label>
				        <div class="col-lg-10 controls has-feedback has-error">
				        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_001" data-bv-result="INVALID" style="">建议图片像素尺寸：1:1</small>
				       </div>
		          	</div>
	              	<div class="form-group " style="margin: 10px;">
	                  	<div class="div_input_submit">
							<input name="submit" type="button" onclick="submit_form()" value="保存">
						</div>
	              	</div>
				</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-fileinput-master/js/fileinput_locale_zh.js"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_fileinput.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(function () {
    //0.初始化fileinput
    var _rb_oFileInput1 = new FileInput();
    _rb_oFileInput1.Init("img_key_001_003_dsztx", '${basePath}', "${img_key_001_003_dsztx!''}");
});
</script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  key_001_003_dsz: {
	              group: '.controls',
	              validators: {
	                  notEmpty: {
	                      message: '姓名不可为空'
	                  }
	              }
	          },
	    	  key_001_003_dszzw: {
	              validators: {
	                  notEmpty: {
	                      message: '职位不可为空'
	                  }
	              }
	          },
	    	  key_001_003_dszjj: {
	              validators: {
	                  notEmpty: {
	                      message: '简介不可为空'
	                  }
	              }
	          },
	    	  key_001_003_gsmnndbg: {
	              group: '.controls',
	              validators: {
	                  notEmpty: {
	                      message: '${(fbReport.date)}年年度报告不可为空'
	                  }
	              }
	          }
	      }
	  });

});


function submit_form(){
	//表单验证
	//$('#form').bootstrapValidator('validate')
	//if(!$('#form').data('bootstrapValidator').isValid()){
	//	return;
	//}
	
	var img_key_001_003_dsztx = $("#img_key_001_003_dsztx").val();
	if( img_key_001_003_dsztx == undefined || img_key_001_003_dsztx ==null || img_key_001_003_dsztx.length==0){
		var hidden_img_key_001_003_dsztx = $("#hidden_img_key_001_003_dsztx").val();
		if( hidden_img_key_001_003_dsztx == undefined || hidden_img_key_001_003_dsztx ==null || hidden_img_key_001_003_dsztx.length==0){
			j_tip("需要上传董事长头像","error");
			return ;
		}else{
			var v = $(".file-error-message").css("display");
			if( v == "block"){
				j_tip('请上传正确的图片','error');
				return false
			}
		}
	}else{
		var suppotFile = new Array();
			suppotFile[0] = "jpg";
			suppotFile[1] = "gif";
			suppotFile[2] = "bmp";
			suppotFile[3] = "png";
			suppotFile[4] = "jpeg";
		if(!isFile(img_key_001_003_dsztx,suppotFile)){
			j_tip('上传的文件格式不正确！上传格式有(.gif、.jpg、.png、.bmp、.jpeg)','error');
			return false;
		}
	}
	
	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		return;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>