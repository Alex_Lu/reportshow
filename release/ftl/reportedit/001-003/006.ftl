<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>财务指标-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<div class="form-group " style="margin: 10px;">
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
							<table class="table-condensed"  style="width:1060px;margin-left:0px;border: 1px solid #cccccc;margin-left: auto;margin-right: auto;">
								<thead>
									<tr  class="">
										<th style="text-align: left;padding-left: 20px;width: 400px;">
										项目
										</th>
										<th style="text-align: left;padding-left: 20px;">
											${(fbReport.date!'')}年
										</th>
										<th style="text-align: left;padding-left: 20px;">
											${(fbReport.date?number-1)?c}年
										</th>
										<!--
										<th style="text-align: left;padding-left: 20px;">
											本期比上年同期增减(%)
										</th>
										-->
										<th style="text-align: left;padding-left: 20px;">
											${(fbReport.date?number-2)?c}年
										</th>
									</tr>
								</thead>
								<tbody>
									<tr  class="">
										<td style="width: 400px;">
											<label class="col-sm-2 control-label" for="key_001_003_mgsy">每股收益（元/股）</label>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
												<input class="form-control " id="hidden_key_001_003_mgsy_bq" value="${(key_001_003_mgsy_bq)!''}" name="hidden_key_001_003_mgsy_bq" type="text"  onblur="zhszfd(this.value,'key_001_003_mgsy_bq','hidden_key_001_003_mgsy_bq')"    placeholder=""/>
												<input class="form-control " id="key_001_003_mgsy_bq" value="${(key_001_003_mgsy_bq)!''}" name="key_001_003_mgsy_bq" type="hidden"     placeholder=""/>
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
						                   	<div class="col-sm-2 controls" style="width:inherit;">
						                   		<input class="form-control " id="hidden_key_001_003_mgsy_sn1" value="${(key_001_003_mgsy_sn1)!''}" name="hidden_key_001_003_mgsy_sn1" type="text"  onblur="zhszfd(this.value,'key_001_003_mgsy_sn1','hidden_key_001_003_mgsy_sn1')"    placeholder=""/>
						                   		<input class="form-control " id="key_001_003_mgsy_sn1" value="${(key_001_003_mgsy_sn1)!''}" name="key_001_003_mgsy_sn1" type="hidden"     placeholder=""/>
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<!--
										<td>
						                   	<div class="col-sm-2 controls" style="width:inherit;">
						                   		<input class="form-control " id="hidden_key_001_003_mgsy_zs" value="${(key_001_003_mgsy_zs)!''}" name="hidden_key_001_003_mgsy_zs" type="text"  onblur="zhszfd(this.value,'key_001_003_mgsy_zs','hidden_key_001_003_mgsy_zs')"    placeholder=""/>
						                   		<input class="form-control " id="key_001_003_mgsy_zs" value="${(key_001_003_mgsy_zs)!''}" name="key_001_003_mgsy_zs" type="hidden"    placeholder=""/>
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										-->
										<td>
						                   	<div class="col-sm-2 controls" style="width:inherit;">
						                   		<input class="form-control " id="hidden_key_001_003_mgsy_sn2" value="${(key_001_003_mgsy_sn2)!''}" name="hidden_key_001_003_mgsy_sn2" type="text"  onblur="zhszfd(this.value,'key_001_003_mgsy_sn2','hidden_key_001_003_mgsy_sn2')"    placeholder=""/>
						                   		<input class="form-control " id="key_001_003_mgsy_sn2" value="${(key_001_003_mgsy_sn2)!''}" name="key_001_003_mgsy_sn2" type="hidden"     placeholder=""/>
						                   	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<tr class="success">
										<td style="width: 400px;">
											<label class="col-sm-2 control-label" for="key_001_003_pjjzcsyl">平均净资产收益率（%）</label>
										</td>
										<td>
						                  	<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjjzcsyl_bq" value="${(key_001_003_pjjzcsyl_bq)!''}" name="hidden_key_001_003_pjjzcsyl_bq"  onblur="zhszfd(this.value,'key_001_003_pjjzcsyl_bq','hidden_key_001_003_pjjzcsyl_bq')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjjzcsyl_bq" value="${(key_001_003_pjjzcsyl_bq)!''}" name="key_001_003_pjjzcsyl_bq"     type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjjzcsyl_sn1" value="${(key_001_003_pjjzcsyl_sn1)!''}" name="hidden_key_001_003_pjjzcsyl_sn1"  onblur="zhszfd(this.value,'key_001_003_pjjzcsyl_sn1','hidden_key_001_003_pjjzcsyl_sn1')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjjzcsyl_sn1" value="${(key_001_003_pjjzcsyl_sn1)!''}" name="key_001_003_pjjzcsyl_sn1"    type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<!--
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjjzcsyl_zs" value="${(key_001_003_pjjzcsyl_zs)!''}" name="hidden_key_001_003_pjjzcsyl_zs"  onblur="zhszfd(this.value,'key_001_003_pjjzcsyl_zs','hidden_key_001_003_pjjzcsyl_zs')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjjzcsyl_zs" value="${(key_001_003_pjjzcsyl_zs)!''}" name="key_001_003_pjjzcsyl_zs"     type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										-->
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjjzcsyl_sn2" value="${(key_001_003_pjjzcsyl_sn2)!''}" name="hidden_key_001_003_pjjzcsyl_sn2"  onblur="zhszfd(this.value,'key_001_003_pjjzcsyl_sn2','hidden_key_001_003_pjjzcsyl_sn2')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjjzcsyl_sn2" value="${(key_001_003_pjjzcsyl_sn2)!''}" name="key_001_003_pjjzcsyl_sn2"    type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
									<tr class="success">
										<td style="width: 400px;">
											<div class="col-sm-2 controls has-feedback" style="width:inherit;">
						                     	<input class="form-control" id="key_001_003_pjzzcsyl_title" value="${(key_001_003_pjzzcsyl_title)!'扣除非经常性损益后的基本每股收益（元／股）'}"  name="key_001_003_pjzzcsyl_title" type="text" placeholder="(必填)"/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
						                  	<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjzzcsyl_bq" value="${(key_001_003_pjzzcsyl_bq)!''}" name="hidden_key_001_003_pjzzcsyl_bq"  onblur="zhszfd(this.value,'key_001_003_pjzzcsyl_bq','hidden_key_001_003_pjzzcsyl_bq')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjzzcsyl_bq" value="${(key_001_003_pjzzcsyl_bq)!''}" name="key_001_003_pjzzcsyl_bq"     type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjzzcsyl_sn1" value="${(key_001_003_pjzzcsyl_sn1)!''}" name="hidden_key_001_003_pjzzcsyl_sn1"  onblur="zhszfd(this.value,'key_001_003_pjzzcsyl_sn1','hidden_key_001_003_pjzzcsyl_sn1')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjzzcsyl_sn1" value="${(key_001_003_pjzzcsyl_sn1)!''}" name="key_001_003_pjzzcsyl_sn1"    type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										<!--
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjzzcsyl_zs" value="${(key_001_003_pjzzcsyl_zs)!''}" name="hidden_key_001_003_pjzzcsyl_zs"  onblur="zhszfd(this.value,'key_001_003_pjzzcsyl_zs','hidden_key_001_003_pjzzcsyl_zs')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjzzcsyl_zs" value="${(key_001_003_pjzzcsyl_zs)!''}" name="key_001_003_pjzzcsyl_zs"    type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
										-->
										<td>
											<div class="col-sm-2 controls" style="width:inherit;">
						                  		<input class="form-control" id="hidden_key_001_003_pjzzcsyl_sn2" value="${(key_001_003_pjzzcsyl_sn2)!''}" name="hidden_key_001_003_pjzzcsyl_sn2"  onblur="zhszfd(this.value,'key_001_003_pjzzcsyl_sn2','hidden_key_001_003_pjzzcsyl_sn2')"     type="text" placeholder=""/>
						                  		<input class="form-control" id="key_001_003_pjzzcsyl_sn2" value="${(key_001_003_pjzzcsyl_sn2)!''}" name="key_001_003_pjzzcsyl_sn2"    type="hidden" placeholder=""/>
						                  	</div>
						                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
										</td>
									</tr>
								</tbody>
							</table>
							</fieldset>
						</div>
						<div class="form-group " style="">
						       <div class="col-lg-10 controls has-feedback has-error">
						        	<small class="help-block" data-bv-validator="callback" data-bv-for="validate_number_key_001_003_002" data-bv-result="INVALID" style="">本页全部为必填项.</small>
						       </div>
			          	</div>
					</div>
				</div>
				</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {

	$('#hidden_key_001_003_mgsy_bq').val(format2(Number('${key_001_003_mgsy_bq!''}'),2));
	$('#hidden_key_001_003_mgsy_sn1').val(format2(Number('${key_001_003_mgsy_sn1!''}'),2));
	$('#hidden_key_001_003_mgsy_zs').val(format2(Number('${key_001_003_mgsy_zs!''}'),2));
	$('#hidden_key_001_003_mgsy_sn2').val(format2(Number('${key_001_003_mgsy_sn2!''}'),2));
	$('#hidden_key_001_003_pjjzcsyl_bq').val(format2(Number('${key_001_003_pjjzcsyl_bq!''}'),2));
	$('#hidden_key_001_003_pjjzcsyl_sn1').val(format2(Number('${key_001_003_pjjzcsyl_sn1!''}'),2));
	$('#hidden_key_001_003_pjjzcsyl_zs').val(format2(Number('${key_001_003_pjjzcsyl_zs!''}'),2));
	$('#hidden_key_001_003_pjjzcsyl_sn2').val(format2(Number('${key_001_003_pjjzcsyl_sn2!''}'),2));
	$('#hidden_key_001_003_pjzzcsyl_bq').val(format2(Number('${key_001_003_pjzzcsyl_bq!''}'),2));
	$('#hidden_key_001_003_pjzzcsyl_sn1').val(format2(Number('${key_001_003_pjzzcsyl_sn1!''}'),2));
	$('#hidden_key_001_003_pjzzcsyl_zs').val(format2(Number('${key_001_003_pjzzcsyl_zs!''}'),2));
	$('#hidden_key_001_003_pjzzcsyl_sn2').val(format2(Number('${key_001_003_pjzzcsyl_sn2!''}'),2));
	
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  hidden_key_001_003_mgsy_bq: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_mgsy_sn1: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	          /*
	    	  hidden_key_001_003_mgsy_zs: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	          */
	    	  hidden_key_001_003_mgsy_sn2: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_pjjzcsyl_bq: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_pjjzcsyl_sn1: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	          /*
	    	  hidden_key_001_003_pjjzcsyl_zs: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	          */
	    	  hidden_key_001_003_pjjzcsyl_sn2: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_pjzzcsyl_bq: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	    	  hidden_key_001_003_pjzzcsyl_sn1: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	          /*
	    	  hidden_key_001_003_pjzzcsyl_zs: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	          */
	    	  hidden_key_001_003_pjzzcsyl_sn2: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
	                    callback: {
	                      message: '只能填写数字或者小数点',
	                      callback: function(value, validator) {
	                      		if(value == null || value=="")return true;
								var re = /[^-][^0-9,.]/g;
                          		return !re.test(value) ;
                    	  }
	                  }
	              }
	          },
	    	  key_001_003_pjzzcsyl_title: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    }
	              }
	          }
	      }
	  });
});




function submit_form(){

	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		return;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>