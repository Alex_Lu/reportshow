<#include "../../include.ftl">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>公司文化-编辑</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	</head>

	<body>
		<#include "../top.ftl">
		<div class="div_form">
			<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
				<input id="page_label" name="page_label" type="hidden" value="1">
				<input id="reportType" name="reportType" type="hidden" value="${reportType!''}">
				<input id="reportIdstr" name="reportIdstr" type="hidden" value="${reportIdstr!''}">
				<input id="pageIdstr" name="pageIdstr" type="hidden" value="${pageName!''}">
				<div class="form-group " style="margin: 10px;">
                   	<label class="col-sm-3 controls" for="key_001_003_gsyj">
                     	<input class="form-control" id="key_001_003_gsyj_title" value="${(key_001_003_gsyj_title)!'公司愿景'}"  name="key_001_003_gsyj_title" type="text" placeholder="(可选)"/>
                  	</label>
                   	<div class="col-sm-9 controls">
                   		<textarea class="form-control input_text" rows="2" id="key_001_003_gsyj" name="key_001_003_gsyj">${(key_001_003_gsyj)!''}</textarea>
                   	</div>
                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                   	<label class="col-sm-3 controls" for="key_001_003_qywh">
                     	<input class="form-control" id="key_001_003_qywh_title" value="${(key_001_003_qywh_title)!'企业文化'}"  name="key_001_003_qywh_title" type="text" placeholder="(可选)"/>
                  	</label>
                  	<div class="col-sm-9 controls">
                   		<textarea class="form-control input_text" rows="2" id="key_001_003_qywh" name="key_001_003_qywh">${(key_001_003_qywh)!''}</textarea>
                  	</div>
                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                   	<label class="col-sm-3 controls" for="key_001_003_gsjzg">
                     	<input class="form-control" id="key_001_003_gsjzg_title" value="${(key_001_003_gsjzg_title)!'公司价值观'}"  name="key_001_003_gsjzg_title" type="text" placeholder="(可选)"/>
                  	</label>
                  	<div class="col-sm-9 controls">
                   		<textarea class="form-control input_text" rows="2" id="key_001_003_gsjzg" name="key_001_003_gsjzg">${(key_001_003_gsjzg)!''}</textarea>
                  	</div>
                  	<span style="color:red;padding:2px;line-height: 35px;margin-left: -15px;">*</span>
              	</div>
              	<div class="form-group " style="margin: 10px;">
                  	<div class="div_input_submit">
						<input name="submit" type="button" onclick="submit_form()" value="保存">
					</div>
              	</div>
			</form>
		</div>
	</body>
</html>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery-2.1.1.min.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/jquery.form.js"></script>
<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script src="${basePath}/resources/js/reportedit/edit_validator.js"></script>
<script src="${basePath}/resources/js/reportedit/edit.js"></script>
<script type="text/javascript" language="javascript">
$(document).on('ready', function() {
	$('#form').bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: {
	    	  key_001_003_gsyj_title: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围1~8个字',
				            max: '8'
	                    }
	              }
	          },
	    	  key_001_003_qywh_title: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围1~8个字',
				            max: '8'
	                    }
	              }
	          },
	    	  key_001_003_gsjzg_title: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围1~8个字',
				            max: '8'
	                    }
	              }
	          },
	    	  key_001_003_gsyj: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围10~100个字',
				            min: '10',
				            max: '100'
	                    }
	              }
	          },
	    	  key_001_003_qywh: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围10~100个字',
				            min: '10',
				            max: '100'
	                    }
	              }
	          },
	    	  key_001_003_gsjzg: {
	              group: '.controls',
	              validators: {
		              	notEmpty: {
	                        message: '必填项'
	                    },
		              	stringLength: {
				            message: '填写范围10~100个字',
				            min: '10',
				            max: '100'
	                    }
	              }
	          }
	      }
	  });
});




function submit_form(){

	$('#form').bootstrapValidator('validate')
	if(!$('#form').data('bootstrapValidator').isValid()){
		return;
	}
	submitForm('form','${webPath}/reportsave/savedgnt');
}
</script>