//初始化fileinput
var FileInput = function() {
	var oFile = new Object();
	// 初始化fileinput控件（第一次初始化）
	oFile.Init = function(ctrlName, basePath, defaultImageUrl, uploadUrl,
			maxFileCount) {
		var control = $('#' + ctrlName);
		//var defaultPreviewContent = "";// 显示默认图片
		var initialPreview = [];// 预览图片
		if (defaultImageUrl != undefined && defaultImageUrl != null
				&& defaultImageUrl.length > 0) {
			//defaultPreviewContent = "<img src='" + basePath + ""+ defaultImageUrl+ "' alt='' style='width:160px'>";
			initialPreview = ["<img src='"+basePath +defaultImageUrl + "' class='file-preview-image' >"];
		}
		
		if (maxFileCount == undefined || maxFileCount == null|| maxFileCount.length == 0) {
			maxFileCount = 1;
		}

		// 初始化上传控件的样式
		control.fileinput({
			language : 'zh', // 设置语言
			uploadUrl : uploadUrl, // 上传的地址
			allowedFileExtensions : [ 'jpg', 'gif', 'png' ,'bmp','jpeg'],// 接收的文件后缀
			showUpload : false, // 是否显示上传按钮
			showCaption : false,// 是否显示标题
			showRemove : false,
			initialPreviewShowDelete:false,
			browseClass : "btn btn-primary", // 按钮样式
			previewFileIcon : "<i class='glyphicon glyphicon-king'></i>",
			dropZoneEnabled : false,// 是否显示拖拽区域
			// minImageWidth: 50, //图片的最小宽度
			// minImageHeight: 50,//图片的最小高度
			// maxImageWidth: 1000,//图片的最大宽度
			// maxImageHeight: 1000,//图片的最大高度
			// maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
			// minFileCount: 0,
			maxFileCount : maxFileCount, // 表示允许同时上传的最大文件个数
			enctype : 'multipart/form-data',
			validateInitialCount : true,
			//defaultPreviewContent : defaultPreviewContent,//默认图片
	        initialPreview:initialPreview,//预览图片的设置
			msgFilesTooMany : "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
		});
		// 导入文件上传完成之后的事件
		// fileuploadedLstSuccess(ctrlName,'filebatchuploadcomplete');
		// fileuploadedLstSuccess(ctrlName,'fileuploaded');
		// fileuploadedLstSuccess(ctrlName,'filebatchuploadsuccess');
		// fileuploadedLstSuccess(ctrlName,'filecleared');
		// fileuploadedLstSuccess(ctrlName,'fileclear');
		// fileuploadedLstSuccess(ctrlName,'filereset');
		// fileuploadedLstSuccess(ctrlName,'fileerror');
		// fileuploadedLstSuccess(ctrlName,'filefoldererror');
		fileuploadedLstSuccess(ctrlName, 'fileuploaderror');
		// fileuploadedLstSuccess(ctrlName,'filebatchuploaderror');
		// fileuploadedLstSuccess(ctrlName,'filedeleteerror');
		// fileuploadedLstSuccess(ctrlName,'filecustomerror');
		// fileuploadedLstSuccess(ctrlName,'filesuccessremove');
	}
	return oFile;
};

function fileuploadedLstSuccess(ctrlName, fiters_name) {
	// 导入文件上传完成之后的事件
	$("#" + ctrlName).on(fiters_name, function(event, data, previewId, index) {
		var file = data.files;
		alert(9);
		for (var i = 0; i < file.length; i++) {
			var t = (file[i].type);
			if (t.indexOf("image") <= -1) {
				j_tip('文件格式类型不正确', 'error');
				$('')
				break;
			}
		}
		/*
		 * var d = data.response.lstOrderImport; if (d == undefined) {
		 * j_tip('文件格式类型不正确','error'); FileInput.destroy(); return; }
		 */
	});
}

/**
 * 验证文件格式
 * @param fileName：文件名称
 * @param suppotFile：文件格式数组(后缀将会进行小写转化比较，所以，这里的数组内容应全是小写)
 * @returns {Boolean}
 */
function isFile(fileName,suppotFile) {
	if (fileName != null && fileName != "") {
		if (fileName.lastIndexOf(".") != -1) {
			if(suppotFile !=null ){
				var fileType = (fileName.substring(fileName.lastIndexOf(".") + 1,fileName.length)).toLowerCase();
				for (var i = 0; i < suppotFile.length; i++) {
					if (suppotFile[i] == fileType) {
						return true;
					} else {
						continue;
					}
				}
				return false;
			}
		}
	}
	return false;
}