
function validator(formid,fields){
	$('#'+formid).bootstrapValidator({
	      feedbackIcons: {
	          valid: 'glyphicon glyphicon-ok',
	          invalid: 'glyphicon glyphicon-remove',
	          validating: 'glyphicon glyphicon-refresh'
	      },
	      fields: fields
	  });
}


/**
 * 校验为数字包含小数点
 * @param value
 */
function _edit_isNumber(value){
	var re = new RegExp(/[^0-9.]/g);
	// re.test()
	if (value!=null&&value != "") {
		if (!re.test(value)) {
			j_tip("填写的值不正确","error");
			//e.value = "";
			e.focus();
		}
	}
}

function createFields(tag,operation){

}

function createValidatorsBds(tag,type,messages){
	
}


function validatorImg(inputid){
	alert(inputid);
	return 'inputid'==inputid;
}

/**
 * 验证vid1是否大于等于vid2，如果不满足则提示errorTxt信息,如果为空则不判断，默认为true
 * @param vid1
 * @param vid2
 * @param errorTxt
 */
function validatorTwoUpAndEqual(vid1,vid2,errorTxt){
	var v1 = $("#"+vid1).val();
	var v2 = $("#"+vid2).val();
	if(v1 == undefined || v1 == null || v1.length == 0 ){
		return;
	}
	if(v2 == undefined || v2 == null || v2.length == 0 ){
		return;
	}
	if(errorTxt == undefined || errorTxt == null || errorTxt.length == 0 ){
		errorTxt = "小于比较值";
	}
	v1 = v1.replace(/[^-0-9.]/g,'');
	v2 = v2.replace(/[^-0-9.]/g,'');
	if( isNaN(v1) || isNaN(v2) ){
		j_tip("数据异常","error");
		return ;
	}
	if(Number(v1) > Number(v2)){
		j_tip(errorTxt,"error");
		return ;
	}
}
