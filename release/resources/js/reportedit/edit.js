
var fileInput = null;
/**
 * 上传插件
 * @param ctrlName
 * @param imageurl
 * @param maxFileSize
 * @param maxImageWidth
 * @param maxImageHeight
 */
function initFileInput(ctrlName, imageurl,maxFileSize,maxImageWidth,maxImageHeight) {
	var control = $('#' + ctrlName);
	if (imageurl == undefined || imageurl == null || imageurl.length == 0) {
		fileInput = control.fileinput({
			language : 'zh', // 设置语言
			showCaption : false,
			showUpload : false,
			showRemove:false,
			/*showPreview:false,*/
			maxFileSize:maxFileSize,
			maxImageWidth: maxImageWidth,
			maxImageHeight: maxImageHeight,
			defaultPreviewContent : "",
			allowedFileExtensions : [ "jpg", "png", "jpeg", "gif" ]
		});
	} else {
		fileInput = control.fileinput({
			language : 'zh', // 设置语言
			showCaption : false,
			showUpload : false,
			showRemove:false,
			/*showPreview:false,*/
			maxFileSize:maxFileSize,
			maxImageWidth: maxImageWidth,
	        maxImageHeight: maxImageHeight,
			defaultPreviewContent : "<img src='" + basePath + imageurl+ "' alt='Your Avatar' style='width:160px'>",
			allowedFileExtensions : [ "jpg", "png", "jpeg", "gif" ]
		});
	}
}


/**
 * 提交表单
 * @param formid
 * @param url
 */
function submitForm(formid, url) {
	$("#" + formid).ajaxSubmit({
		type : 'post',
		url : url,
		dataType : 'json',
		success : function(data) {
			if (data.success) {
				j_tip("保存成功","success");
				setTimeout('next_page()', 1000);
			} else {
				j_tip(data.error,'error');
			}
		},
		error : function(XmlHttpRequest, textStatus, errorThrown) {
			console.log(XmlHttpRequest);
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
}

/**
 * ajax提交
 * @param url
 * @param data
 */
function submitAjax(url,data){
	$.ajax({
		url : url,
		type : 'post',
		dataType : 'json',
		data:data,
		success:function(data){
			if (data.success) {
				j_tip("保存成功","success");
				setTimeout('next_page()', 1000);
			} else {
				j_tip(data.error,'error');
			}
		},
		error : function(XmlHttpRequest, textStatus, errorThrown) {
			console.log(XmlHttpRequest);
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
}

/**
 * 读取文件后缀名称，并转化成小写
 * @param file_name
 * @returns
 */
function houzuiToLowerCase(file_name) {
	if (file_name == null || file_name.length == 0)
		return null;
	var result = /\.[^\.]+/.exec(file_name);
	return result == null ? null : (result + "").toLowerCase();
}


/**
 * 判断是否网址
 * @param e
 */
function isURL(e) {
	var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
			+ "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" // ftp的user@
			+ "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
			+ "|" // 允许IP和DOMAIN（域名）
			+ "([0-9a-z_!~*'()-]+\.)*" // 域名- www.
			+ "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // 二级域名
			+ "[a-z]{2,6})" // first level domain- .com or .museum
			+ "(:[0-9]{1,4})?" // 端口- :80
			+ "((/?)|" // a slash isn't required if there is no file name
			+ "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
	var re = new RegExp(strRegex);
	// re.test()
	if (e.value != "") {
		if (!re.test(e.value)) {
			j_tip("请输入正确的连接网址","error");
			//e.value = "";
			e.focus();
		}
	}
}



/**
 * 去除百分符号
 */
function qcbffh(e){
	
}

function check_number(e) {
	var re = /^((\d+\.?\d*)|(\d*\.\d+))$/;
	if (e.value != "") {
		if (!re.test(e.value)) {
			j_tip("请输入正确的数字","error");
			e.value = "";
			e.focus();
		}
	}
}

function check_number_s(e) {
	var re = /^\d+(?=\.{0,1}\d+$|$)/;
	if (e.value != "") {
		if (!re.test(e.value)) {
			j_tip("请输入正确的数字","error");
			e.value = "";
			e.focus();
		}
	}
}


function onoclick_select(obj){
	$("#navList").children("a").each(function(){
    	$(this).css("background-color","");
    });
	$(obj).css("background-color","#bebec5");
}

function next_page(){
	var obj = null;
	$("#navList").children("a").each(function(){
		var b = $(this).attr("class");
		if( b == "navList_a_select"){
			obj = $(this);
			return false;
		}
    });
    if( obj != null ){
    	//$(obj).parent().next()[0].click();
    	$(obj).next()[0].click();
    }
}

function prev_page(){
	var obj = null;
	$("#navList").children("a").each(function(){
		var b = $(this).attr("class");
		if( b == "navList_a_select"){
			obj = $(this);
		}
    });
    if( obj != null ){
    	$(obj).prev()[0].click();
    }
}


function toFixed(number, precision) {
	var b = 1;
	if (isNaN(number))
		return number;
	if (number < 0)
		b = -1;
	var multiplier = Math.pow(10, precision);
	return Math.round(Math.abs(number) * multiplier) / multiplier * b;
}

function load_href(url){
	window.location.href=url;
}

function zhszfd(value,id,hid,formatnum,hidformatnum){
	if( value == undefined || value == null || value.length == 0 ){
		$('#'+id).val("");
		return ;
	}
	value = value.replace(/,/g,'');
	if(isNaN(value)){
		return ;
	}
	if( formatnum == undefined || formatnum == null || formatnum.length == 0 ){
		formatnum = 2 ;
	}
	if( hidformatnum == undefined || hidformatnum == null || hidformatnum.length == 0 ){
		hidformatnum = formatnum ;
	}
	// dadsa  /^[+-]?\d*[.]?\d*$/
	$('#'+id).val(format3(Number(value.replace(/[^-0-9.]/g,'')),hidformatnum));
	$('#'+hid).val(format2(Number($('#'+id).val()),formatnum));
}