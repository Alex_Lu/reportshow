var PAGE_ROWS = 10;
var querystr = '';
var version = 1;

function binderweima(){
	$(".erweima").mouseover(function(){
		if($(this).html()==''){
			return;
		}
		var offset = $(this).offset();
		$('#erweima').parent().css('left',offset.left-120);
		$('#erweima').parent().css('top',offset.top);
		var imgpath = $(this).find('.noshow').html();
		$('#erweima').attr('src',basePath+'/'+imgpath);
		$('#erweima').parent().removeClass('noshow');
	});
	$(".erweima").mouseout(function(){
		if(!$('#erweima').parent().hasClass('noshow')){
			$('#erweima').parent().addClass('noshow');
		}
	});
}

$('#inputGgtype').change(function(){
	var p1=$(this).children('option:selected').val();
	if(p1=='001-003'){
		$('.themefield').css('display','block');
	}else{
		$('.themefield').css('display','none');
	}
});

$(function($) {
	//加载数据
	initRecords();
	$('#addform').bootstrapValidator({
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
    	  ggname: {
              group: '.controls',
              validators: {
                  notEmpty: {
                      message: '公告名称不可为空'
                  }
              }
          },
          ggtype: {
        	  group:'.controlsggtype',
              validators: {
                  callback: {
                      message: '功能开发中···',
                      callback: function(value, validator) {
                    	  if(value == '001-001' || value == '001-002' || value == '001-003'){
                    		  //$('#inputGgname').val(company+'2015年度'+typemap.get(value)+"公告");
                    		  $('#inputGgname').val(company+"2015年度报告");
                    	  }
                          return (value == '001-001' || value == '001-002' || value == '001-003');
                      }
                  }
              }
          }
      }
  });
	
  //$('#inputGgname').val(company+'2015年度'+typemap.get($('#inputGgtype').val())+"公告");
  $('#inputGgname').val(company+"2015年度报告");
	
});

function initRecords(){
	version = version+1;
	$.ajax({
		url : webPath + '/report/page'+querystr,
		type : 'post',
		cache : false,
		data : {
			page : 1,
			rows:PAGE_ROWS
		},
		success : function(data) {
			var success = jQuery.parseJSON(data).success;
			if(success){
				var page = jQuery.parseJSON(data).page;
				$('#pagination-report').children().remove();
				$('#pagination-report').removeClass('pagination');
				if(page.totalProperty==0){
					renderNulllpage();
				}else{
					var showpage = 5;
					var totalPage = Math.floor((page.totalProperty+PAGE_ROWS-1)/PAGE_ROWS);
					if(totalPage<showpage){
						showpage = totalPage;
					}
					$('#pagination-report').twbsPagination({
						totalPages:totalPage ,
						visiblePages: showpage,
						version: '1.1',
						onPageClick: function (event, page) {
							loadpage(page);
						}
					});
					renderpage(page);
				}
				
			}else{
				j_tip('数据查询失败，请稍后重试！','error');
			}
		}
	});
}

function renderNulllpage(){
	var html = "<tr><th><input type=\"checkbox\" id=\"check_all\"/></th><th>公告类型</th><th>公告名称</th><th>二维码</th><th>创建时间</th><th>操作</th></tr>";
	html = html+"<tr class=\"zwsjtr\"><td colspan=\"7\">暂无数据···</td></tr>";
	$('.table').html(html);
}

function loadpage(pagenum){
	$.ajax({
		url : webPath + '/report/page'+querystr,
		type : 'post',
		cache : false,
		data : {
			page : pagenum,
			rows:PAGE_ROWS
		},
		success : function(data) {
			var success = jQuery.parseJSON(data).success;
			if(success){
				var page = jQuery.parseJSON(data).page;
				renderpage(page);
			}else{
				j_tip('数据查询失败，请稍后重试！','error');
			}
		}
	});
}

function renderpage(page){
	var datas = page.datas;
	var html = "<tr><th><input type=\"checkbox\" id=\"check_all\"/></th><th>公告类型</th><th>公告名称</th><th>二维码</th><th>创建时间</th><th>操作</th></tr>";
	for(var i=0;i<datas.length;i++){
		var thisDate = new Date(datas[i].createdate.time);
		var type = datas[i].report_type_em;
		if(typemap.get(type)!=null && typemap.get(type)!=''){
			type = typemap.get(type);
		}
		var theme = datas[i].report_theme;
		if(thememap.get(theme)!=null && thememap.get(theme)!=''){
			theme = thememap.get(theme);
		}
		var datestr = thisDate.getFullYear()+"-"+formatdate((thisDate.getMonth()+1))+"-"+formatdate(thisDate.getDate())+" "+formatdate(thisDate.getHours())+":"+formatdate(thisDate.getMinutes())+":"+formatdate(thisDate.getSeconds());
		var ewm = "<span class=\"noshow\">"+datas[i].qr_code+"</span><span class=\"glyphicon glyphicon-qrcode\" aria-hidden=\"true\"></span>";
		var typecode = datas[i].report_type_em.replace('001-','');
		var yulan = "<a target=\"_blank\" href=\""+basePath+"/upload/"+datas[i].report_type_em+'/'+datas[i].idStr+"/htm.htm\">预览</a>";
		if(datas[i].qr_code==null || datas[i].qr_code==''){
			ewm = '';
			yulan='';
		}
		var editlink = webPath+'/reportedit/view/'+datas[i].report_type_em+'/'+datas[i].idStr+'/001';
		var pagelink = webPath+'/report_page/'+datas[i].idStr;
		var operationTd = yulan + "";
		operationTd +="&nbsp;&nbsp;<a target=\"_self\" href=\""+editlink+"\">编辑数据</a>";
		if('001-003'==datas[i].report_type_em) {
			operationTd +="&nbsp;&nbsp;<a target=\"_self\" href=\""+pagelink+"\">编辑页面</a>";
		}
		var tr = "<tr><td><input type=\"checkbox\" id=\"check_"+datas[i].idStr+"\"/></td><td>"+type+"</td><td>"+datas[i].report_name+"</td><td class=\"erweima\" id=\"ewm_"+datas[i].idStr+"\">"+ewm+"</td><td>"+datestr+"</td><td>"+operationTd+"</td></tr>";
		html = html+tr;
	}
	$('.table').html(html);
	binderweima();
	bindcheckbox();
}

function bindcheckbox(){
	$("#check_all").click(function(){   
	    if($(this).prop("checked")==true){   
	    	$('.table input[type="checkbox"]').prop('checked',true);
	    }else{   
	    	$('.table input[type="checkbox"]').prop('checked',false);
	    }   
	});
}

function formatdate(num){
	if(num<10){
		return '0'+num;
	}else{
		return num;
	}
	
}

/**
 * 创建公告
 */
function addReport(){
	var name = $('#inputGgname').val();
	var type = $('#inputGgtype').val();
	var theme =$('#inputGgtheme').val();
	var date = $('#inputGgdate').val();
	var unit = $('#inputGgunit').val();
	//表单验证
	$('#addform').bootstrapValidator('validate');
	if(!$('#addform').data('bootstrapValidator').isValid()){
		return;
	}
	//提交数据
	$.ajax({
		url : webPath + '/report/add',
		type : 'post',
		cache : false,
		data : {
			report_type_em : type,
			report_theme:theme,
			report_name:name,
			date:date,
			unit:unit
		},
		success : function(data) {
			var success = jQuery.parseJSON(data).success;
			if(success){
				$(".canceladd").trigger("click");
				j_tip('保存成功','success');
				var reportId = jQuery.parseJSON(data).reportId;
				setTimeout('window.location="'+webPath+'/reportedit/view/'+type+'/'+reportId+'/001"', 1200); 
			}else{
				j_tip('保存失败，请稍后重试！','error');
			}
		}
	});
}

/**
 * 删公告
 */
function deleteReport(){
	var checks = $('.table input[type="checkbox"]');
	var selectCount = 0;
	var ids = '';
	for(var i=0;i<checks.length;i++){
		if($(checks[i]).attr('id')=='check_all'){
			continue;
		}
		if($(checks[i]).prop('checked')==true){
			selectCount = selectCount+1;
			ids = ids+$(checks[i]).attr('id').replace('check_', '')+',';
		}
	}
	if(selectCount==0){
		j_tip('请选择要删除的记录','warning');
		return;
	}else{
		//提交数据
		$.ajax({
			url : webPath + '/report/delete',
			type : 'post',
			cache : false,
			data : {
				ids : ids
			},
			success : function(data) {
				var success = jQuery.parseJSON(data).success;
				if(success){
					j_tip('删除成功','success');
					initRecords();
				}else{
					j_tip('删除失败，请稍后重试！','error');
				}
			}
		});
	}
}


function valiDeleteSelectReportAjax(){
	var checks = $('.table input[type="checkbox"]');
	var selectCount = 0;
	var ids = '';
	for(var i=0;i<checks.length;i++){
		if($(checks[i]).attr('id')=='check_all'){
			continue;
		}
		if($(checks[i]).prop('checked')==true){
			selectCount = selectCount+1;
			ids = ids+$(checks[i]).attr('id').replace('check_', '')+',';
		}
	}
	if(selectCount==0){
		j_tip('请选择要删除的记录','warning');
		return;
	}else{
		$('#myDelete').modal('show');
	}

}



/**
 * 重新加载列表
 */
function querylist(){
	var queryname = $('#inputname').val();
	var querydate = $('#inputdate').val();
	var querytype = $('#inputselect').val();
	var str = '';
	if(queryname!=null && queryname.length>0){
		str = '&report_name='+queryname;
	}
	if(querydate!=null && querydate!=''){
		str = str+'&createdate='+querydate;
	}
	if(querytype!='全部类型'){
		str = str+'&report_type_em='+querytype;
	}
	if(str!=''){
		querystr = str.replace('&','?');
	}else{
		querystr='';
	}
	initRecords();
}

function resetadd(){
	$('#inputGgname').val(company+'2015年度'+typemap.get($('#inputGgtype').val()));
	$('#addform').data('bootstrapValidator').resetForm();
}

/**
 * 发布公告
 */
function publishReport(){
	var checks = $('.table input[type="checkbox"]');
	var selectCount = 0;
	var ids = '';
	for(var i=0;i<checks.length;i++){
		if($(checks[i]).attr('id')=='check_all'){
			continue;
		}
		if($(checks[i]).prop('checked')==true){
			selectCount = selectCount+1;
			ids = ids+$(checks[i]).attr('id').replace('check_', '')+',';
		}
	}
	if(selectCount==0){
		j_tip('请选择要发布的记录','warning');
		return;
	}else{
		//提交数据
		$.ajax({
			url : webPath + '/report/publish',
			type : 'post',
			cache : false,
			data : {
				ids : ids
			},
			success : function(data) {
				var success = jQuery.parseJSON(data).success;
				if(success){
					j_tip('发布成功','success');
					initRecords();
				}else{
					j_tip('发布失败，请稍后重试！','error');
				}
			}
		});
	}
}

function goEditPage(type,reportId){
	window.location=webPath+"/reportedit/index?reportType="+type+"&reportIdstr="+reportId;
}