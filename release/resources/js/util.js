Array.prototype.remove = function(s) {
    for (var i = 0; i < this.length; i++) {
        if (s == this[i])
            this.splice(i, 1);
    }
}

/**
 * Simple Map
 * 
 * 
 * var m = new Map();
 * m.put('key','value');
 * ...
 * var s = "";
 * m.each(function(key,value,index){
 *         s += index+":"+ key+"="+value+"\n";
 * });
 * alert(s);
 * 
 * @author dewitt
 * @date 2008-05-24
 */
function Map() {
    /** 存放键的数组(遍历用到) */
    this.keys = new Array();
    /** 存放数据 */
    this.data = new Object();
    
    /**
     * 放入一个键值对
     * @param {String} key
     * @param {Object} value
     */
    this.put = function(key, value) {
        if(this.data[key] == null){
            this.keys.push(key);
        }
        this.data[key] = value;
    };
    
    /**
     * 获取某键对应的值
     * @param {String} key
     * @return {Object} value
     */
    this.get = function(key) {
        return this.data[key];
    };
    
    /**
     * 删除一个键值对
     * @param {String} key
     */
    this.remove = function(key) {
        this.keys.remove(key);
        this.data[key] = null;
    };
    
    /**
     * 遍历Map,执行处理函数
     * 
     * @param {Function} 回调函数 function(key,value,index){..}
     */
    this.each = function(fn){
        if(typeof fn != 'function'){
            return;
        }
        var len = this.keys.length;
        for(var i=0;i<len;i++){
            var k = this.keys[i];
            fn(k,this.data[k],i);
        }
    };
    
    /**
     * 获取键值数组(类似Java的entrySet())
     * @return 键值对象{key,value}的数组
     */
    this.entrys = function() {
        var len = this.keys.length;
        var entrys = new Array(len);
        for (var i = 0; i < len; i++) {
            entrys[i] = {
                key : this.keys[i],
                value : this.data[i]
            };
        }
        return entrys;
    };
    
    /**
     * 判断Map是否为空
     */
    this.isEmpty = function() {
        return this.keys.length == 0;
    };
    
    /**
     * 获取键值对数量
     */
    this.size = function(){
        return this.keys.length;
    };
    
    /**
     * 重写toString 
     */
    this.toString = function(){
        var s = "{";
        for(var i=0;i<this.keys.length;i++,s+=','){
            var k = this.keys[i];
            s += k+"="+this.data[k];
        }
        s+="}";
        return s;
    };
}


/**
 * 千分符格式化
 * @param num
 * @returns
 */
function format1 (num) {
	if( num == null || num == undefined || num == ""  ){
		return "";
	}
	if( isNaN(num)) return num;
    return (num.toFixed(2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}

/**
 * 千分符格式化
 * @param num
 * @returns
 */
function format2 (num,n) {
	if( num == null || num == undefined || num == ""  ){
		return "";
	}
	if(  n == null || n == undefined || n == ""  || isNaN(n) ){
		n = 0;
	}
	if( isNaN(num)) return num;
    return (num.toFixed(n) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
}

/**
 * 保留小数点
 * @param num
 * @returns
 */
function format3 (num) {
	if( num == null || num == undefined || num == ""  ){
		return "";
	}
	if( isNaN(num)) return num;
    return (num.toFixed(2) + '');
}


//通用TIP提示方法
function j_tip(message,type){

	var toastrObj = toastr;
//	if (window.parent.toastr) {
//		toastrObj = window.parent.toastr;
//	}

	if (type == undefined || type == null) {
		toastrObj.info(message);
	} else if (type == "error") {
		toastrObj.error(message);
	} else if (type == "success") {
		toastrObj.success(message);
	} else if (type == "warning") {
		toastrObj.warning(message);
	} else {
		toastrObj.info(message);
	}
}