var PAGE_ROWS = 10;
var telephoneValidate = /^13[0-9]{9}|15[012356789][0-9]{8}|17[3678][0-9]{8}|18[0-9]{9}|147[0-9]{8}$/;
var emailValidate = /\w@\w*\.\w/;
$(function($) {
	//加载数据

	$.ajax({
		url : webPath + '/userctrl/page',
		type : 'post',
		cache : false,
		data : {
			page : 1,
			rows:PAGE_ROWS
		},
		success : function(data) {
			var success = jQuery.parseJSON(data).success;
			if(success){
				var page = jQuery.parseJSON(data).page;
				$('#pagination-report').children().remove();
				$('#pagination-report').removeClass('pagination');
				if(page.totalProperty==0){
					renderNulllpage();
				}else{
					var showpage = 5;
					var totalPage = Math.floor((page.totalProperty+PAGE_ROWS-1)/PAGE_ROWS);
					if(totalPage<showpage){
						showpage = totalPage;
					}
					$('#pagination-report').twbsPagination({
						totalPages:totalPage ,
						visiblePages: showpage,
						version: '1.1',
						onPageClick: function (event, page) {
							loadpage(page);
						}
					});
					renderpage(page);
				}
				
			}else{
				j_tip('数据查询失败，请稍后重试！','error');
			}
		}
	});
});

function renderNulllpage(){
	var html = "<tr><th><input type=\"checkbox\" id=\"check_all\"/></th><th>手机号</th><th>公司</th><th>创建时间</th><th>邮箱</th></tr>";
	html = html+"<tr class=\"zwsjtr\"><td colspan=\"6\">暂无数据···</td></tr>";
	$('.table').html(html);
}

function loadpage(pagenum){
	$.ajax({
		url : webPath + '/userctrl/page',
		type : 'post',
		cache : false,
		data : {
			page : pagenum,
			rows:PAGE_ROWS
		},
		success : function(data) {
			var success = jQuery.parseJSON(data).success;
			if(success){
				var page = jQuery.parseJSON(data).page;
				renderpage(page);
			}else{
				j_tip('数据查询失败，请稍后重试！','error');
			}
		}
	});
}

function renderpage(page){
	var datas = page.datas;
	var html = "<tr><th><input type=\"checkbox\" id=\"check_all\"/></th><th>手机号</th><th>公司</th><th>创建时间</th><th>邮箱</th></tr>";
	for(var i=0;i<datas.length;i++){
		var thisDate = new Date(datas[i].createdate.time);
//		var type = datas[i].report_type_em;
//		if(typemap.get(type)!=null && typemap.get(type)!=''){
//			type = typemap.get(type);
//		}
//		var theme = datas[i].report_theme;
//		if(thememap.get(theme)!=null && thememap.get(theme)!=''){
//			theme = thememap.get(theme);
//		}
		var datestr = thisDate.getFullYear()+"-"+formatdate((thisDate.getMonth()+1))+"-"+formatdate(thisDate.getDate())+" "+formatdate(thisDate.getHours())+":"+formatdate(thisDate.getMinutes())+":"+formatdate(thisDate.getSeconds());
//		var ewm = "<span class=\"noshow\">"+datas[i].qr_code+"</span><span class=\"glyphicon glyphicon-qrcode\" aria-hidden=\"true\"></span>";
//		var typecode = datas[i].report_type_em.replace('001-','');
//		var yulan = "<a target=\"_blank\" href=\""+basePath+"/upload/"+datas[i].report_type_em+'/'+datas[i].idStr+"/htm.htm\">预览</a>";
//		if(datas[i].qr_code==null || datas[i].qr_code==''){
//			ewm = '';
//			yulan='';
//		}
//		var editlink = webPath+'/reportedit/view/'+datas[i].report_type_em+'/'+datas[i].idStr+'/001';
//		var pagelink = webPath+'/report_page/'+datas[i].idStr;
//		var operationTd = yulan + "";
//		operationTd +="&nbsp;&nbsp;<a target=\"_self\" href=\""+editlink+"\">编辑数据</a>";
//		if('001-003'==datas[i].report_type_em) {
//			operationTd +="&nbsp;&nbsp;<a target=\"_self\" href=\""+pagelink+"\">编辑页面</a>";
//		}
		var tr = "<tr><td><input type=\"checkbox\" id=\"check_"+datas[i].idStr+"\"/></td><td>"+datas[i].telephone+"</td><td >"+datas[i].company+"</td><td>"+datestr+"</td><td>"+datas[i].email+"</td></tr>";
		html = html+tr;
	}
	$('.table').html(html);
	bindcheckbox();
}

function bindcheckbox(){
	$("#check_all").click(function(){   
	    if($(this).prop("checked")==true){   
	    	$('.table input[type="checkbox"]').prop('checked',true);
	    }else{   
	    	$('.table input[type="checkbox"]').prop('checked',false);
	    }   
	});
}

function formatdate(num){
	if(num<10){
		return '0'+num;
	}else{
		return num;
	}
	
}

function addUser(){
	var telephone = $('#telephone').val();
	var company =$('#company').val();
	var email = $('#email').val();
	var password = $('#password').val();
	//表单验证
	if (!checkInput()) {
    	return false;
    } else {
	//提交数据
	$.ajax({
		url : webPath + '/userctrl/addUser',
		type : 'post',
		cache : false,
		data : {
			telephone:telephone,
			company:company,
			email:email,
			password:password
		},
		success : function(data) {
			var success = jQuery.parseJSON(data).success;
			var msg = jQuery.parseJSON(data).msg;
			if(success){
//				$(".canceladd").trigger("click");
				j_tip('保存成功','success');
				location.reload();
//				var reportId = jQuery.parseJSON(data).reportId;
//				setTimeout('window.location="'+webPath+'/reportedit/view/'+type+'/'+reportId+'/001"', 1200); 
			}else{
				j_tip('保存失败,'+msg,'error');
			}
		}
	});
    }
}

/**检查输入项是否为空
 * @param 是否检查通过
 */
function checkInput() {
    if ($('#telephone').val().length == 0) {
    	j_tip('请输入手机号');
    	$('#telephone').focus();
        return false;
    }
    
    if ($('#telephone').val().length != 11) {
    	j_tip('手机号码长度应为11位');
    	$('#telephone').focus();
        return false;
    }
    
    if (!telephoneValidate.test($('#telephone').val())) {
    	j_tip('你输入的手机号码无效');
    	$('#telephone').focus();
        return false;
    }
    
    if ($('#password').val().length == 0) {
    	j_tip('请输入密码');
    	$('#password').focus();
        return false;
    }
    
    if ($('#password').val().length < 6) {
    	j_tip('密码长度不能小于6');
    	$('#password').focus();
        return false;
    }
    
    if ($('#password').val().length > 15) {
    	j_tip('密码长度不能大于15');
    	$('#password').focus();
        return false;
    }
    
    if ($('#password2').val().length == 0) {
    	j_tip('请输入确认密码');
    	$('#password2').focus();
        return false;
    }
    
    if ($('#password').val() != $('#password2').val()) {
    	j_tip('2次输入的密码不相符');
    	$('#password2').focus();
        return false;
    }
    if ($('#email').val().length == 0) {
    	j_tip('请输入邮箱');
    	$('#email').focus();
    	return false;
    }
    if (!emailValidate.test($('#email').val())) {
    	j_tip('你输入的邮箱无效');
    	$('#email').focus();
    	return false;
    }
    return true;
}