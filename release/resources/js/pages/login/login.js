/**回车
 */
function enterkey() {
	//兼容IE或其它其它浏览器
    var event = arguments[0] || window.event;
    //兼容IE或其它浏览器
    var obj = event.srcElement ? event.srcElement : event.target;
    var targetType = obj.type;
    if (event.keyCode == 13) {
	    //在按扭上点击回车事件，触发了按扭的单击事件，不用再重复提交
	    if('button'==targetType || 'reset'==targetType) return;
	    loginbutton();
    }
};

$(function() {
    //显示验证码
    refreshChart();
    //loginForm.j_username.focus();
});

/**检查输入项是否为空
 * @param 是否检查通过
 */
function checkInput() {
    if (loginForm.j_username.value.length == 0) {
    	showMsg('请输入用户名');
        loginForm.j_username.focus();
        return false;
    }
    
    if (loginForm.j_password.value.length == 0) {
    	showMsg('请输入密码');
        loginForm.j_password.focus();
        return false;
    }
    
    if (loginForm.validateCode.value.length == 0) {
    	showMsg('请输入验证码');
        loginForm.validateCode.focus();
        return false;
    }
    return true;
};

function loginbutton() {
	var username=$('#username').val();
	var password=$('#password').val();
	var vCode=$('#validateCode').val();
    if (!checkInput()) {
    	return false;
    } else {
//    	loginForm.action=getContextPath()+"/web/j_spring_security_check";
//    	doSubmit();
//    	return true;
    	$.ajax({
			url : getContextPath() + "/web/loginctrl/login_report",
			type : "POST",
			dataType : "json",
			data : {
				"user" : username,
				"pwd" : password,
				"vCode" : vCode,
			},
			success : function(data) {
				if (data.success) {
					window.location=getContextPath()+"/web/report/listview";
				} else {
					showMsg(data.msg);
					refreshChart();
				}
			},
			failure : function() {
				alert("登录失败！");
				refreshChart();
			}
		});
    	console.log(getContextPath()+"/web/login_report");
    }
};

