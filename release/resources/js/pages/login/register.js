var telephoneValidate = /^13[0-9]{9}|15[012356789][0-9]{8}|17[3678][0-9]{8}|18[0-9]{9}|147[0-9]{8}$/;
var emailValidate = /\w@\w*\.\w/;
$(function() {
    //显示验证码
    refreshChart();
    //loginForm.j_username.focus();
});

/**检查输入项是否为空
 * @param 是否检查通过
 */
function checkInput() {
    if (loginForm.j_username.value.length == 0) {
    	showMsg('请输入邮箱');
        loginForm.j_username.focus();
        return false;
    }
    if (!emailValidate.test(loginForm.j_username.value)) {
    	showMsg('你输入的邮箱无效');
        loginForm.j_username.focus();
        return false;
    }
    if (loginForm.j_telephone.value.length == 0) {
    	showMsg('请输入手机号');
        loginForm.j_telephone.focus();
        return false;
    }
    
    if (loginForm.j_telephone.value.length != 11) {
    	showMsg('手机号码长度应为11位');
        loginForm.j_telephone.focus();
        return false;
    }
    
    if (!telephoneValidate.test(loginForm.j_telephone.value)) {
    	showMsg('你输入的手机号码无效');
        loginForm.j_telephone.focus();
        return false;
    }
    
    if (loginForm.j_password_1.value.length == 0) {
    	showMsg('请输入密码');
        loginForm.j_password_1.focus();
        return false;
    }
    
    if (loginForm.j_password_1.value.length < 6) {
    	showMsg('密码长度不能小于6');
        loginForm.j_password_1.focus();
        return false;
    }
    
    if (loginForm.j_password_1.value.length > 15) {
    	showMsg('密码长度不能大于15');
        loginForm.j_password_1.focus();
        return false;
    }
    
    if (loginForm.j_password_2.value.length == 0) {
    	showMsg('请输入确认密码');
        loginForm.j_password_2.focus();
        return false;
    }
    
    if (loginForm.j_password_2.value != loginForm.j_password_1.value) {
    	showMsg('2次输入的密码不相符');
        loginForm.j_password_2.focus();
        return false;
    }
    
    if (loginForm.validateCode.value.length == 0) {
    	showMsg('请输入验证码');
        loginForm.validateCode.focus();
        return false;
    }
    
    if (loginForm.message.value.length == 0) {
    	showMsg('请输入短信验证码');
        loginForm.message.focus();
        return false;
    }
    return true;
};

function registerbutton() {
	var email=$('#username').val();
	var telephone=$('#telephone').val();
	var password=$('#pwd1').val();
	var company=$('#company').val();
	var vCode=$('#validateCode').val();
	var message=$('#message').val();
    if (!checkInput()) {
    	return false;
    } else {
//    	loginForm.action=getContextPath()+"/web/j_spring_security_check";
//    	doSubmit();
//    	return true;
    	$.ajax({
			url : getContextPath() + "/web/loginctrl/newregister_user",
			type : "POST",
			dataType : "json",
			data : {
				"email" : email,
				"telephone" : telephone,
				"password" : password,
				"company":company,
				"validateCodes" : vCode,
				"u_test" : message,
			},
			success : function(data) {
				if (data.success) {
					showMsg("注册成功！");
					location.href = getContextPath()+"/login.html";
				} else {
					showMsg(data.msg);
					refreshChart();
				}
			},
			failure : function() {
				alert("注册失败！");
				refreshChart();
			}
		});
    }
};
