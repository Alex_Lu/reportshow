
/**重新获得验证码
 */
function refreshChart() {
	var chart = document.getElementById("chart");
	if(chart!=null) chart.src = getContextPath() + "/web/cmndd/rand_image?t=" + new Date().getTime()+"&imageKey=validateCode";
};

function getContextPath() {
    var pathName = document.location.pathname;
    var index = pathName.substr(1).indexOf("/");
    var result = pathName.substr(0,index+1);
    return result;
}

function showMsg(msg) {
	alert(msg);
}

function register(){
	$('#register').attr('href',getContextPath()+'/register.html'); 
}

function forgetPwd(){
	$('#forgetPwd').attr('href',getContextPath()+'/forgetPwd.html'); 
}

function login(){
	$('#baklogin').attr('href',getContextPath()+'/login.html'); 
}

function loginManager(){
	$('#loginManager').attr('href',getContextPath()+'/login_manager.html'); 
}

function getMessage(){
	var telephone = $("#telephone").val();
	if(telephone==null||telephone==""||telephone==undefined){
		alert("请输入您的手机号");
		return;
	}
	var curCount = 60;
	var iCount = setInterval(function() {
		var btnObj = $("#messagebtn");
		if (curCount <= 0) {
			clearInterval(iCount);
			btnObj.removeAttr("disabled");// 启用按钮
			btnObj.css("cursor", "pointer");
			btnObj.css("background", "#70ca10");
			btnObj.val("免费获取短信验证码");
		} else {
			btnObj.attr("disabled", "true");
			btnObj.css("background", "#d4d4d4");
			btnObj.css("cursor", "default");
			btnObj.val("重新获取验证码(" + curCount + ")");
			curCount--;
		}
	}, 1000);
	$.ajax({
		url : getContextPath() + '/web/smscoderandctrl/newsend_sms_code_rand?u_telephone='+ $("#telephone").val(),
		type : 'get',
		dateType : 'json',
		data : {"validateCodes": $("#validateCode").val()},
		success : function(data) {
			data = jQuery.parseJSON(data);
			if (data.curCount != null&& data.curCount != undefined) {
				curCount = data.curCount;
			}
//			showMsg(data.msg);
//			if (data.msg == "success")
//				$("#test").hide();
//			else
//				$("#test").show();
		}

	});
	
}