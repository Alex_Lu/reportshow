/**回车
 */
function enterkey() {
	//兼容IE或其它其它浏览器
    var event = arguments[0] || window.event;
    //兼容IE或其它浏览器
    var obj = event.srcElement ? event.srcElement : event.target;
    var targetType = obj.type;
    if (event.keyCode == 13) {
	    //在按扭上点击回车事件，触发了按扭的单击事件，不用再重复提交
	    if('button'==targetType || 'reset'==targetType) return;
	    loginMButton();
    }
};

/**重新获得验证码
 */
function refreshChart() {
	var chart = document.getElementById("chart");
	if(chart!=null) chart.src = webPath + "/cmndd/rand_image?t=" + new Date().getTime()+"&imageKey=validateCode";
};

$(function() {
    //显示验证码
    refreshChart();
    //loginForm.j_username.focus();
});

/**检查输入项是否为空
 * @param 是否检查通过
 */
function checkInput() {
    if (loginForm.j_username.value.length == 0) {
    	alert('请输入用户名');
        loginForm.j_username.focus();
        return false;
    }
    
    if (loginForm.j_password.value.length == 0) {
    	alert('请输入密码');
        loginForm.j_password.focus();
        return false;
    }
    
    if (loginForm.validateCode.value.length == 0) {
    	alert('请输入验证码');
        loginForm.validateCode.focus();
        return false;
    }
    return true;
};

function loginMButton() {
	var username=$('#username').val();
	var password=$('#password').val();
	var vCode=$('#validateCode').val();
    if (!checkInput()) {
    	return false;
    } else {
    	loginForm.action=webPath+"/j_spring_security_check";
    	doSubmit();
    	return true;
    }
    
    function doSubmit() {
        var sha256 = pidCrypt.SHA256(loginForm.j_password.value);
        loginForm.j_password.value = sha256;
        loginForm.submit();
    }
};
