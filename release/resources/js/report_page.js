$(function(){
	$('.btn.remove').click(function(){
		$(this).parent().detach();
	});
	var dropable = $('#dropzone').droppable({
		activeClass : 'active',
		hoverClass : 'hover',
		accept : ":not(.ui-sortable-helper)"
	});
	dropable.sortable({
		items : '.drop-item',
		sort : function() {
			$(this).removeClass("active");
		}
	});
	$('#report_page_save_btn').click(function(){
		save();
	});
	$('#report_page_def_btn').click(function(){
		if(confirm("确定要恢复页面数据吗？")) {
			set_def();
		}
	});
	
});
function save() {
	var items = $('#dropzone .item');
	if(items==null || items.size()==0) {
		j_tip('无数据，不能保存','error');
		return;
	}
	if(items.size()<10) {
		j_tip('请至少保留10个页面','error');
		return;
	}
	var pageList = [];
	for(var i = 0; i < items.size(); i++) {
		var pageIdStr = $(items[i]).attr('idStr');
		pageList.push(pageIdStr);
	}
	
	$.ajax({
		url : webPath + '/report_page/save',
		type : 'post',
		cache : false,
		data : {
			pageList : JSON.stringify(pageList),
			reportId:reportId
		},
		success : function(data) {
			data = JSON.parse(data);
			if(data && data.success) {
				j_tip(data.msg+"",'success');
				setTimeout(function(){
					window.location.href=webPath+"/report/listview";
				}, 600);
			} else {
				j_tip(data.msg,'error');
			}
		},
		error:function() {
			alert('服务器异常');
		}
	});
}
function set_def() {
	$.ajax({
		url : webPath + '/report_page/set_def',
		type : 'post',
		cache : false,
		data : {reportId:reportId},
		success : function(data) {
			data = JSON.parse(data);
			if(data && data.success) {
				j_tip(data.msg+"",'success');
				setTimeout(function(){
					location.reload();
				}, 600);
			} else {
				j_tip(data.msg,'error');
			}
		},
		error:function() {
			alert('服务器异常');
		}
	});
}

