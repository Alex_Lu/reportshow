<#include "ftl/include.ftl">
<html>
<head>
<meta charset="UTF-8">
<title>公告管理后台-登录</title>
<script type="text/javascript" src="${basePath}/resources/js/jquery/jquery-1.10.1.js"></script>
<link rel="stylesheet" type="text/css" href="${basePath}/resources/css/login.css"/>
<script type="text/javascript" src="${basePath}/resources/js/pages/login/loginManager.js"></script>
<script type="text/javascript" src="${basePath}/resources/js/pidcrypt_util.js"></script>
<script type="text/javascript" src="${basePath}/resources/js/pidcrypt.js"></script>
<script type="text/javascript" src="${basePath}/resources/js/sha256.js"></script>
</head>
<body>
	<div id="login" style="font-size:16px;width: 372px;height: 332px;">
			<form action="" method="post" name="loginForm">
			<ul id="login_top">
				<li>管理员登录</li>
			</ul>
			<ul id="login_form" style="padding-left:20px;">
				<li class="login_input login_i1"><label class="usernmae_label" for="username"></label><input autocomplete ="off" id="username" tabindex="1" class="username" name="j_username" id="username" type="text" placeholder="用户名"/></li>
				<li class="login_input login_i2" style="margin-top:20px;"><label class="password_label" for="password"></label><input autocomplete ="off" tabindex="2" class="password" name="j_password" id="password" type="password" placeholder="密码"/></li>
			</ul>
			<ul style="height:30px;padding-left:20px;margin-top:20px;">
				<input type="text" name="validateCode" maxlength="4" id="validateCode" tabindex="3" onKeyDown="javascript:if (event.keyCode==13) loginbutton();" placeholder="验证码" style="margin-left:12px;margin-top:12px;float:left;height:39px;border:1px solid #DDDDDD;width: 37%;"/>
				<div class="codeImg" style="">
					<a href="javascript:refreshChart()"><img id="chart" border="0" src="" title='换一张' style="margin-left:14px;margin-top:12px;height:39px;width:38%"></a>
				</div>
			</ul>
			<div style="clear:both;"></div>
			<!-- <ul id="login_remem" style="margin-right:70px;">
				<li style="float: left;margin-left:32px;"><a id="register" href="#" onclick="register()">用户注册</a></li>
				<li style="float: right;margin-right:24px;"><a id="forgetPwd" href="#" onclick="forgetPwd()">忘记密码</a></li>
			</ul> -->
			<ul id="login_submit">
				<li><input type="submit" onClick="loginMButton()" value="登录" style="width:300px;margin-top:10px;margin-left:20px;"/></li>
			</ul>
			</form>
		</div>
</body>
</html>