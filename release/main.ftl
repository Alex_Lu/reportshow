<#include "ftl/include.ftl">
<html  lang="zh-CN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>用户列表</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width; initial-scale=1.0"/>
		<link href="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="${basePath}/resources/lib/bootstrapValidator/css/bootstrapValidator.css"/>
		<link rel="stylesheet" type="text/css" href="${basePath}/resources/css/index.css"/>
		<script>
		  var company = '';
		  <#if company?exists>
               company = '${company}';
		  </#if>
		</script>
		
	</head>

	<body>
		<#include "ftl/header_1.ftl">
		<div class="container list">
			<div class="btn-toolbar" style="margin-bottom:5px;">
				<div class="btn-group">
				  <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height: 35px;width: 70px;">新建</button>
				</div>
			</div>
				<table class="table table-bordered table-striped">
					<tr><th><input type="checkbox" id="check_all"/></th><th>手机号</th><th>公司</th><th>创建时间</th><th>邮箱</th></tr>
					<tr class="zwsjtr"><td colspan="6">暂无数据···</td></tr>
				</table>
			<ul id="pagination-report" class="pagination-lg" style="float:right;margin-top: 5px;"></ul>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">创建用户</h4>
		      </div>
		      <div class="modal-body" style="width:80%;margin-left:auto;margin-right:auto;">
		        <form class="form-horizontal" id="addform">
				  <div class="form-group">
				    <label for="telephone">手机号</label>
				    <div class="controls">
				      <input type="text" id="telephone" class="form-control" name="telephone" placeholder="手机号">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="passwod">密码</label>
				    <div class="controls">
				      <input type="password" id="password" class="form-control" name="password" placeholder="密码">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="password2">确认密码</label>
				    <div class="controls">
				      <input type="password" id="password2" class="form-control" name="password2" placeholder="确认密码">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="company">公司</label>
				    <div class="controls">
				      <input type="text" id="company" class="form-control" name="company" placeholder="公司">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="email">邮箱</label>
				    <div class="controls">
				      <input type="email" id="email" class="form-control" name="email" placeholder="邮箱">
				    </div>
				  </div>
				</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default canceladd" data-dismiss="modal">取消</button>
		        <button type="button" class="btn btn-primary" onclick="addUser()">保存</button>
		      </div>
		    </div>
		  </div>
		</div>
		
		<script src="${basePath}/resources/lib/jquery-1.11.1.min.js"></script>
    	<script src="${basePath}/resources/lib/bootstrap-3.3.5/bootstrap-3.3.5/dist/js/bootstrap.min.js"></script>
    	<script src="${basePath}/resources/lib/bootstrap/js/jquery.twbsPagination.js"></script>
     	<script src="${basePath}/resources/lib/bootstrapValidator/js/bootstrapValidator.min.js"></script>
     	<script src="${basePath}/resources/js/userlist.js"></script>
	</body>
</html>