Zepto(function() {
	setTimeout(function(){
		$(document).on('touchstart', '.guo1', function(e) {
			$('.guo2').css('opacity', '0');
			$('.guo2').css('-webkit-animation', 'none');
			$('.guo2').css('animation', 'none');
			$(this).find('.guo2').css('-webkit-animation', 'zoomIn .5s ease 0s forwards;');
			$('#gsjy_title').html($(this).attr('title'));
			$('#gsjy_desc').html($(this).attr('desc'));
		});
	},5600);

	setTimeout(function(){
		$(document).on('touchstart', '.touxiang', function(e) {
			$('.touxiang2').css('opacity', '0');
			$('.touxiang2').css('-webkit-animation', 'none');
			$('.touxiang2').css('animation', 'none');
			$(this).find('.touxiang2').css('-webkit-animation', 'zoomIn .5s ease 0s forwards;');
			$('#xm014-1').html($(this).attr('xm'));
			$('#zw014-1').html($(this).attr('zw'));
			$('#jl014-1').html($(this).attr('jl'));
		});
	},5600);
	var bodyScroll = null;
	var yStart = 0;
	var yEnd = 0;
	var height = 0;
	$('#wrapper').on('touchstart',function(e){
		allowSlide = false;
		yStart = e.touches[0].clientY;
		yEnd = yStart;
		height = parseInt($('#scroller').css('height').replace('px',''))-620;
	});
	var top = 0;
	$('#wrapper').on('touchmove',function(e){
		yEnd = e.touches[0].clientY;
		if(yEnd-yStart==0) {
			return;
		}
		var temp = top+yEnd-yStart;
		if(temp>0) temp = 0;
		if(temp<-height) temp = -height;
		$('#scroller').css('top',temp);
	});
	$('#wrapper').on('touchend',function(e){
		setTimeout(function(){
			allowSlide = true;
		},100);
		top +=(yEnd-yStart);
		if(top>0) top = 0;
		if(top<-height) top = -height;
	});
});
function loadPie(id, list) {
	if($("#"+id)==null || $("#"+id).length==0) return;
	var chart = echarts.init($("#"+id)[0]);
	var option = {
			color:['#00b597', '#FFF', '#ffbf4a', '#cc504f', '#91c7ae', '#6e7074', '#61a0a8', '#bda29a', '#44525d', '#c4ccd3'],
			series : [{
				type : 'pie',
				legendHoverLink:false,
				hoverAnimation:false,
				radius : [ '60%', '85%' ],
				label : {
					normal:{
						formatter:function(params){
							var value = format2(parseFloat(params.value,2),2);
							return params.name+'\n'+value;
						},
						textStyle : {
							fontSize : '20',
							color:'#FDE5A9'
						}
					}
				},
				data : list
			}]
		};
	chart.setOption(option);
}


function loadbarChart(pagenum,datamap,hasLabel){
	var bars = $('.'+pagenum+'barchart');
	if(bars!=null && bars.length==3){
		var opetion = {
			    calculable : true,
			    grid:{left:150,top:25},
			    xAxis : [{type : 'category',data : ['2013','2014','2015'],
			            axisLabel : {textStyle:{color:'#fff',fontWeight:'bold',fontSize:'20',}},
			            axisLine:{show:true,lineStyle:{color:'#fff',width:2,type:'dashed'}},splitLine:{show:false}}],
			    yAxis : [{type : 'value',splitNumber:3,
			            axisLabel : {formatter: '{value}',textStyle:{color:'#fff',fontWeight:'bold',fontSize:'20',}},
			            axisLine:{show:true,lineStyle:{color:'#fff',type:'dashed',width:2,opacity:1}},splitLine:{show:false}}],
			    series : [{name:'财务数据',type:'bar',label: {
	                normal: {
	                    show: hasLabel,
	                    position: 'top',
	                    textStyle:{color:'#fff',fontSize:24}
	                }
	            },data:[],barWidth:50,}]
			};
		for(var i=0;i<3;i++){
			opetion.series[0].data=datamap.get(pagenum+i);
			echarts.init(bars[i]).setOption(opetion);
		}
	}
}
function slide(obj) {
	
}
