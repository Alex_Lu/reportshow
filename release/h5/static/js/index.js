
var isPlaying = true;
var showIndex = 0, oldIndex = 0;
var startY, endY, moveY, touchFirst_obj, touchLast_obj;
var nums = 0;
//是否允许拖动,默认为true
var allowSlide = true;
Zepto(function(){
	var mp3 = document.getElementById("bgmusic");
    mp3.play();
    nums = $("#main > article").size();
    $('.music-ico').addClass('rotate').on('touchstart', function () {    
        if (isPlaying) {
            mp3.pause();
            $(this).removeClass('rotate');
            isPlaying = false;
        } else {
            mp3.play();
            $(this).addClass('rotate');
            isPlaying = true;
        }
    });
    $(document).on('touchstart', '#main', function (e) {
//        e.preventDefault();
        touchFirst_obj = {
            startY: e.originalEvent.touches[0].clientY
        };
    });
    $(document).on('touchmove', '#main', function (e) {
        e.preventDefault();
//        if(showIndex == 1) return false;
        var touchLast_obj = e.originalEvent.touches[0];
        moveY = touchLast_obj.clientY - touchFirst_obj.startY;
    });

    $(document).on('touchend', '#main', function (e) {
//        e.preventDefault();
//        if(showIndex == 1) return false;
        if(nums==1 || !allowSlide) return false;
        if (moveY > 20) {
            movepage(-1);
        } else if (moveY < -20) {
            movepage(1);
        }
        moveY = 0;
    });
    afterMove(null,$('section').children().first());
});
var isMoving = false;
function movepage(t) {
	if(isMoving) return;
	isMoving = true;
	oldIndex = showIndex;
	if (t == 1) {
		$('section').children().eq(showIndex).css({'animation': 'moveToTop 0.6s ease forwards', '-webkit-animation': 'moveToTop 0.6s ease forwards' });
        showIndex = showIndex == nums-1 ? 0 : showIndex + 1;
        $('section').children().eq(showIndex).css({ 'display': 'block', 'animation': 'moveFromBottom 0.6s ease forwards', '-webkit-animation': 'moveFromBottom 0.6s ease forwards' }).one('webkitAnimationEnd', function (e) {
        	$('section').children().eq(oldIndex).css('display', 'none');
        	isMoving = false;
        	afterMove($('section').children().eq(oldIndex), $('section').children().eq(showIndex));
        }).one('animationEnd', function (e) {
        	$('section').children().eq(oldIndex).css('display', 'none');
        	isMoving = false;
        	afterMove($('section').children().eq(oldIndex), $('section').children().eq(showIndex));
        });
	} else {
		$('section').children().eq(showIndex).css({'animation': 'moveToBottom .6s ease forwards', '-webkit-animation': 'moveToBottom 0.6s ease forwards' });
        showIndex = showIndex == 0 ? nums-1 : showIndex - 1;
        $('section').children().eq(showIndex).css({ 'display': 'block', 'animation': 'moveFromTop 0.6s ease forwards', '-webkit-animation': 'moveFromTop 0.6s ease forwards' }).one('webkitAnimationEnd', function (e) {
        	$('section').children().eq(oldIndex).css('display', 'none');
        	isMoving = false;
        	afterMove($('section').children().eq(oldIndex), $('section').children().eq(showIndex));
        }).one('animationEnd', function (e) {
        	$('section').children().eq(oldIndex).css('display', 'none');
        	isMoving = false;
        	afterMove($('section').children().eq(oldIndex), $('section').children().eq(showIndex));
        });
	}
}
/**
 * 移动后触发的方法
 * 传入移动后的页面
 */
function afterMove(fromPage,toPage) {
}