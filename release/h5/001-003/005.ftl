
<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
财务数据
</div>

<div style="position: absolute;width:120px;left:10px;bottom:80px;-webkit-animation:upExpand 2s ease 1.1s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/005-1.png" style="width:100%;">
</div>
<div class="zhuzhuangtu" style="top:150px; -webkit-animation:downExpand .6s ease 1.6s forwards;opacity:0;">
	<div class="title">营业收入（${report_unit_name!}）</div>
	<div style="width:510px;height:213px;"  id="main00501" class="005barchart"></div>
</div>
<div class="zhuzhuangtu" style="top:400px; -webkit-animation:downExpand .6s ease 2.1s forwards;opacity:0;">
	<div class="title">归属于母公司的净利润（${report_unit_name!}）</div>
	<div style="width:510px;height:213px;"  id="main00502" class="005barchart"></div>
</div>
<div class="zhuzhuangtu" style="top:680px; -webkit-animation:downExpand .6s ease 2.6s forwards;opacity:0;">
	<div class="title">${(key_001_003_005_qt_title)!''}（${report_unit_name!}）</div>
	<div style="width:510px;height:213px;"  id="main00503" class="005barchart"></div>
</div>
<script>
var chart0051arr = [${(key_001_003_yysr_sn2)!0},${(key_001_003_yysr_sn1)!0},${(key_001_003_yysr_bq)!0}];
var chart0052arr = [${(key_001_003_gslmgsdjlr_sn2)!0},${(key_001_003_gslmgsdjlr_sn1)!0},${(key_001_003_gslmgsdjlr_bq)!0}];
var chart0053arr = [${(key_001_003_005_qt_sn2)!0},${(key_001_003_005_qt_sn1)!0},${(key_001_003_005_qt_bq)!0}];
var chartdata1 = new Map();
chartdata1.put('0050',chart0051arr);
chartdata1.put('0051',chart0052arr);
chartdata1.put('0052',chart0053arr);
loadbarChart('005',chartdata1);
</script>