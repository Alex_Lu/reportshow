<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
利润分配
</div>
<div style="position: absolute;width:240px;left:200px;bottom:80px;-webkit-animation:upExpand 1s ease 1.1s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/007-1.png" style="width:100%;">
</div>
<div style="position: absolute; width: 46px; height: 90px; left: 290px; bottom: 270px;overflow: hidden;">
	<div style="width:100%;position: absolute;-webkit-animation: fallDown .8s ease-out 1.6s infinite;opacity:0;">
		<img src="${basePath}/h5/static/imgs/001-003/007-2.png" style="width:100%;">
	</div>
</div>

<div style="position: absolute;width:480px;left:80px;top:180px;-webkit-animation:downExpand 1s ease 1.6s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/007-3.png" style="width:100%;">
</div>
<div style="position: absolute;width:312px;left:133px;top:240px;-webkit-animation:downExpand 1s ease 2.6s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/007-4.png" style="width:100%;">
</div>
<div style="position: absolute;width:123px;left:190px;top:300px;-webkit-animation:downExpand 1s ease 3.6s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/007-5.png" style="width:100%;">
</div>
<#if key_001_003_007_list??>
	<#if key_001_003_007_list?size==3>
		<div style="position: absolute;width:480px;left:325px;top:165px;-webkit-animation:expand .8s ease 2.1s forwards;opacity:0;">
			<span style="font-size:1.6em;color:#fff;">每十股${key_001_003_007_list[0].title!}</span>
			<span style="color:#b36472;font-size:2.2em;font-weight:bold;">${key_001_003_007_list[0].data!}</span>
			<span style="font-size:1.6em;color:#fff;">${key_001_003_007_list[0].unit!}</span>
		</div>
		<div style="position: absolute;width:480px;left:325px;top:225px;-webkit-animation:expand .8s ease 3.1s forwards;opacity:0;">
			<span style="font-size:1.6em;color:#fff;">每十股${key_001_003_007_list[1].title!}</span>
			<span style="color:#b36472;font-size:2.2em;font-weight:bold;">${key_001_003_007_list[1].data!}</span>
			<span style="font-size:1.6em;color:#fff;">${key_001_003_007_list[1].unit!}</span>
		</div>
		<div style="position: absolute;width:480px;left:325px;top:285px;-webkit-animation:expand .8s ease 4.1s forwards;opacity:0;">
			<span style="font-size:1.6em;color:#fff;">每十股${key_001_003_007_list[2].title!}</span>
			<span style="color:#b36472;font-size:2.2em;font-weight:bold;">${key_001_003_007_list[2].data!}</span>
			<span style="font-size:1.6em;color:#fff;">${key_001_003_007_list[2].unit!}</span>
		</div>
	<#elseif key_001_003_007_list?size==2>
		<div style="position: absolute;width:480px;left:325px;top:165px;-webkit-animation:expand .8s ease 2.1s forwards;opacity:0;">
			<span style="font-size:1.6em;color:#fff;">每十股${key_001_003_007_list[0].title!}</span>
			<span style="color:#b36472;font-size:2.2em;font-weight:bold;">${key_001_003_007_list[0].data!}</span>
			<span style="font-size:1.6em;color:#fff;">${key_001_003_007_list[0].unit!}</span>
		</div>
		<div style="position: absolute;width:480px;left:325px;top:285px;-webkit-animation:expand .8s ease 3.1s forwards;opacity:0;">
			<span style="font-size:1.6em;color:#fff;">每十股${key_001_003_007_list[1].title!}</span>
			<span style="color:#b36472;font-size:2.2em;font-weight:bold;">${key_001_003_007_list[1].data!}</span>
			<span style="font-size:1.6em;color:#fff;">${key_001_003_007_list[1].unit!}</span>
		</div>
	<#elseif key_001_003_007_list?size==1>
		<div style="position: absolute;width:480px;left:325px;top:225px;-webkit-animation:expand .8s ease 2.1s forwards;opacity:0;">
			<span style="font-size:1.6em;color:#fff;">每十股${key_001_003_007_list[0].title!}</span>
			<span style="color:#b36472;font-size:2.2em;font-weight:bold;">${key_001_003_007_list[0].data!}</span>
			<span style="font-size:1.6em;color:#fff;">${key_001_003_007_list[0].unit!}</span>
		</div>
	</#if>
</#if>
