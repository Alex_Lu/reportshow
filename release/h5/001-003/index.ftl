<#include "../include_h5.ftl">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script type="text/javascript">
if (/Android (\d+\.\d+)/.test(navigator.userAgent)) {
    var version = parseFloat(RegExp.$1);
    if (version > 2.3) {
        var phoneScale = parseInt(window.screen.width) / 640;
        document.write('<meta name="viewport" content="width=640, minimum-scale = ' + phoneScale + ', maximum-scale = ' + phoneScale + ', target-densitydpi=device-dpi">');
    } else {
        document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
    }
} else {
    document.write('<meta name="viewport" content="width=640, user-scalable=no">');
}
var report_unit_name = '${report_unit_name}';
</script>
<title>${title}</title>
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/animation.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/animation_fade.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/index.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/animate-rotate.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/index.001-003.css" />

<style type="text/css">
<#if report_theme=='002-002'>
	section>article {
		background-color:#043b4c;
		color: #FDE5A9;
	}
	.background_color {
		background-color:#043b4c;
	}
<#elseif report_theme=='002-003'>
	section>article {
		background-color:#541818;
		color: #FDE5A9;
	}
	.background_color {
		background-color:#541818;
	}
<#else>
	section>article {
		background-color:#4e4e68;
		color: #FDE5A9;
	}
	.background_color {
		background-color:#4e4e68;
	}
</#if>
</style>

<script src="${basePath}/resources/js/util.js"></script>
<script type="text/javascript" src="${basePath}/h5/static/js/zepto.min.js"></script>
<script type="text/javascript" src="${basePath}/h5/static/js/echarts.min.js"></script>
<script type="text/javascript" src="${basePath}/h5/static/js/index.js"></script>
<script type="text/javascript" src="${basePath}/h5/static/js/index.001-003.js"></script>
</head>
<body scroll="no">
	<audio preload="" id="bgmusic" loop="loop">
		<source src="${basePath}/h5/static/theShow.mp3" type="audio/mpeg">
	</audio>
	<div class="music-ico"></div>
	<section id="main">
	<#if pageList?exists>
		<#list pageList as page>
			<article><#include "${page.fbPage.page_number}.ftl"></article>
		</#list>
	</#if>
	</section>
	<div style="position: absolute;width: 641px;height: 24px;top: 977px;;animation: totop 1.5s ease-out 0.5s infinite;-webkit-animation: totop 1.5s ease-out .5s infinite;z-index:5;">
         <img src="${basePath}/h5/static/imgs/1ca5ef_641_24.png" style="width: 100%; height: 100%;">
     </div>
</body>
</html>