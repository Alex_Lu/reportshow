<#assign bili011=key_001_003_yftrryslzgszrsdbl?default("0")?eval/>
<#assign bili011_1=0/>
<#assign bili011_2=9/>
<#if (bili011 lte 100) && (bili011 gt 0)>
	<#assign bili011_1=(bili011/10-1)/>
	<#assign bili011_2=((100-bili011)/10-1)/>
</#if>
<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
研发投入
</div>
<div style="position: absolute;width:220px;left:10px;top:110px;-webkit-animation:upExpand .5s ease 1.1s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/011-3.png" style="width:100%;">
</div>
<hr color=#FFF SIZE=4 style="position: absolute;width:353px;left:170px;top:210px;-webkit-animation:expand .5s ease 1.5s forwards;opacity:0;">
<div style="position: absolute;width:55px;right:70px;top:187px;-webkit-animation:rotatingAndShow 5s ease 2s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/011-4.png" style="width:100%;">
</div>
<hr color=#FFF SIZE=212 style="position: absolute;width:2px;right:94px;top:239px;-webkit-animation:downExpand .5s ease 2.1s forwards;opacity:0;">
<div style="position: absolute;width:55px;right:70px;top:450px;-webkit-animation:rotatingAndShow 4s ease 2.5s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/011-4.png" style="width:100%;">
</div>
<hr color=#FFF SIZE=4 style="position: absolute;width:388px;left:135px;top:474px;-webkit-animation:expand .5s ease 2.6s forwards;opacity:0;">
<div style="position: absolute;width:70px;left:70px;top:440px;-webkit-animation:rotatingAndShow 3s ease 3s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/011-4.png" style="width:100%;">
</div>
<hr color=#FFF SIZE=380 style="position: absolute;width:2px;left:105px;top:508px;-webkit-animation:downExpand .5s ease 3.1s forwards;opacity:0;">
<div style="position: absolute;width:50px;left:80px;top:880px;-webkit-animation:rotatingAndShow 2s ease 3.5s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/011-4.png" style="width:100%;">
</div>
<hr color=#FFF SIZE=4 style="position: absolute;width:340px;left:120px;top:900px;-webkit-animation:expand .5s ease 3.6s forwards;opacity:0;">
<div style="position: absolute;width:220px;right:10px;top:800px;-webkit-animation:upExpand 1s ease 4.1s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/011-3.png" style="width:100%;">
</div>
<div style="position: absolute;right:200px;top:175px;color:#B36472;font-size:2em;font-weight:bold;-webkit-animation:big .5s ease 4.1s forwards;opacity:0;">
人员
</div>
<div style="position: absolute;left:210px;top:230px;font-size:1.5em;line-height:1.6em;-webkit-animation:expand .5s ease 4.6s forwards;opacity:0;">

<div>研发人员</div>
<div>数量：${key_001_003_yftrrydsl?default("0")?eval}人</div>
<div>占公司总人数比例：${key_001_003_yftrryslzgszrsdbl?default("0")?eval}%</div>
<#if (bili011_1>0) && bili011_1<10>
<#list 0..bili011_1 as t><img src="${basePath}/h5/static/imgs/001-003/011-1.png" style="width:15px;margin-right:5px;"></#list>
</#if>
<br/>
<#if (bili011_2>0) && (bili011_2<10)>
<#list 0..bili011_2 as t><img src="${basePath}/h5/static/imgs/001-003/011-2.png" style="width:15px;margin-right:5px;"></#list>
</#if>
</div>
<div style="position: absolute;left:200px;top:440px;color:#B36472;font-size:2em;font-weight:bold;-webkit-animation:big .5s ease 5.1s forwards;opacity:0;">
金额
</div>

<div style="position: absolute;left:105px;top:504px;font-size:1.4em;min-width:250px;text-align:right;-webkit-animation:fadeInLeft .5s ease 5.6s forwards;opacity:0;">
	<span style="font-size:1.6em;color:#B36472;font-weight:bold;">${key_001_003_yftrhj?default("0")?eval?string(",##0.##")}</span>${report_unit_name!}
</div>
<div style="position: absolute;left:105px;top:540px;height:25px;width:250px;background-color:#ffbf4a;-webkit-animation:fadeInLeft .5s ease 5.6s forwards;opacity:0;">
</div>
<div style="position: absolute;left:370px;top:540px;font-size:1.4em;-webkit-animation:fadeInLeft .5s ease 5.6s forwards;opacity:0;">
研发投入合计
</div>
<div style="position: absolute;left:105px;top:574px;font-size:1.4em;min-width:250px;text-align:right;-webkit-animation:fadeInLeft .5s ease 6.1s forwards;opacity:0;">
	<span style="font-size:1.6em;color:#B36472;font-weight:bold;">${key_001_003_yftrbqzbh?default("0")?eval?string(",##0.##")}</span>${report_unit_name!}
</div>
<div style="position: absolute;left:105px;top:610px;height:25px;width:250px;border:1px dashed #ff6145;-webkit-animation:fadeInLeft .5s ease 6.1s forwards;opacity:0;">
	<div style="height:100%;width:${key_001_003_yftrbqzbh?default("0")?eval/key_001_003_yftrhj?default("1")?eval*100}%;background-color:#ff6145;"></div>
</div>
<div style="position: absolute;left:370px;top:610px;font-size:1.4em;-webkit-animation:fadeInLeft .5s ease 6.1s forwards;opacity:0;">
本期资本化研发投入
</div>
<div style="position: absolute;left:105px;top:644px;font-size:1.4em;min-width:250px;text-align:right;-webkit-animation:fadeInLeft .5s ease 6.6s forwards;opacity:0;">
	<span style="font-size:1.6em;color:#B36472;font-weight:bold;">${key_001_003_yftrbqfy?default("0")?eval?string(",##0.##")}</span>${report_unit_name!}
</div>
<div style="position: absolute;left:105px;top:680px;height:25px;width:250px;border:1px dashed #00b597;-webkit-animation:fadeInLeft .5s ease 6.6s forwards;opacity:0;">
	<div style="height:100%;width:${key_001_003_yftrbqfy?default("0")?eval/key_001_003_yftrhj?default("1")?eval*100}%;background-color:#00b597;"></div>
</div>
<div style="position: absolute;left:370px;top:680px;font-size:1.4em;-webkit-animation:fadeInLeft .5s ease 6.6s forwards;opacity:0;">
本期费用化研发投入
</div>
<div style="position: absolute;left:105px;top:714px;font-size:1.4em;width:250px;text-align:right;-webkit-animation:fadeInLeft .5s ease 7.1s forwards;opacity:0;">
	<span style="font-size:1.6em;color:#B36472;font-weight:bold;">${key_001_003_yftrzezyysrbl!}%</span>
</div>
<div style="position: absolute;left:105px;top:750px;height:25px;width:250px;border:1px dashed #ff9360;-webkit-animation:fadeInLeft .5s ease 7.1s forwards;opacity:0;">
	<div style="height:100%;width:${key_001_003_yftrzezyysrbl?default("0")?eval}%;background-color:#ff9360;"></div>
</div>
<div style="position: absolute;left:370px;top:750px;font-size:1.4em;-webkit-animation:fadeInLeft .5s ease 7.1s forwards;opacity:0;">
研发投入总额<br/>占营业收入比例
</div>

