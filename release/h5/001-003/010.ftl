
<#assign maxof010=0/>
<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png" class="img_breadth">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png" class="img_breadth">
主营业务发展
</div>
<div class="title_small">(分地区)</div>
<div class="yewufazhan" style="position:absolute;top:180px;width:100%;text-align:center;">
	<div class="biaoti" style="-webkit-animation:upExpand 1s ease 1.6s forwards;opacity:0;">营业收入（${report_unit_name!}）</div>
	<div class="div_pie" id="list010" style="-webkit-animation:expand 1s ease 2.1s forwards;opacity:0;"></div>
</div>
<div class="yewufazhan" style="position:absolute;top:620px;width:100%;text-align:center;">
	<div class="biaoti" style="-webkit-animation:upExpand 1s ease 2.6s forwards;opacity:0;">毛利率</div>
	<div style="position:absolute;height:300px;left:40px;width:560px;-webkit-animation:upExpand 1s ease 3.1s forwards;opacity:0;">
		<#if json_key_001_003_table_dq?? && json_key_001_003_table_dq?size gte 2 && json_key_001_003_table_dq?size lte 5>
			<hr color=#FFF SIZE=1 style="position: absolute;width:100%;bottom:30px;">
			<#list json_key_001_003_table_dq as item>
				<#if maxof010 lt item.json_key_001_003_mlr_?default("0")?eval>
				<#assign maxof010=item.json_key_001_003_mlr_?default("0")?eval/>
				</#if>
			</#list>
			<#list json_key_001_003_table_dq as item>
				<#assign itemTemp=item.json_key_001_003_mlr_?default("0")?eval/>
				<div class="zhu_item zhu_item_${json_key_001_003_table_dq?size}_${item_index}">
					<div class="bili" style="bottom:${itemTemp/maxof010*200-5}px;">${itemTemp}%</div>
					<img src="${basePath}/h5/static/imgs/001-003/005-3.png" class="zhu" style="height:${itemTemp/maxof010*200}px;">
					<div class="yuandian"></div>
					<div class="item_name">${item.json_key_001_003_fhy_!}</div>
				</div>
			</#list>
		<#else>
		<div style="font-size:3em;margin-top:100px;">无</div>
		</#if>
	</div>
</div>
<script type="text/javascript">
var list010_data = [];
<#if json_key_001_003_table_dq?? && json_key_001_003_table_dq?size gte 2 && json_key_001_003_table_dq?size lte 5>
	<#list json_key_001_003_table_dq as item>
		list010_data.push({name:'${item.json_key_001_003_fhy_!}',value:'${item.json_key_001_003_yysr_!}'});
	</#list>
</#if>
loadPie('list010',list010_data);
</script>