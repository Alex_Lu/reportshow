<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
本期亮点
</div>
<div style="width: 280px;height: 280px;position:absolute;top:150px;left:180px; -webkit-animation:downExpand .6s ease 1s forwards;opacity:0;">
	<img src="${basePath}/h5/static/imgs/001-003/002-1.png" style="width: 280px;height: 280px;position:absolute;top:0px;">
	<div style="position:absolute;top:70px;left:50px;width:180px;text-align:center;overflow:hidden;">
		<span style="color:#b36472;font-size:2em;font-weight:bold;">${key_001_003_yysr_bq?default("0")?eval?string(",##0.##")}</span><span style="font-size:1.6em;">${report_unit_name!}</span>
		<div style="color:#99cdd3;font-size:1.8em;">营业收入</div>
		<div style="font-size:1.6em;">
		<img src="${basePath}/h5/static/imgs/001-003/002-4.png" style="width:25px;height:25px;-webkit-animation:upup 1s ease-out 1.2s infinite;">
		<#if key_001_003_yysr_zs?? && (key_001_003_yysr_zs?default("0")?eval>0)>
		增长<#else>
		减少</#if>${key_001_003_yysr_zs?default("0")?eval?string("#.##")}%
		</div>
	</div>
</div>
<div style="width: 280px;height: 280px;position:absolute;top:400px;left:120px; -webkit-animation:downExpand .6s ease 1.6s forwards;opacity:0;">
	<img src="${basePath}/h5/static/imgs/001-003/002-2.png" style="width: 280px;height: 280px;position:absolute;top:0px;">
	<div style="position:absolute;top:50px;left:50px;width:180px;text-align:center;overflow:hidden;">
		<div style="text-align:center;"><span style="color:#b36472;font-size:2em;font-weight:bold;">${key_001_003_gslmgsdjlr_bq?default("0")?eval?string(",##0.##")}</span><span style="font-size:1.6em;">${report_unit_name!}</span></div>
		<div style="color:#99cdd3;font-size:1.8em;text-align:center;">归属于母公司<br/>的净利润</div>
		<div style="font-size:1.6em;">
		<img src="${basePath}/h5/static/imgs/001-003/002-4.png" style="width:25px;height:25px;-webkit-animation:upup 1s ease-out 1.2s infinite;">
		<#if key_001_003_gslmgsdjlr_zs?? && (key_001_003_gslmgsdjlr_zs?default("0")?eval>0)>
		增长<#else>
		减少</#if>${key_001_003_gslmgsdjlr_zs?default("0")?eval?string("#.##")}%
		</div>
	</div>
</div>
<div style="width: 280px;height: 280px;position:absolute;top:600px;right:100px;-webkit-animation:downExpand .6s ease 2.2s forwards;opacity:0;">
	<img src="${basePath}/h5/static/imgs/001-003/002-3.png" style="width: 280px;height: 280px;position:absolute;top:0px;">
	<div style="position:absolute;top:70px;right:50px;width:80%;text-align:center;">
		<span style="color:#b36472;font-size:2em;font-weight:bold;">${key_001_003_qt_bq?default("0")?eval?string(",##0.##")}</span><span style="font-size:1.6em;">${report_unit_name}</span>
		<div style="color:#99cdd3;font-size:1.8em;">${key_001_003_002_qt_bq_title!}</div>
		<div style="font-size:1.6em;">
		<img src="${basePath}/h5/static/imgs/001-003/002-4.png" style="width:25px;height:25px;-webkit-animation:upup 1s ease-out 1.2s infinite;">
		<#if key_001_003_qt_sn2?? && (key_001_003_qt_sn2?default("0")?eval>0)>
		增长<#else>
		减少</#if>${key_001_003_qt_sn2?default("0")?eval?string("#.##")}%
		</div>
	</div>
</div>

<div style="position: absolute;width:250px;left:10px;bottom:80px;-webkit-animation:upExpand 1s ease 2.8s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/002-5.png" style="width:100%;">
</div>
