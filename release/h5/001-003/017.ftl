<#assign sizeof017=0/>
<#if json_key_001_003_table_irrl??>
	<#assign sizeof017=json_key_001_003_table_irrl?size/>
</#if>
<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png" class="img_breadth">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png" class="img_breadth">
投资者关系管理
</div>
<div style="text-align:center;width:100%;margin-top:150px;-webkit-animation:big .8s ease 1.1s forwards;opacity:0;">
<img src="${basePath}/h5/static/imgs/001-003/017.png" style="width:250px;">
</div>
<#if json_key_001_003_table_irrl??>
<#list json_key_001_003_table_irrl as item>
	<div style="text-align:center;width:80%;margin-left:10%;font-size:1.3em;margin-top:20px;-webkit-animation:fadeInRight .6s ease ${1.6+item_index*0.5}s forwards;opacity:0;">
		<div style="font-size:1.4em;font-weight:bold;line-height:40px;color:#B36472;">${item.json_key_001_003_mc_!}</div>
		<div>时间：${item.json_key_001_003_rq_!}</div>
		<div>地点：${item.json_key_001_003_nr_!}</div>
	</div>
</#list>
</#if>
<div style="margin-top:20px;font-size:1.2em;margin-left:80px;-webkit-animation:fadeInUp .8s ease ${1.6+sizeof017*0.5}s forwards;opacity:0;">
<table>
	<tr>
		<td style="width:70px;">联系人:</td>
		<td>${key_001_003_lxr!}</td>
	</tr>
	<tr>
		<td>电&nbsp;&nbsp;&nbsp;话:</td>
		<td>${key_001_003_dh!}</td>
	</tr>
	<tr>
		<td>地&nbsp;&nbsp;&nbsp;址:</td>
		<td>${key_001_003_dz!}</td>
	</tr>
</table>
</div>