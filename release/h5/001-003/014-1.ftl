<#assign sizeof01401=0/>
<#if json_key_001_003_table_ggtd??>
	<#assign sizeof01401=json_key_001_003_table_ggtd?size/>
</#if>
<div class="title">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
<img src="${basePath}/h5/static/imgs/001-003/jiantou.png">
高管团队
</div>
<div class="title_small">管理层</div>
<style type="text/css">
	.touxiang {
		width:130px;
		float:left;
		opacity:0;
		padding:12px 0 12px 24px;
		z-index:3;
	}
	.touxiang2 {
		position: absolute;
		opacity:0;
	}
	.shou-014-1 {
		position:absolute;
		bottom:-10px;
		left:10px;
		width:24px;
		z-index:5;
	}
	.title-014-1 {
		position: absolute;
		top:50px;
		left:40px;
		width:100px;
	}
</style>
<div style="width:100%;position: absolute;top:190px;">
<#if json_key_001_003_table_ggtd??>
	<#list json_key_001_003_table_ggtd as item>
		<div class="touxiang" style="-webkit-animation:big .4s ease ${2+item_index*0.5}s forwards;" 
			xm="${item.json_key_001_003_xm_}" 
			zw="${item.json_key_001_003_zw_}" 
			jl="${item.json_key_001_003_jl_}">
			<div class="touxiang2" style="<#if item_index==0>-webkit-animation:zoomIn .5s ease ${2+sizeof01401*0.5}s forwards;</#if>">
				<img src="${basePath}/h5/static/imgs/001-003/014-2.png" style="width:100%;">
				<img src="${basePath}/h5/static/imgs/001-003/shou.png" class="shou-014-1">
			</div>
			<div class="title-014-1">
				<div style="color:#B36472;font-size:24px;text-align:center;">${item.json_key_001_003_xm_}</div>
				<div style="font-size:16px;text-align:center;">${item.json_key_001_003_zw_}</div>
			</div>
			<img src="${basePath}/h5/static/imgs/001-003/014-1.png" style="width:100%;">
		</div>
	</#list>
</#if>
<div style="clear:both;"></div>
</div>
<#if json_key_001_003_table_ggtd?? && json_key_001_003_table_ggtd?size gt 0>
	<div style="border-radius: 5px;border: 2px solid #f8fdfe;position: absolute;left:40px;bottom:120px;height:360px;width:560px;overflow:hidden;-webkit-animation:fadeInUp .8s ease ${2+sizeof01401*0.5}s forwards;opacity:0;">
		<div style="padding:30px;background-color:#ff6145;height:100%;">
			<div style="padding-bottom:6px;margin-bottom:10px;border-bottom: 1px solid #fde5a9;">
				<div style="width:250px;margin:0 auto;">
					<div id="xm014-1" style="font-size:30px;line-weight:bold;float:left;margin:0 20px;">${json_key_001_003_table_ggtd[0].json_key_001_003_xm_}</div>
					<div id="zw014-1" style="font-size:16px;float:left;text-align:center;">${json_key_001_003_table_ggtd[0].json_key_001_003_zw_}</div>
					<div style="clear:both;"></div>
				</div>
			</div>
			<div id="jl014-1" style="font-size:20px;">
				${json_key_001_003_table_ggtd[0].json_key_001_003_jl_}
			</div>
		</div>
	</div>
</#if>
