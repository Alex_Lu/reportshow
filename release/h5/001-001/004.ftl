<div style="position: absolute; width: 100%; height: 100%;">
	<img src="${basePath}/h5/static/imgs/background_640x1040.png" style="width:100%;height:100%;">
</div>
<div style="position: absolute; opacity:1;width: 120px;height:92px; color: #28303B;font-size: 3.4em;top: 33px;font-weight: bold;left: 58px;line-height: 54px;">
	${report_type}
</div>
<div style="position: absolute; opacity:1;width: 240px;height: 32px;color: #452709;font-size: 1.4em;top: 95px;font-weight: 600;right: 54px;line-height: 32px;text-align: right;">
	${report_type}主要原因
</div>
<div class="rotateReasonLeft" style="left: 0px; top: 310px; height:180px; position: absolute; z-index: 3;">
	<div style="position: absolute; opacity:1; width: 520px; height: 6px; top: 174px; left: 0px; background-color:#FFFFFF;-webkit-animation:showing 0.6s ease .6s forwards;opacity: 0;">
	</div>
	<div style="position: absolute; width: 400px; left:40px; height: 86px; bottom: 16px; color:#000000; font-size: 2.2em; font-weight: bold;-webkit-animation:showing 1.2s ease 1.8s forwards;opacity:0;">
		${key_yysm1!}
	</div>
	<div style="position: absolute; width: 143px; height: 143px; left: 428px; top: 37px;-webkit-animation:slideInReason1 1.8s ease .6s forwards;opacity: 0;">
		<img src="${basePath}/h5/static/icon/reason_icon_1_143_143.png">
	</div>
</div>
<div class="rotateReasonRight" style="left: 75px; top: 410px; height:143px; width:565px; position: absolute; z-index: 3;">
	<div style="position: absolute; width: 520px; height: 6px; top: 137px; left: 71px; background-color:#FFFFFF;-webkit-animation:showing 0.6s ease 1.8s forwards;opacity: 0;">
	</div>
	<div style="position: absolute; opacity:0; width: 400px; right:0px; height: 86px; bottom: 16px; color:#000000; font-size: 2.2em; font-weight: bold;-webkit-animation:showing 1.2s ease 3s forwards;opacity:0;">
		${key_yysm2!}
	</div>
	<div style="position: absolute; width: 143px; height: 143px; left: 0px; top: 0px;-webkit-animation:slideInReason2 1.8s ease 1.8s forwards;opacity: 0;">
		<img src="${basePath}/h5/static/icon/reason_icon_2_143_143.png" style="width: 100%; height: 100%;">
	</div>
</div>
<div class="rotateReasonLeft" style="left: 0px; top: 700px; height:180px; width:565px; position: absolute; z-index: 3;">
	<div style="position: absolute; width: 520px; height: 6px; top: 174px; left: 0px; background-color:#FFFFFF;-webkit-animation:showing 0.6s ease 3s forwards;opacity: 0;">
	</div>
	<div style="position: absolute; width: 400px; left:40px; height: 86px; bottom: 16px; color:#000000; font-size: 2.2em; font-weight: bold;-webkit-animation:showing 1.2s ease 4.2s forwards;opacity:0;">
		${key_yysm3!}
	</div>
	<div style="position: absolute; width: 143px; height: 143px; left: 428px; top: 37px; -webkit-animation:slideInReason3 1.8s ease 3s forwards;opacity: 0;">
		<img src="${basePath}/h5/static/icon/reason_icon_3_143_143.png">
	</div>
</div>
