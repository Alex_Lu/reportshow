<div style="top:32px;position: absolute;width:100%;text-align:center;-webkit-animation:fadeInDown .5s ease .5s forwards;opacity:0;">
	<img src="${basePath}/${img_key_logo!}" style="width:232px;height: 80px;">
	<div style="color: #5C5C5C;font-size: 1.5em;">
		${zuzhijigou}
	</div>
	<div style="color: #5C5C5C;font-size: 1.5em;">
		${year}年度${report_type}公告
	</div>
</div>
<div style="position: absolute;width:100%;text-align:center;top:220px;-webkit-animation:fadeInUp 1s ease .5s forwards;opacity:0;">
	<img src="${basePath}/h5/static/imgs/phone_641x618.png" style="width:100%;">
	<div style="position: absolute;top:86px;width:100%;color: #5C5C5C;font-size: 32px;font-family: 'Arial Black';font-weight: bold;">
		${year}
	</div>
	<div style="position: absolute;top:130px;width:100%;color: #F68E21;font-size: 48px;font-weight: bold;">
		${report_type}
	</div>
</div>
<div style="top: 629px;position: absolute;left: 260px;-webkit-animation: bounce 1s ease-out 2s infinite;opacity:0;">
	<img src="${basePath}/h5/static/imgs/logo_117x117.png" style="width: 100%;height:100%;">
</div>
<div style="position: absolute; width: 380px;height: 531px;left: 378px;bottom: 10px;-webkit-animation: handDownUp 2s ease-out 1.6s infinite;opacity:0;">
	<img src="${basePath}/h5/static/imgs/hand.png">
</div>
<div style="position: absolute;width: 218px; height: 53px;left: 14px;top: 892px;-webkit-animation:fadeInLeft 1s ease 1.6s forwards;opacity:0;">
	<img aria-hidden="false" src="${basePath}/h5/static/imgs/logo_175x41.png" style="width: 100%; height: 100%;">
</div>