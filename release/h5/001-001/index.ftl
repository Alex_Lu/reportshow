<#include "../include_h5.ftl">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script type="text/javascript">
if (/Android (\d+\.\d+)/.test(navigator.userAgent)) {
    var version = parseFloat(RegExp.$1);
    if (version > 2.3) {
        var phoneScale = parseInt(window.screen.width) / 640;
        document.write('<meta name="viewport" content="width=640, minimum-scale = ' + phoneScale + ', maximum-scale = ' + phoneScale + ', target-densitydpi=device-dpi">');
    } else {
        document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
    }
} else {
    document.write('<meta name="viewport" content="width=640, user-scalable=no">');
}
//微信去掉下方刷新栏
if (navigator.userAgent.indexOf('MicroMessenger') >= 0) {
    document.addEventListener('WeixinJSBridgeReady',
    function() {
        //WeixinJSBridge.call('hideToolbar');
    });
}
</script>
<title>${title}</title>
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/animation.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/animation_fade.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/index.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/h5/static/css/animate-rotate.css" />

<style type="text/css">
<#if report_theme=='002-001'>
section>article:nth-child(odd) {
	background-color:#FFDF61;
}
section>article:nth-child(even) {
	background-color:#FF8500;
}
<#elseif report_theme=='002-001'>
article {
	background: url(${basePath}/h5/static/imgs/bgimg/blue.jpg) no-repeat fixed;
}
</#if>
</style>

<script type="text/javascript" src="${basePath}/h5/static/js/zepto.min.js"></script>
<script type="text/javascript" src="${basePath}/h5/static/js/index.js"></script>
</head>
<body scroll="no">
	<audio preload="" id="bgmusic" loop="loop">
		<source src="${basePath}/h5/static/bgm03.mp3" type="audio/mpeg">
	</audio>
	<div class="music-ico"></div>
	<section id="main">
		<article><#include "001.ftl"></article>
		<article><#include "002.ftl"></article>
		<article><#include "003.ftl"></article>
		<article><#include "004.ftl"></article>
		<article><#include "005.ftl"></article>
		<article><#include "006.ftl"></article>
	</section>
	<div style="position: absolute;width: 641px;height: 24px;top: 977px;;animation: totop 1.5s ease-out 0.5s infinite;-webkit-animation: totop 1.5s ease-out .5s infinite;">
         <img aria-hidden="false" src="${basePath}/h5/static/imgs/1ca5ef_641_24.png" style="width: 100%; height: 100%;">
     </div>
</body>
</html>