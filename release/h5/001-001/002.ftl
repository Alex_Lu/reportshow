<div style="position: absolute; width: 100%; height: 100%;">
	<img src="${basePath}/h5/static/imgs/background_640x1040.png" style="width: 100%; height: 100%;">
</div>
<div style="position: absolute; opacity:1;width: 120px;height:92px; color: #28303B;font-size: 3.4em;top: 33px;font-weight: bold;left: 58px;line-height: 54px;">
	${report_type}
</div>
<div style="position: absolute; opacity:1;width: 240px;height: 32px;color: #452709;font-size: 1.4em;top: 95px;font-weight: 600;right: 54px;line-height: 32px;text-align: right;">
	业绩预告情况
</div>
<div style="position: absolute; width: 550px; height: 88px; box-shadow: none; left: 0px; top: 202px; z-index: 3; -webkit-animation:fadeInLeft .6s ease .6s forwards;opacity:0;">
	<div style="position: absolute; width: 100%; height: 100%;">
		<img src="${basePath}/h5/static/imgs/dc30ce_550_88.png" style="width: 100%; height: 100%;">
	</div>
	<div style="position: absolute; opacity:1;width: 450px;line-height: 40px;color: #000000;font-size: 30px;top: 4px;font-weight: bold;left: 10px;text-align: center;">
		${year}年净利润
	</div>
	<div style="position: absolute; opacity:1;width: 450px; line-height: 40px;color: #000000;font-size: 30px;top: 44px;font-weight: bold;left: 10px;text-align: center;">
		比上年同期增长${key_tqzzfwzd!}%-${key_tqzzfwzg!}%
	</div>
</div>
<div class=" x-component" style="position: absolute;top: 51px;">
	<div style="position: absolute; width: 641px; height: 410px;top: 326px; -webkit-animation:expand .6s ease 1.2s forwards;opacity:0;">
		<img src="${basePath}/h5/static/imgs/032b5b_641_410.png" style="width: 100%; height: 100%;">
	</div>
	<div style="position: absolute; width: 641px; height: 179px; overflow: hidden; left: 0px; top: 302px; z-index: 2;">
		<div style="position: absolute; width: 307px; height: 307px; left: 166px; top: 23px;-webkit-animation:rotateIn-${max} .6s ease 1.8s forwards;opacity:0;">
			<img src="${basePath}/h5/static/imgs/ed67a2_306_307.png" style="width: 100%; height: 100%;">
		</div>
		<div style="position: absolute; width: 307px; height: 307px; left: 166px; top: 23px;z-index:2;-webkit-animation:rotateIn-${min} .6s ease 1.8s forwards;opacity:0;">
			<img src="${basePath}/h5/static/imgs/dca415_306_307.png" style="width: 100%; height: 100%;">
		</div>
		<div style="position: absolute; width: 307px; height: 307px; left: 166px; top: 23px;-webkit-animation:rotateIn-${max} .6s ease 1.8s forwards;opacity:0; ">
			<img src="${basePath}/h5/static/imgs/angle.png" style="width: 100%; height: 100%;">
		</div>
	</div>
	<div style="left: 0px; top: 0px; position: absolute; z-index: 3;-webkit-animation:showing .6s ease 2.4s forwards;opacity:0;">
		<div class="percentText${mincss?int}" style="position: absolute; opacity:1; line-height:20px; color: #000000;font-size: 2.5em;font-style: italic;font-weight: bold;">
			${key_tqzzfwzd!}%
		</div>
		<div class="percentText${maxcss?int}" style="position: absolute; opacity:1; line-height:20px; color: #000000;font-size: 2.5em;font-style: italic;font-weight: bold;">
			${key_tqzzfwzg!}%
		</div>
		<div style="position: absolute; width: 60px; height: 54px; left: 288px; top: 448px;">
			<img src="${basePath}/h5/static/imgs/p_87_79.png" style="width: 100%; height: 100%;">
		</div>
	</div>
</div>

<div style="position: absolute; width: 548px; height: 89px;left: 93px; top: 852px; -webkit-animation:fadeInRight 1s ease 2.8s forwards;opacity:0;">
	<img aria-hidden="false" src="${basePath}/h5/static/imgs/c947e7_548_89.png">
	<div style="position: absolute; opacity:1;width: 450px;height: 45px;color: #000000;font-size: 30px;top: 8px;font-weight: bold;left: 80px;text-align: center;">
		${key_smwz1!}<br/>${key_smwz2!}
	</div>
</div>